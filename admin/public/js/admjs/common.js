//取得圖片真實寬高
( function ( $ ){ 
    var 
    props =  [ 'Width' ,  'Height' ], 
    prop ;

    while  ( prop = props . pop ())  { 
    ( function  ( natural , prop )  { 
      $ . fn [ natural ]  =  ( natural in  new  Image ())  ?  
      function  ()  { 
      return  this [ 0 ][ natural ]; 
      }  :  
      function  ()  { 
      var  
      node =  this [ 0 ], 
      img , 
      value ;

      if  ( node . tagName . toLowerCase ()  ===  'img' )  { 
        img =  new  Image (); 
        img . src = node . src , 
        value = img [ prop ]; 
      } 
      return value ; 
      }; 
    }( 'natural'  + prop , prop . toLowerCase ())); 
    } 
  }( jQuery ));

function arraySearch(val,arr) {
	for (var i=0; i<arr.length; i++)
		if (arr[i] == val)
			return true;
	return false;
}
function arrayRemove(val,arr){
	var i = arr.indexOf(val);
	if(i != -1) {
		arr.splice(i, 1);
	}
}

//編輯信件
function edit() {
	$('.edit_div').summernote({	
		focus: true,
		lang: 'zh-TW'
	});
	$('#mail_subject,#cancel_edit,#save').prop('disabled',false);
};
//取消編輯
function cancel_edit() {
	$('.edit_div').destroy();
	$('#mail_subject,#cancel_edit,#save').prop('disabled',true);
};

//儲存信件
function save(ajax) {	
	var msg = '<p>儲存後系統將會正式更改信件內容</p><p>你可以透過測試寄信確認信件是否有誤</p>';
	warning('注意', msg, '確認', function() {
		$.ajax({	
			url: base_url+ajax,
			type:"post",
			data: {	
				"project" : $('#mail_project').val() ,
				"subject" : $('#mail_subject').val() ,
				"content" : $('.edit_div').code()
			},
			success:function(){
				$.growlUI('儲存成功!');
				$('.edit_div').destroy();
				$('#mail_subject').prop('disabled',true);
				$('#mail_subject,#cancel_edit,#save').prop('disabled',true);
			},
			error:function(){
				$.growlUI('儲存失敗!');
			}
		});		
	});
};





//建立狀態啟用停用checkbox
function create_state() {
	var options = {
		onText: "啟用",
		offText: "停用",
		onColor: 'success',
		offColor: 'danger',		
		animate: true,
		size: 'mini'
	};
	$("[name='enable-checkbox']").bootstrapSwitch(options).on('switchChange.bootstrapSwitch', function(event, state) {		
		var myid=$(this).closest("tr").find("td:eq(0)").text();		
		$.ajax({
			url: base_url+"common/change_state",
			type:"post",
			data: {	
				"table": 	$("#mytable").val(),
				"id": 		myid,
				"state" : 	state	
			},
			success: function(){
				if(state==true)
					$.growlUI('啟用成功!');
				else
					$.growlUI('停用成功!');
			}	
		});
	});
};

//建立狀態啟用停用checkbox
function create_vip_state() {
	var options = {
		onText: "啟用",
		offText: "停用",
		onColor: 'info',
		offColor: 'danger',		
		animate: true,
		size: 'mini'
	};
	$("[name='vip-checkbox']").bootstrapSwitch(options).on('switchChange.bootstrapSwitch', function(event, state) {		
		var myid=$(this).closest("tr").find("td:eq(0)").text();		
		$.ajax({
			url: base_url+"common/change_vip",
			type:"post",
			data: {	
				"table": 	$("#mytable").val(),
				"id": 		myid,
				"state" : 	state	
			},
			success: function(){
				if(state==true)
					$.growlUI('啟用成功!');
				else
					$.growlUI('停用成功!');
			}	
		});
	});	
};

//單張上傳圖片,並且可以裁剪*******************************/
function create_single_upload(path,form) {
	form = form || ".upload_form";	
	//使用strict mode
	'use strict';
	//初始化
	$(form).fileupload({
		url: base_url+'upload/init_single_upload/'+path,
		maxNumberOfFiles: 1,
		maxFileSize: 5000000,					//文件大小限制
		autoUpload: true,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i	
    });
	//跨網域	
    $(form).fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	//讀取
	$(form).addClass('fileupload-processing');
	$.ajax({
		url: $(form).fileupload('option', 'url'),
		dataType: 'json',
		context: $(form)[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');		
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});	
		if(result.files.length>0){
			$(this).find('.edit_pic').css('min-height','0');
			$(this).find('.new_pic').hide();
			$(this).parent().parent().find('.btnDoCrop').attr('disabled',false);
		}
	});
	//上傳驗證
	$(form).bind('fileuploadstart', function (e, data) {
		$(this).find('.new_pic').hide();
		$(this).find('.edit_pic').css('min-height','0');
	}).bind('fileuploadadd', function (e, data) {
		var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'jpg,png';
		if (allowdtypes.indexOf(fileType) < 0) {
			alert('檔案格式不正確');
			return false;
		}	
		if(data.originalFiles[0]['size'] > 5000000) {
            alert('檔案太大');
			return false;
        }
	}).bind('fileuploaddone', function (e, data) {			
		$(this).parent().parent().find('.btnDoCrop').attr('disabled',false);
	}).bind('fileuploaddestroyed', function (e, data) {
		$(this).find('.new_pic').show();
		$(this).find('.edit_pic').css('min-height','300px');
		$(this).parent().parent().find('.cropfinish,.btnDoCrop').attr('disabled',true);	
	});	
};

//裁剪*******************************/
var myblock;
var jcrop_api;
function cropSelect(c){
	myblock.find('.x').val(c.x);
	myblock.find('.y').val(c.y);
	myblock.find('.w').val(c.w);
	myblock.find('.h').val(c.h);
	var naturalWidth = myblock.find('.jcrop-holder').find('.target').naturalWidth();
	myblock.find('.now_w').val(myblock.find('.jcrop-holder').width());
	myblock.find('.origin_w').val(naturalWidth);
	if(c.w>0)
		myblock.find('.cropfinish').attr('disabled',false);
};
function cropChange(c){	
	myblock.find('.cropfinish').attr('disabled',true);		
};
$(document).on('click','.cropfinish',function(){
	var dir = $(this).data('dir');	
	$.ajax({
		url: base_url+'upload/crop_cover/'+dir,
		type:"post",
		data: myblock.closest('form').serialize(),
		success: function(){
			myblock.find('.target').attr('src',myblock.find('.target').attr('src').replace(/\.jpg/g, ".png")+1);
			myblock.find('.photos_delete').data('url',myblock.find('.photos_delete').data('url').replace(/\.jpg/g,".png"));
			myblock.find('.target').height('auto');
			myblock.find('.btnDoCrop').attr('disabled',false);
			myblock.find('.cropfinish').attr('disabled',true);
		}			
	});		
	jcrop_api.destroy();			
});

//多張圖片上傳
function create_multiple_upload(form,type,id) {	
	//使用strict mode(嚴格模式)
	'use strict';	
	//初始化
	$(form).fileupload({       
		url: base_url+'upload/init_multiple_upload/'+type+'/'+id,
		maxNumberOfFiles: 5,	
		autoUpload: true,
		maxFileSize: 5000000,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i 			
    });
	//跨網域	
    $(form).fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );	
	//讀取
	$(form).addClass('fileupload-processing');
	$.ajax({
		url: $(form).fileupload('option', 'url'),
		dataType: 'json',
		context: $(form)[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');		
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});		
		$.ajax({
			url: base_url+'upload/get_cover',
			type:"post",
			dataType:"json",
			data:{'id':$('#id').val()},
			success:function(result){
				$(".photos_cover").each(function(){
					if(result.filename==$(this).data("name")){
						$(this).addClass('is_cover');
					}							
				});
				
			}
		});
	});	
	//相簿照片上傳驗證
	$(form).bind('fileuploadadd', function (e, data) {
		var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'jpeg,jpg,png,JPEG,JPG,PNG';
		if (allowdtypes.indexOf(fileType) < 0) {
			alert_box('錯誤','檔案格式不正確');
			return false;
		}	
		if(data.originalFiles[0]['size'] > 5000000) {
            alert_box('錯誤','檔案太大');
			return false;
        }
		if($(form).find(".template-download:not('.ui-state-error')").length >= 5) {
            alert_box('錯誤','最多5張');
			return false;
        }		
	}).bind('fileuploaddone', function (e, data) {
		$.ajax({
			url: base_url+'upload/upload_photos',
			type:"post",
			dataType:'json',
			data: { 
				"filename" 	: data.result.files[0].name,
				"id"		: $('#id').val()
			},
			success: function(result){
				if(result==false){
					console.log($('.photos_cover'));
					$('.photos_cover').each(function(){
						if($(this).data('name')==data.result.files[0].name)
							$(this).addClass('is_cover');
					});
					$.growlUI('設置封面!');
				}else{
					
				}
					
			}	
		});
	}).bind('fileuploaddestroy', function (e, data) {	
		$.ajax({
			url: base_url+'upload/delete_photos',
			type:"post",
			data: { 
				"filename" 	: data.name,
				"id"		: $('#id').val()
			},
			success: function(){
				$.growlUI('刪除成功!');
			}	
		});
	});
};


//編輯此筆
function editFormatter(value, row, index) {
    return [        
        '<a class="edits ml10" href="javascript:void(0)" title="編輯">',
			'<img class="edit" src="'+base_url+'public/img/edit-icon.png'+'">',
        '</a>'
    ].join('');
}

//刪除此筆
function deleteFormatter(value, row, index) {
    return [        
        '<a class="deletes ml10" href="javascript:void(0)" title="刪除">',
			'<img class="delete" src="'+base_url+'public/img/delete-icon.png'+'" >',
        '</a>'
    ].join('');
}

//editable快速編輯
$(document).on('editable-save.bs.table','.quickedit',function(e, name, args){
	$.ajax({
		url: base_url+'common/quicksave',
		type:"post",
		data: {		
			'table':$('#mytable').val(),
			'pk':	$('#pk').val(),
			'field':name,
			'value':args			
		},
		success: function(){
			$.growlUI('儲存成功!');
		}			
	});
});











/*
//建立jquery file upload
function create_upload(id,cover) {	
	'use strict';	//使用strict mode(嚴格模式)
	$('#fileupload').fileupload({
        url: base_url+'public/plugin/jQuery-File-Upload-9.8.1/server/php/index.php?id='+id+'&'
    });	
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	$('#fileupload').fileupload('option',{
		maxFileSize: 5000000,							//文件大小限制
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i		//文件類型限制				
	}); 
	$('#fileupload').addClass('fileupload-processing');
	$.ajax({
		url: $('#fileupload').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#fileupload')[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
		$(".yes_icon").hide();
		$(".yes_icon").each(function(){			
			if($.trim($(this).closest("tr").find("td:eq(1)").text()) == cover)
				$(this).closest("tr").find(".yes_icon").fadeIn();
		});		
	});
};
*/

//警告視窗
function alert_box(tittle,msg) {	
	bootbox.dialog({
		message: msg,
		title: '<img src="'+base_url+'public/img/warning_icon.png" style="height:30px;" /><span style="line-height:30px;">'+tittle+'</span>',
		className: "warning",
		buttons: {			
			cancel: {
				label: "確認",
				className: "btn-default"			
			}
		}
	});			
}

//警告視窗
function warning(tittle,msg,button,callback) {	
	bootbox.dialog({
		message: msg,
		title: '<img src="'+base_url+'public/img/warning_icon.png" style="height:30px;" /><span style="line-height:30px;">'+tittle+'</span>',
		className: "warning",
		buttons: {
			confirm: {
				label: button,
				className: "btn-primary",
				callback: callback
			},				
			cancel: {
				label: "取消",
				className: "btn-default"			
			}
		}
	});			
}

//刪除視窗
function delete_box(mytr,ajax,id) {	
	bootbox.dialog({
		message: '確定要刪除嗎?',
		title: '<img src="'+base_url+'public/img/warning_icon.png" style="height:30px;" /><span style="line-height:30px;">警告</span>',
		className: "warning",
		buttons: {
			confirm: {
				label: '確定',
				className: "btn-primary",
				callback: function() {
					$.ajax({
						url: base_url+ajax,
						type:"post",
						data: {"id" : id },
						success:function(){				
							mytr.css("background-color","wheat");
							mytr.css("border-color","wheat");
							mytr.fadeOut(400, function(){
								mytr.remove();
							});	
							//set_num('.num_company','admin/company/get_num');
							//set_num('.num_baby','admin/babys/get_num');
							$.growlUI('刪除成功!');
						}	
					});				
				}
			},				
			cancel: {
				label: "取消",
				className: "btn-default"			
			}
		}
	});			
}
//設置提示通知
function set_num(item,ajax){
	$.getJSON ( base_url+ajax, function(result){
		$(item).text(result.num);
		var sum=0;
		$('.notify').each(function(){		   
			sum+=parseInt($(this).text())
		});
		$('#total_num').text(sum);
	});	
}

$(document).ready(function(){	
	$(".datepicker").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		locale: "zh-TW",
		buttonText : "今天"
	});
	
	/*create_state();
	set_num('.num_company','admin/company/get_num');
	set_num('.num_company_verify','admin/company/get_num_verify');
	set_num('.num_company_contact','admin/company/get_num_contact_notread');
	set_num('.num_baby','admin/babys/get_num');
	set_num('.num_baby_verify','admin/babys/get_num_verify');
	$(".modal-content").draggable();*/
});