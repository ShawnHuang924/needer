				
<div class="page-header">
	<div class="container-fluid">
		<h1>刊登廣告</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('');?>">刊登廣告</a></li>
			<li><a>刊登廣告</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-xs-12">			
			<input type="hidden" id="mytable" value="member">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('advertising/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="company_name">公司名稱</th>		
						<th data-width="10" data-field="contact_name">連絡姓名</th>
						<th data-width="15" data-field="company_web">公司網址</th>
						<th data-width="15" data-field="email">電子信箱</th>
						<th data-width="8" data-field="phone">連絡電話</th>						
						<th data-width="30" data-field="remark">備註詳情</th>
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>								
				
<script>

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'advertising/delete',row.id);
    }
};

</script>