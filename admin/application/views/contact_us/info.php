				
<div class="page-header">
	<div class="container-fluid">
		<h1>連絡我們</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('');?>">連絡我們</a></li>
			<li><a>連絡我們</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-xs-12">			
			<input type="hidden" id="mytable" value="member">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('contact_us/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="contact_name">連絡姓名</th>	
						<th data-width="10" data-field="email">電子信箱</th>	
						<th data-width="10" data-field="phone">連絡電話</th>
						<th data-width="10" data-field="subject">問題主旨</th>
						<th data-width="30" data-field="problem">問題說明</th>
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>								
				
<script>

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'contact_us/delete',row.id);
    }
};

</script>