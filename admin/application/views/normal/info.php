				
<div class="page-header">
	<div class="container-fluid">
		<h1>ㄧ般會員列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('');?>">會員管理</a></li>
			<li><a>ㄧ般會員列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<a href="<?=base_url("normal/create");?>"><button id="new" type="button" class="btn btn-primary">新增ㄧ般會員</button></a>
			<input type="hidden" id="mytable" value="member">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('normal/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="edit" data-sortable="false" data-formatter="editFormatter" data-events="editEvents">編輯</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="status">啟用狀態</th>
						<th data-width="10" data-field="pic" data-visible="false">縮圖</th>
						<th data-width="15" data-field="name">聯絡人</th>
						<th data-width="10" data-field="sex" data-filter-control="select">性別</th>
						<th data-width="15" data-field="phone">電話</th>
						<th data-width="15" data-field="county" data-filter-control="select">居住地</th>
						<th data-width="15" data-field="birthday">生日</th>
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>						
									
				
<script>

window.editEvents = {    
    'click .edits': function (e, value, row, index) {
		var windowObjectReference = window.open('<?=base_url("normal/edit").'/';?>' + row.id, 'normal');
    }
};

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'normal/delete',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();
});	



</script>