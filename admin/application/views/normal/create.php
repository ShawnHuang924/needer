<div class="page-header">
	<div class="container-fluid">
		<h1>新增舞台提供者</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('member');?>">會員管理</a></li>
			<li><a href="<?=base_url('normal/info');?>">一般會員列表</a></li>
			<li><a >新增一般會員</a></li>
		</ul>		
	</div>
</div>	
<div class="info_panel">
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-bottom:0;">
		<li class="active"><a href="#info" data-toggle="tab">會員資料</a></li>				
	</ul>
	<div id="my-tab-content" class="tab-content">	
		<!--廠商資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form" action="<?=base_url('normal/create_normal');?>" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" />
						<div style="width:400px;float:left;">
							<div class="form-group">
								<label class="info_label">email</label>
								<div class="info_input">
									<input type="email" name="account" class="form-control" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">密碼</label>
								<div class="info_input">
									<input type="password" name="password" class="form-control" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">姓</label>
								<div class="info_input">
									<input type="text" name="last_name" class="form-control" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">名</label>
								<div class="info_input">
									<input type="text" name="name" class="form-control " placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">性別</label>
								<div class="info_input">								
									<select name="sex" class="selectpicker" data-width="250">	
										<option value="1" >男生</option>	
										<option value="0" >女生</option>														
									</select>
								</div>					
							</div>
							<div class="form-group">
								<label class="info_label">聯絡電話</label>
								<div class="info_input">
									<input type="text" name="phone" class="form-control" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">居住城市</label>
								<div class="info_input">
									<select name="city" class="selectpicker" data-width="250" data-size="8">	
										<?php foreach($county as $value){ ?>
										<option value="<?=$value['id']?>" ><?=$value['county_name']?></option>
										<?php } ?>								
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">出生日期</label>
								<div class="info_input">
									<input type="text" name="birthday" class="form-control datepicker" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">感興趣主題</label>
								<div class="info_input">							
									<select name="theme[]" class="selectpicker" multiple data-width="250" data-size="8">									
										<?php foreach($theme as $val){ ?>
										<option value="<?=$val['key']?>" ><?=$val['value']?></option>
										<?php } ?>
									</select>
								</div>					
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<button type="submit" class="btn btn-primary create_baby" >建立會員</button>				
							</div>
						</div>					
					</div>				
				</section>			
			</form>
		</div>
		
		
	</div>
</div>	


<script>
$(document).ready(function() {	
	
	$('#twzipcode').twzipcode({});
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});	
	 
});
</script>	