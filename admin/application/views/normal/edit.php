<div class="page-header">
	<div class="container-fluid">
		<h1><?=$info['last_name'].$info['name']?></h1>
		<ul class="breadcrumb">
		<li><a href="<?=base_url('member');?>">會員管理</a></li>
			<li><a href="<?=base_url('normal/info');?>">一般會員列表</a></li>
			<li><a ><?=$info['last_name'].$info['name']?></a></li>
		</ul>
		<p><small>編輯 <?=$info['last_name'].$info['name']?><small></p>
	</div>
</div>	
<div class="info_panel">
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-bottom:0;">
		<li class="active"><a href="#info" data-toggle="tab">個人資料</a></li>
	</ul>
	<div id="my-tab-content" class="tab-content">	
		<!--個人資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form upload_form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" value="<?=$info['id']?>" />
						<div style="width:400px;float:left;">
							<div class="form-group">
								<label class="info_label">email</label>
								<div class="info_input">
									<input type="email" class="form-control" value="<?=$info['account']?>" disabled placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">姓</label>
								<div class="info_input">
									<input type="text" name="last_name" class="form-control" value="<?=$info['last_name']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">名</label>
								<div class="info_input">
									<input type="text" name="name" class="form-control" value="<?=$info['name']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">性別</label>
								<div class="info_input">								
									<select name="sex" class="selectpicker" data-width="250">	
										<option value="1" <?=($info['sex']==1)?'selected':'';?>>男生</option>	
										<option value="0" <?=($info['sex']==0)?'selected':'';?>>女生</option>														
									</select>
								</div>					
							</div>
							<div class="form-group">
								<label class="info_label">聯絡電話</label>
								<div class="info_input">
									<input type="text" name="phone" class="form-control" value="<?=$info['phone']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">居住城市</label>
								<div class="info_input">
									<select name="city" class="selectpicker" data-width="250" data-size="8">	
										<?php foreach($county as $value){ ?>
										<option value="<?=$value['id']?>" <?=($info['county']==$value['id'])?'selected':'';?>><?=$value['county_name']?></option>
										<?php } ?>								
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">出生日期</label>
								<div class="info_input">
									<input type="text" name="birthday" id="birthday" class="form-control" value="<?=$info['birthday']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">感興趣主題</label>
								<div class="info_input">							
									<select name="theme[]" class="selectpicker" multiple data-width="250" data-size="8">									
										<?php foreach($theme as $val){ ?>
										<option value="<?=$val['key']?>" <?=(in_array($val['key'], $themes))?'selected':'';?>><?=$val['value']?></option>
										<?php } ?>
									</select>
								</div>					
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<button type="button" class="btn btn-primary save_info" >儲存</button>				
							</div>
						</div>						
					</div>				
				</section>			
			</form>
		</div>
	</div>
</div>	
	
	
<!-- 上傳前的template -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-upload fade">		
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>
<!-- 上傳完後的template -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-download fade">
	<span class="delete photos_delete" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		<i class="fa fa-times"></i>				
	</span>		
	<img class="target" src="{%=file.url+'?'+(new Date()).getTime()%}" style="width: 100%;height:auto ">						
	</div>
{% } %}
</script>





<script>

function set_detail_type(me) {
    $.ajax({
		url: base_url+'provider/get_detail_type',
		type:"post",
		data: {'id':me.val()},
		dataType: 'json',
		success:function(result){
			console.log(result);
			var my =  me.closest('.stage_block');			
			my.find('select.detail_type').empty();				
			for(var key in result){
				my.find('select.detail_type').append('<option value="">'+result[key]+'</option>');				
			}
			my.find('select.detail_type').selectpicker('refresh');
		}			
	});   
}

$(document).ready(function() {
	$("#birthday").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		value: new Date("<?=$info['birthday']?>"),
		locale: "zh-TW",
		buttonText : "今天"
	});	
	
	$('#twzipcode').twzipcode({	
		<?php if($info['company_district']!=0){?>
		'zipcodeSel': '<?=$info['company_district'];?>'
		<?php }?>
	});
	
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});
	
	$(document).on("click", ".save_info", function() {		
		$.ajax({
			url: base_url+'normal/save_info',
			type:"post",
			data: $(this).closest('form').serialize(),
			dataType: "json",
			success: function(result){
				$.growlUI(result);
			}			
		});
	});
	
	
	create_multiple_upload('#photos_upload','member','<?=$info['id']?>');
	$(document).on('click','.photos_cover',function(){	
		var me=$(this);
		$.ajax({
			url: base_url+'upload/set_cover',
			type:"post",
			data: {
				'id':$('#id').val(),
				'filename':$(this).data('name')				
			},
			success:function(){
				$('.photos_cover').removeClass('is_cover');
				me.addClass('is_cover');
				$.growlUI('設置封面!');				
			}
		});
	});
	
	/*$(document).on('click','#save_photos',function(){	
		$.ajax({
			url: base_url+'upload/resave_photos',
			type:"post",
			data: $('form').serialize(),
			success:function(){
				$.growlUI('儲存成功!');				
			}
		});
	});*/
	
	$(document).on('change', '.stage_type', function() {	
		if($(this).val()=='2'){
			$(this).closest('.stage_block').find('.date1').hide();
			$(this).closest('.stage_block').find('.date2').show();
		}			
		else{
			$(this).closest('.stage_block').find('.date1').show();
			$(this).closest('.stage_block').find('.date2').hide();
		}
		set_detail_type($(this));
	});
	
	$(document).on('click', '.switch', function() {			
		var input = $(this).closest('.form-group').find('.info_input');
		if($(this).data('type')==1){
			$(this).text('選每週固定');
			$(this).data('type',2);
			input.find('div.weeks').attr('disabled',true).hide();			
			input.find('.spec').attr('disabled',false);
			input.find('.e-datewidget').show();
		}else{
			$(this).text('選特定日期');
			$(this).data('type',1);
			input.find('div.weeks').attr('disabled',false).show();
			input.find('.spec').attr('disabled',true);
			input.find('.e-datewidget').hide();			
		}
	});
	
	$(".spec").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		locale: "zh-TW",
		buttonText : "今天"
	});	
	
	
	$('#datetimepicker6').datetimepicker({format: 'HH:mm'});
	$('#datetimepicker7').datetimepicker({format: 'HH:mm'});
	$("#datetimepicker6").on("dp.change", function (e) {
		$('#datetimepicker7').data("DateTimePicker").minDate(e.date);
	});
	$("#datetimepicker7").on("dp.change", function (e) {
		$('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
	});
	
	
	$('.daterange1').datetimepicker({format: 'YYYY-MM'});
	$('.daterange2').datetimepicker({format: 'YYYY-MM'});
	$(".daterange1").on("dp.change", function (e) {
		$('.daterange2').data("DateTimePicker").minDate(e.date);
	});
	$(".daterange2").on("dp.change", function (e) {
		$('.daterange1').data("DateTimePicker").maxDate(e.date);
	});
	
	$(document).on('click', '.new_stage', function() {	
		
	
	});
	
	/*$('.time').timepicker({
		'useSelect': true,
        'showDuration': true,
        'timeFormat': 'g:ia'
    });
	$('.datepair').datepair();*/
	 
});
</script>	
				
					