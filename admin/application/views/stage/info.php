				
<div class="page-header">
	<div class="container-fluid">
		<h1>舞台列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('stage');?>">舞台管理</a></li>
			<li><a>舞台列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<a href="<?=base_url("stage/create");?>"><button id="new" type="button" class="btn btn-primary">新增舞台</button></a>
			<input type="hidden" id="mytable" value="stage_info">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('stage/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="edit" data-sortable="false" data-formatter="editFormatter" data-events="editEvents">編輯</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="status">啟用狀態</th>			
						<th data-width="15" data-field="company_name">公司名稱</th>
						<th data-width="10" data-field="stage" data-filter-control="select">舞台類型</th>
						<th data-width="15" data-field="detail_type">偏好類別</th>
						<th data-width="15" data-field="activity_date">活動日期</th>
						<th data-width="15" data-field="activity_time">活動時段</th>
						<th data-width="15" data-field="length" data-filter-control="select">表演時間長度</th>	
						<th data-width="20" data-field="exhibition_date" data-visible="false">展覽日期</th>
						<th data-width="15" data-field="week_length" data-visible="false">展期長度</th>
						<th data-width="15" data-field="remark">備註事項</th>
						<th data-width="15" data-field="other_remark">其他備註事項</th>						
					</tr>
				</thead>
			</table>
		</div>	
	</div>	
</div>						
									
				
<script>

window.editEvents = {    
    'click .edits': function (e, value, row, index) {
		var windowObjectReference = window.open('<?=base_url("stage/edit").'/';?>' + row.id, 'stage');
    }
};

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'stage/delete',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();
});	



</script>