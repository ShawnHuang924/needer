<div class="page-header">
	<div class="container-fluid">
		<h1>新增廠商</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('company');?>">寶貝管理</a></li>
			<li><a href="<?=base_url('company/info');?>">廠商列表</a></li>
			<li><a >新增廠商</a></li>
		</ul>		
	</div>
</div>	
<div class="info_panel">
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-bottom:0;">
		<li class="active"><a href="#info" data-toggle="tab">廠商資料</a></li>				
	</ul>
	<div id="my-tab-content" class="tab-content">	
		<!--廠商資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form" action="<?=base_url('company/create_company');?>" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" />
						<div class="form-group">
							<label class="info_label">email</label>
							<div style="float:left;">
								<input type="email" name="email" class="form-control" placeholder="必填" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">密碼</label>
							<div style="float:left;">
								<input type="password" name="password" class="form-control" placeholder="必填" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司名稱</label>
							<div style="float:left;">
								<input type="text" name="company_name" class="form-control" placeholder="必填" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司電話</label>
							<div style="float:left;">
								<input type="text" name="tel" class="form-control"/>
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司地址</label>
							<div id="twzipcode">
								<div data-role="county"
									 data-name="county"
									 data-style="county-district selectpicker"
									 style="float: left;"></div>							
								<div data-role="district"
									 data-name="district"
									 data-style="county-district selectpicker"
									 style="float: left;"></div>
								<div data-role="zipcode"
									 data-style="zipcode-style"
									 style="display:none;"></div>
							</div>
							<div style="float:left;">
								<input type="text" name="address" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人</label>
							<div style="float:left;">
								<input type="text" name="contact_name" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人職稱</label>
							<div style="float:left;">
								<input type="text" name="contact_title" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人電話</label>
							<div style="float:left;">
								<input type="text" name="contact_phone" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">創立日期</label>
							<div style="float:left;">
								<input id="founded_date" type="text" name="founded_date" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">產業類別</label>
							<div style="float:left;">
								<select name="industry" class="selectpicker" data-width="120" data-live-search="true" data-size="8">	
									<?php foreach($industry as $value){ ?>
									<option value="<?=$value['industry_code']?>" ><?=$value['industry_name']?></option>
									<?php } ?>								
								</select>
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">註冊日期</label>
							<div style="float:left;">
								<input id="register_date" type="text" name="register_date" class="form-control"  />
							</div>						
						</div>
						<!--<div class="form-group">
							<label class="info_label">付費日期</label>
							<div style="float:left;">
								<input type="text" name="pay_date" class="form-control" placeholder="必填" value="" required />
							</div>						
						</div>-->
						<div class="form-group">
							<label class="info_label">影片</label>
							<div style="float:left;">
								<input type="url" name="youtube" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">臉書</label>
							<div style="float:left;">
								<input type="url" name="fb" class="form-control" />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label"></label>
							<button type="submit" class="btn btn-primary create_baby" >建立廠商，並繼續編輯照片</button>				
						</div>
					</div>				
				</section>			
			</form>
		</div>
		
		
	</div>
</div>	


<script>
$(document).ready(function() {

	$("#founded_date").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		locale: "zh-TW",
		buttonText : "今天"
	});
	
	$("#register_date").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		locale: "zh-TW",
		buttonText : "今天"
	});
	
	$('#twzipcode').twzipcode({});
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});	
	 
});
</script>	