<div class="page-header">
	<div class="container-fluid">
		<h1><?=$info['company_name']?></h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('company');?>">廠商管理</a></li>
			<li><a href="<?=base_url('company/info');?>">廠商列表</a></li>
			<li><a ><?=$info['company_name']?></a></li>
		</ul>
		<p><small>編輯<?=$info['company_name']?><small></p>
	</div>
</div>	
<div class="info_panel">
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-bottom:0;">
		<li class="active"><a href="#info" data-toggle="tab">個人資料</a></li>
		<li><a href="#cover" data-toggle="tab">封面照</a></li>
		<li><a href="<?=base_url('company/photos/'.$info['id']);?>">相簿</a></li> 
		<li><a href="#permission" data-toggle="tab">權限設定</a></li>
	</ul>
	<div id="my-tab-content" class="tab-content">	
		<!--個人資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form upload_form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" value="<?=$info['id']?>" />
						<div class="form-group">
							<label class="info_label">email</label>
							<div style="float:left;">
								<input type="text" class="form-control" disabled value="<?=$info['email']?>" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司名稱</label>
							<div style="float:left;">
								<input type="text" name="company_name" class="form-control" placeholder="必填" value="<?=$info['company_name']?>" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司電話</label>
							<div style="float:left;">
								<input type="text" name="tel" class="form-control" value="<?=$info['tel']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">公司地址</label>
							<div id="twzipcode">
								<div data-role="county"
									 data-name="county"
									 data-style="county-district selectpicker"
									 style="float: left;"></div>							
								<div data-role="district"
									 data-name="district"
									 data-style="county-district selectpicker"
									 style="float: left;"></div>
								<div data-role="zipcode"
									 data-style="zipcode-style"
									 style="display:none;"></div>
							</div>
							<div style="float:left;">
								<input type="text" name="address" class="form-control"  value="<?=$info['address']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人</label>
							<div style="float:left;">
								<input type="text" name="contact_name" class="form-control" value="<?=$info['contact_name']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人職稱</label>
							<div style="float:left;">
								<input type="text" name="contact_title" class="form-control" value="<?=$info['contact_title']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">聯絡人電話</label>
							<div style="float:left;">
								<input type="text" name="contact_phone" class="form-control" value="<?=$info['contact_phone']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">創立日期</label>
							<div style="float:left;">
								<input id="founded_date" type="text" name="founded_date" class="form-control" placeholder="必填" value="<?=$info['founded_date']?>" required />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">產業類別</label>
							<div style="float:left;">
								<select name="industry" class="selectpicker" data-width="120" data-live-search="true" data-size="8" >	
									<?php foreach($industry as $value){ ?>
									<option value="<?=$value['industry_code']?>" <?=($info['industry']==$value['industry_code'])?'selected':'';?>><?=$value['industry_name']?></option>
									<?php } ?>								
								</select>
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">註冊日期</label>
							<div style="float:left;">
								<input id="register_date" type="text" name="register_date" class="form-control"  value="<?=$info['register_date']?>"  />
							</div>						
						</div>
						<!--<div class="form-group">
							<label class="info_label">啟用付費功能日期</label>
							<div style="float:left;">
								<input type="text" name="pay_date" class="form-control" placeholder="必填" value="" required />
							</div>						
						</div>-->
						<div class="form-group">
							<label class="info_label">影片</label>
							<div style="float:left;">
								<input type="text" name="youtube" class="form-control"  value="<?=$info['youtube']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label">臉書</label>
							<div style="float:left;">
								<input type="text" name="fb" class="form-control" value="<?=$info['fb']?>"  />
							</div>						
						</div>
						<div class="form-group">
							<label class="info_label"></label>
							<button type="button" class="btn btn-primary save_info" >儲存</button>				
						</div>
					</div>				
				</section>			
			</form>
		</div>
		<!--封面照-->
		<div class="tab-pane" id="cover">
		   <form class="form-horizontal tasi-form upload_form" action="<?=base_url('company/save_pic');?>" method="post" data-toggle="validator" role="form" id="form_cover">
				<section class="panel">						
					<div class="panel-body mycrop">
						<input type="hidden" class="is_cut" value="0">	
						<input type="hidden" name="type" value="company" />
						<input type="hidden" name="id" value="<?=$info['id']?>" />
						<input type="hidden" name="folder" value="cover" />		
						
						<div class="edit_pic" style="min-height:300px">  
							<div class="files" role="presentation"></div>						
							<div class="new_pic">							
								請將照片拖曳至此 或 點選新增照片
								<span class="btn fileinput-button fileinput_all" >
									<input type="file" name="files[]" accept="image/*" style="cursor:pointer;">
								</span>
							</div>
						</div>
						<span class="help-block with-errors"></span>
						<input type="hidden" class="x" name="x" />
						<input type="hidden" class="y" name="y" />
						<input type="hidden" class="w" name="w" />
						<input type="hidden" class="h" name="h" />
						<input type="hidden" class="now_w" name="now_w" />
						<input type="hidden" class="origin_w" name="origin_w" />
						<input type="hidden" class="cover_ratio" name="cover_ratio" value="<?=(1/1);?>" />	
						
						<label class="info_label"></label>
						<button type="button" class="btn btn-primary btnDoCrop" disabled>選取</button>
						<button type="button" class="btn btn-primary cropfinish" data-dir="company-cover" disabled>裁剪</button>
						<button type="button" class="btn btn-primary save_cover"  >儲存</button>				
						
					</div>
				</section>	
			</form>	   
		</div>
		<!--權限設定-->
		<div class="tab-pane" id="permission">		  
			<section class="panel">						
				<div class="panel-body">
					
				</div>
			</section>				 
		</div>
	</div>
</div>	
	
	
<!-- 上傳前的template -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-upload fade">		
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>
<!-- 上傳完後的template -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-download fade">
	<span class="delete photos_delete" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		<i class="fa fa-times"></i>				
	</span>		
	<img class="target" src="{%=file.url+'?'+(new Date()).getTime()%}" style="width: 100%;height:auto ">						
	</div>
{% } %}
</script>





<script>
$(document).ready(function() {
	create_single_upload('company-cover','#form_cover');
	$(document).on('click','.btnDoCrop',function(){	
		myblock = $(this).closest('.mycrop');
		$(this).attr('disabled',true);
		myblock.find('.target').Jcrop({
			onSelect: cropSelect,
			onChange: cropChange,
			aspectRatio: 1/1
		},function(){
			jcrop_api = this;
		});
	});	
	
	$("#founded_date").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		value: new Date("<?=$info['founded_date'];?>"),
		locale: "zh-TW",
		buttonText : "今天"
	});
	
	$("#register_date").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		value: new Date("<?=$info['register_date'];?>"),
		locale: "zh-TW",
		buttonText : "今天"
	});
	
	$('#twzipcode').twzipcode({	
		<?php if($info['zipcode']!=0){?>
		'zipcodeSel': '<?=$info['zipcode'];?>'
		<?php }?>
	});
	
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});
	$(document).on("click", ".save_cover", function() {		
		$.ajax({
			url: base_url+'company/save_pic',
			type:"post",
			data: $(this).closest('form').serialize(),
			success: function(){
				$.growlUI('儲存成功!');
			}			
		});
	});
	
	$(document).on("click", ".save_info", function() {		
		$.ajax({
			url: base_url+'company/save_info',
			type:"post",
			data: $(this).closest('form').serialize(),
			dataType: "json",
			success: function(result){
				$.growlUI(result);
			}			
		});
	});
	 
});
</script>	
				
					