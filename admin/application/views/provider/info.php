				
<div class="page-header">
	<div class="container-fluid">
		<h1>提供舞台者列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('');?>">會員管理</a></li>
			<li><a>提供舞台者列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-xs-12">
			<a href="<?=base_url("provider/create");?>"><button id="new" type="button" class="btn btn-primary">新增提供舞台者</button></a>
			<input type="hidden" id="mytable" value="member">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('provider/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="edit" data-sortable="false" data-formatter="editFormatter" data-events="editEvents">編輯</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="status">啟用狀態</th>
						<th data-width="10" data-field="pic" data-visible="false">縮圖</th>	
						<th data-width="8"  data-field="company_code" data-filter-control="input">代號</th>	
						<th data-width="18" data-field="company_name" data-filter-control="input">公司名稱</th>
						<th data-width="10" data-field="company_type" data-filter-control="select">公司類型</th>
						<th data-width="10" data-field="company_district" data-filter-control="select">公司縣市</th>
						<th data-width="30" data-field="company_address" data-visible="false">詳細地址</th>		
						<th data-width="20" data-field="company_web" data-visible="false">公司網址</th>
						<th data-width="12" data-field="name" data-filter-control="input">聯絡人</th>
						<th data-width="10" data-field="sex" data-filter-control="select">性別</th>
						<th data-width="15" data-field="phone">電話</th>
						<th data-width="12" data-field="county" data-filter-control="select">居住地</th>
						<th data-width="15" data-field="birthday">生日</th>
						<th data-width="5" data-field="preview">預覽</th>						
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>						
									
				
<script>

window.editEvents = {    
    'click .edits': function (e, value, row, index) {
		var windowObjectReference = window.open('<?=base_url("provider/edit").'/';?>' + row.id, 'provider');
    }
};

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'provider/delete',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();
	create_vip_state();	
});	



</script>