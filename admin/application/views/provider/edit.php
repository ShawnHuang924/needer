<div class="page-header">
	<div class="container-fluid">
		<h1><?=$info['company_name']?></h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('member');?>">會員管理</a></li>
			<li><a href="<?=base_url('provider/info');?>">舞台提供者列表</a></li>
			<li><a ><?=$info['company_name']?></a></li>
		</ul>
		<p><small>編輯 <?=$info['company_name']?><small></p>
	</div>
</div>	
<div class="info_panel">
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-bottom:0;">
		<li class="active"><a href="#info" data-toggle="tab">個人資料</a></li>
		<li><a href="#photos" data-toggle="tab">相簿</a></li>
		<li><a href="#stages" data-toggle="tab">舞台</a></li>
	</ul>
	<div id="my-tab-content" class="tab-content">	
		<!--個人資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form upload_form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" value="<?=$info['id']?>" />
						<div style="width:400px;float:left;">
							<div class="form-group">
								<label class="info_label">email</label>
								<div class="info_input">
									<input type="email" name="account" class="form-control" value="<?=$info['account']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">姓</label>
								<div class="info_input">
									<input type="text" name="last_name" class="form-control" value="<?=$info['last_name']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">名</label>
								<div class="info_input">
									<input type="text" name="name" class="form-control" value="<?=$info['name']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">性別</label>
								<div class="info_input">								
									<select name="sex" class="selectpicker" data-width="250">	
										<option value="1" <?=($info['sex']==1)?'selected':'';?>>男生</option>	
										<option value="0" <?=($info['sex']==0)?'selected':'';?>>女生</option>														
									</select>
								</div>					
							</div>
							<div class="form-group">
								<label class="info_label">聯絡電話</label>
								<div class="info_input">
									<input type="text" name="phone" class="form-control" value="<?=$info['phone']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">居住城市</label>
								<div class="info_input">
									<select name="city" class="selectpicker" data-width="250" data-size="8">	
										<?php foreach($county as $value){ ?>
										<option value="<?=$value['id']?>" <?=($info['county']==$value['id'])?'selected':'';?>><?=$value['county_name']?></option>
										<?php } ?>								
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">出生日期</label>
								<div class="info_input">
									<input type="text" name="birthday" id="birthday" class="form-control" value="<?=$info['birthday']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">感興趣主題</label>
								<div class="info_input">							
									<select name="theme[]" class="selectpicker" multiple data-width="250" data-size="8">									
										<?php foreach($theme as $val){ ?>
										<option value="<?=$val['key']?>" <?=(in_array($val['key'], $themes))?'selected':'';?>><?=$val['value']?></option>
										<?php } ?>
									</select>
								</div>					
							</div>
						</div>
						<div style="width:600px;float:left;">
							<div class="form-group">
								<label class="info_label">公司名稱</label>
								<div class="info_input">
									<input type="text" name="company_name" class="form-control" value="<?=$info['company_name']?>" placeholder="必填" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">公司類型</label>
								<div class="info_input">
									<select name="company_type" class="selectpicker" data-width="250" data-size="8" required>	
										<?php foreach($company_type as $value){ ?>
										<option value="<?=$value['key']?>" <?=($info['company_type']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
										<?php } ?>								
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">公司地址</label>
									<div id="twzipcode">
									<div data-role="county"
										 data-name="county"
										 data-style="county-district selectpicker"
										 style="float:left;"></div>							
									<div data-role="district"
										 data-name="district"
										 data-style="county-district selectpicker"
										 style="float:left;"></div>
									<div data-role="zipcode"
										 data-name="company_district"
										 data-style="zipcode-style"
										 style="display:none;"></div>
								</div>
							</div>	
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input" style="width:390px;">
									<input type="text" name="company_address" value="<?=$info['company_address']?>" class="form-control"  placeholder="必填" required />
								</div>							
							</div>
							<div class="form-group">
								<label class="info_label">公司網址</label>
								<div class="info_input" style="width:390px;">
									<input type="url" name="company_web" value="<?=$info['company_web']?>" class="form-control" />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">希望被表演者之聯繫方式 *</label>
								<div class="info_input" >
									<select name="contact" class="selectpicker" data-width="250">	
										<?php foreach($contact as $value){ ?>
										<option value="<?=$value['key']?>" <?=($info['contact']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
										<?php } ?>
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">希望被聯繫時段 *</label>
								<div class="info_input" >
									<div class="input-group " >
										<input type="text" name="contact_start_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_start_time']));?>">
										<span class="input-group-addon">to</span>
										<input type="text" name="contact_end_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_end_time']));?>">
									</div>					
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">可供應之項目</label>
								<div class="info_input">
									<select name="supply_item[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
										<?php foreach($supply_item as $key => $value){ ?>
										<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_items))?'selected':'';?>><?=$value['value']?></option>
										<?php } ?>	
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input">
									<input type="text" name="other_item" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_item']?>" />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">可提供之設備</label>
								<div class="info_input">
									<select name="supply_device[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
										<?php foreach($supply_device as $key => $value){ ?>
										<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_devices))?'selected':'';?>><?=$value['value']?></option>
										<?php } ?>	
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input">
									<input type="text" name="other_device" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_device']?>" />
								</div>						
							</div>
							
							
							
							
							
							
							
							
							<div class="form-group">
								<label class="info_label"></label>
								<button type="button" class="btn btn-primary save_info" >儲存</button>				
							</div>
						</div>
					</div>				
				</section>			
			</form>
		</div>
		<!--相簿-->
		<div class="tab-pane" id="photos">	
			<!--<button type="button" class="btn btn-primary" id="save_photos" style="position:absolute;right:37px;top:146px;" >全部照片重新存檔</button>-->
			<section class="panel">						
				<div class="panel-body">					
					<form id="photos_upload" method="POST" enctype="multipart/form-data">
						<input type="hidden" id="type" name="type" value="company">
						<input type="hidden" id="id" name="id" value="<?=$info['id'];?>">
						<span class="fileinput-button edit_btn btn_pink col-lg-3 col-md-4 col-sm-4 " style="top:0px;padding:0 5px 0 5px;margin-bottom:10px;">
							<div class="stretchy-wrapper" style="background: #eee;">								
								<span style="position:absolute;top:calc(50% - 27px);left:calc(50% - 59px);font-size:14px;text-align:center">請將照片拖曳至此<br>或<br>點選新增照片</span>
								<input type="file" name="files[]" multiple  accept="image/*" style="cursor:pointer;bottom: 0;">
							</div>
						</span>						
						<div class="files" role="presentation"></div>					
					</form>	
				</div>				
			</section>	
		</div>
		<!--舞台-->
		<div class="tab-pane" id="stages">
			<section class="panel">						
				<div class="panel-body">					
					<div class="col-lg-4 col-md-6 col-sm-12 stage_block">
						<form class="form-horizontal tasi-form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
							<input type="hidden" name="id" value="<?=$info['id']?>" />
							<div class="form-group">
								<label class="info_label">舞台類型</label>
								<div class="info_input">							
									<select name="stage_type" class="selectpicker stage_type" data-width="250" data-size="8" required>	
										<?php foreach($stage_type as $value){ ?>
										<option value="<?=$value['id']?>"><?=$value['type']?></option>
										<?php } ?>								
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">偏好類別</label>
								<div class="info_input">
									<select name="detail_type[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
										<?php foreach($detail_type as $key => $val){ ?>
										<option value="<?=$val?>" ><?=$val?></option>
										<?php } ?>
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input">
									<input type="text" name="other_detail_type" class="form-control myinput" placeholder="其他偏好"  />
								</div>						
							</div>
							<div class="date1">
								<div class="form-group">
									<label class="info_label">活動日期</label>
									<div class="info_input">							
										<select name="activity_date[]" class="selectpicker weeks" multiple data-width="250" data-size="8">									
											<?php foreach($week as $val){ ?>
											<option value="<?=$val['value']?>"><?=$val['value']?></option>
											<?php } ?>
										</select>
										<input type="text" name="spec_date" class="form-control spec"  disabled class="form-control" placeholder="必填" required />
									</div>
									<button type="button" data-type="1" class="btn btn-info switch" style="margin-left:15px;">選特定日期</button>
								</div>
								<div class="form-group">
									<label class="info_label">活動時段</label>
									<div class="info_input" >
										<div class='input-group date' id='datetimepicker6' style="width:50%;float:left;">
											<input type='text' name="start_time" class="form-control myinput" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>	
										<div class='input-group date' id='datetimepicker7' style="width:50%;float:left;">
											<input type='text' name="end_time" class="form-control myinput" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>			
									</div>						
								</div>
								<div class="form-group">
									<label class="info_label">表演時間長度</label>
									<div class="info_input">
										<select name="length" class="selectpicker" data-width="250" data-size="8" required>	
											<option value="30分鐘">30分鐘</option>
											<option value="60分鐘">60分鐘</option>
											<option value="90分鐘">90分鐘</option>
											<option value="120分鐘">120分鐘</option>
											<option value="大於120分鐘">大於120分鐘</option>								
										</select>
									</div>						
								</div>
							</div>
							<div class="date2" style="display:none">
								<div class="form-group">
									<label class="info_label">展覽日期</label>
									<div class="info_input" >							
										<div class='input-group daterange1' style="width:50%;float:left;">
											<input type='text' name="start_date" class="form-control" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>	
										<div class='input-group daterange2' style="width:50%;float:left;">
											<input type='text' name="end_date" class="form-control" />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>	
									</div>						
								</div>
								<div class="form-group">
									<label class="info_label">展期長度</label>
									<div class="info_input">
										<select name="week_length" class="selectpicker" data-width="250" data-size="8">									
											<?php for($i=1;$i<=12;$i++){ ?>
											<option value="<?=$i?>週"><?=$i?>週</option>
											<?php } ?>
										</select>
									</div>						
								</div>
							</div>
							<div class="form-group">
								<label class="info_label">備註事項</label>
								<div class="info_input">
									<select name="remark[]" class="selectpicker" multiple data-width="250" data-size="8">										
										<option value="提供影音連結或檔案" >提供影音連結或檔案</option>
										<option value="提供作品集" >提供作品集</option>
									</select>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input">
									<input type="text" name="other_remark" class="form-control myinput" value="" placeholder="其他備註事項"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<button type="button" class="btn btn-primary new_stage" >新增舞台</button>			
							</div>
						</form>		
					</div>					
				</div>				
			</section>	
		</div>
	</div>
</div>	

	
<!-- 上傳前的template -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="col-lg-3 col-md-4 col-sm-4 template-upload fade" style="padding:0 5px 0 5px;margin-bottom:10px;">			
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>

<!-- 上傳完後的template -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}    
    <div class="col-lg-3 col-md-4 col-sm-4 template-download fade" data-key="" style="padding:0 5px 0 5px;margin-bottom:10px;">	
		<div class="stretchy-wrapper" style="">
			<div class="photo" style="background-image: url({%=file.url%});"></div>			
		</div>
		<span class="photos_cover" data-name="{%=file.name%}">設為封面</span>	 
		<span class="delete photos_delete" style="top:1px;right: 6px;" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			<i class="fa fa-times"></i>	
			<input type="hidden" name="filename[]" value="{%=file.name%}">	
		</span>	 
    </div>
{% } %}
</script>


<script>

function set_detail_type(me) {
    $.ajax({
		url: base_url+'provider/get_detail_type',
		type:"post",
		data: {'id':me.val()},
		dataType: 'json',
		success:function(result){
			console.log(result);
			var my =  me.closest('.stage_block');			
			my.find('select.detail_type').empty();				
			for(var key in result){
				my.find('select.detail_type').append('<option value="'+result[key]+'">'+result[key]+'</option>');				
			}
			my.find('select.detail_type').selectpicker('refresh');
		}			
	});   
}

$(document).ready(function() {
	$("#birthday").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		value: new Date("<?=$info['birthday']?>"),
		locale: "zh-TW",
		buttonText : "今天"
	});	
	
	$('#twzipcode').twzipcode({	
		<?php if($info['company_district']!=0){?>
		'zipcodeSel': '<?=$info['company_district'];?>'
		<?php }?>
	});
	
	$('.clockpicker').clockpicker({
		placement: 'top',
		align: 'left',
		autoclose: true
	});
	
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});
	
	$(document).on("click", ".save_info", function() {		
		$.ajax({
			url: base_url+'provider/save_info',
			type:"post",
			data: $(this).closest('form').serialize(),
			dataType: "json",
			success: function(result){
				$.growlUI(result);
			}			
		});
	});
	
	
	create_multiple_upload('#photos_upload','member','<?=$info['id']?>');
	$(document).on('click','.photos_cover',function(){	
		var me=$(this);
		$.ajax({
			url: base_url+'upload/set_cover',
			type:"post",
			data: {
				'id':$('#id').val(),
				'filename':$(this).data('name')				
			},
			success:function(){
				$('.photos_cover').removeClass('is_cover');
				me.addClass('is_cover');
				$.growlUI('設置封面!');				
			}
		});
	});
	
	
	
	$(document).on('change', '.stage_type', function() {	
		if($(this).val()=='2'){
			$(this).closest('.stage_block').find('.date1').hide();
			$(this).closest('.stage_block').find('.date2').show();
		}			
		else{
			$(this).closest('.stage_block').find('.date1').show();
			$(this).closest('.stage_block').find('.date2').hide();
		}
		set_detail_type($(this));
	});
	
	$(document).on('click', '.switch', function() {			
		var input = $(this).closest('.form-group').find('.info_input');
		if($(this).data('type')==1){
			$(this).text('選每週固定');
			$(this).data('type',2);
			input.find('div.weeks').attr('disabled',true).hide();			
			input.find('.spec').attr('disabled',false);
			input.find('.e-datewidget').show();
		}else{
			$(this).text('選特定日期');
			$(this).data('type',1);
			input.find('div.weeks').attr('disabled',false).show();
			input.find('.spec').attr('disabled',true);
			input.find('.e-datewidget').hide();			
		}
	});
	
	$(".spec").ejDatePicker({  
		dateFormat: "yyyy-MM-dd",
		startLevel: ej.DatePicker.Level.Decade,
		maxDate: new Date(),
		locale: "zh-TW",
		buttonText : "今天"
	});	
	
	
	$('#datetimepicker6').datetimepicker({format: 'HH:mm'});
	$('#datetimepicker7').datetimepicker({format: 'HH:mm'});
	$("#datetimepicker6").on("dp.change", function (e) {
		$('#datetimepicker7').data("DateTimePicker").minDate(e.date);
	});
	$("#datetimepicker7").on("dp.change", function (e) {
		$('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
	});
	
	
	$('.daterange1').datetimepicker({format: 'YYYY-MM'});
	$('.daterange2').datetimepicker({format: 'YYYY-MM'});
	$(".daterange1").on("dp.change", function (e) {
		$('.daterange2').data("DateTimePicker").minDate(e.date);
	});
	$(".daterange2").on("dp.change", function (e) {
		$('.daterange1').data("DateTimePicker").maxDate(e.date);
	});
	
	$(document).on('click', '.new_stage', function() {	
		$.ajax({
			url: base_url+'provider/save_stage',
			type:"post",
			data: $(this).closest('form').serialize(),
			success:function(){
				$('#stages .selectpicker').selectpicker('val', '');	
				$('#stages .myinput').val('');				
				$.growlUI('儲存成功!');				
			}
		});
	});
	
	/*$('.time').timepicker({
		'useSelect': true,
        'showDuration': true,
        'timeFormat': 'g:ia'
    });
	$('.datepair').datepair();*/
	 
});
</script>	
				
					