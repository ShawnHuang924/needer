				
<div class="page-header">
	<div class="container-fluid">
		<h1>VIP廠商審核</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('company');?>">網站項目管理</a></li>
			<li><a>VIP廠商審核</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<input type="hidden" id="mytable" value="industry">
			<input type="hidden" id="pk" value="industry_code">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('company/get_company_vip_verify');?>"
					data-sort-name="sort">			
				<thead>			
					<tr>
						<th data-width="20" data-field="industry_code">審核通過</th>	
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除此案</th>
						<th data-width="15" data-field="company_name">公司名稱</th>
						<th data-width="10" data-field="tel">公司電話</th>
						<th data-width="30" data-field="address">公司地址</th>
						<th data-width="10" data-field="contact_name">聯絡人</th>
						<th data-width="10" data-field="contact_title">聯絡人職稱</th>											
						<th data-width="10" data-field="contact_phone">聯絡人電話</th>
						<th data-width="15" data-field="email">email</th>
						<th data-width="10" data-field="register_date">註冊日期</th>
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>						
									
				
<script>
window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'site_item/delete_industry',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();					
});	
</script>				

