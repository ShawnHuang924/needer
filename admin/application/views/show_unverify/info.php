				
<div class="page-header">
	<div class="container-fluid">
		<h1>展演列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('show');?>">展演管理</a></li>
			<li><a>展演列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<!--<a href="<?=base_url("show/create");?>"><button id="new" type="button" class="btn btn-primary">新增展演</button></a>-->
			<input type="hidden" id="mytable" value="show_info">
			<input type="hidden" id="myid" >
			<table id="eventsTable"
					data-toggle="table"
					data-url="<?=base_url('show_unverify/get_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>
						<th data-width="8"  data-field="edit" data-sortable="false" data-formatter="editFormatter" data-events="editEvents">編輯</th>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="10" data-field="status">啟用狀態</th>	
						<th data-width="20" data-field="show_code">展演代號</th>
						<th data-width="30" data-field="activity_name">活動名稱</th>
						<th data-width="10" data-field="type" data-filter-control="select">展演類型</th>
						<th data-width="20" data-field="dates">展演日期</th>
						<th data-width="20" data-field="activity_date">展演時段</th>
						<th data-width="15" data-field="price">票價</th>
						<th data-width="15" data-field="name">表演者</th>	
						<th data-width="15" data-field="county_name">縣市</th>
						<th data-width="5" data-field="preview">預覽</th>						
					</tr>
				</thead>
			</table>
		</div>	
	</div>	
</div>						
									
				
<script>
/*
window.viewEvents = {    
    'click .views': function (e, value, row, index) {
		var windowObjectReference = window.open('<?=base_url("show/view").'/';?>' + row.id, 'show');
    }
};
*/
window.editEvents = {    
    'click .edits': function (e, value, row, index) {
		var windowObjectReference = window.open('<?=base_url("show_unverify/edit").'/';?>' + row.id, 'show');
    }
};

window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'show/delete',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();
});	



</script>