<div class="page-header">
	<div class="container-fluid">
		<h1><?=$info['activity_name']?></h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('show_unverify');?>">展演管理</a></li>
			<li><a href="<?=base_url('show_unverify/info');?>">展演列表(未審核)</a></li>
			<li><a ><?=$info['activity_name']?></a></li>
		</ul>
		<p><small>編輯<?=$info['activity_name']?><small></p>
	</div>
</div>	
<div class="info_panel">
	
	<div id="my-tab-content" class="tab-content">	
		<!--個人資料-->
		<div class="tab-pane active" id="info">
			<form class="form-horizontal tasi-form upload_form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
				<section class="panel">						
					<div class="panel-body">
						<input type="hidden" name="id" value="<?=$info['id']?>" />
						<div style="width:400px;float:left;">
							<div class="form-group">
								<label class="info_label">活動名稱</label>
								<div class="info_input">
									<input type="text" name="activity_name" class="form-control" disabled value="<?=$info['activity_name']?>" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">演出者</label>
								<div class="info_input">
									<input type="text" name="performer_name" class="form-control" disabled placeholder="必填" value="<?=$info['performer_name']?>" required />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">類型</label>
								<div class="info_input">
									<?php foreach( $stage_type as $key => $val ){?>	
									<?php if($info['show_type']==$val['key']){?>
									<input type="text" name="show_type" class="form-control" disabled value="<?=$val['value']?>"  />
									<?php }}?>
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">開始日期</label>
								<div class="info_input">
									<input type="text" name="start_date" class="form-control" disabled value="<?=$info['start_date']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">結束日期</label>
								<div class="info_input">
									<input type="text" name="end_date" class="form-control" disabled value="<?=$info['end_date']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">開始時間</label>
								<div class="info_input">
									<input type="text" name="start_time" class="form-control" disabled value="<?=$info['start_time']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">結束時間</label>
								<div class="info_input">
									<input type="text" name="end_time" class="form-control" disabled value="<?=$info['end_time']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">票價</label>
								<div class="info_input">
									<input type="text" name="price" class="form-control" disabled value="<?=$info['price']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">最低消費</label>
								<div class="info_input">
									<input type="text" name="price_limit" class="form-control" disabled value="<?=$info['price_limit']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">介紹</label>
								<div class="info_input">
									<input type="text" name="intro" class="form-control" disabled value="<?=$info['intro']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">表演者id</label>
								<div class="info_input">
									<input type="text" name="member_id" class="form-control" disabled value="<?=$info['member_id']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label">結束時間</label>
								<div class="info_input">
									<input type="text" name="end_time" class="form-control" disabled value="<?=$info['end_time']?>"  />
								</div>						
							</div>														
							<div class="form-group">
								<label class="info_label">店家地址</label>
								<div class="info_input">
									<input type="text" name="end_time" class="form-control" disabled value="<?=$info['end_time']?>"  />
								</div>						
							</div>
							<div class="form-group">
								<label class="info_label"></label>
								<div class="info_input">
									<button type="button" class="btn btn-primary save_info" style="">審核通過</button>
									<button type="button" class="btn btn-primary " style="">審核失敗</button>									
								</div>

							</div>
						</div>
					</div>
				</section>			
			</form>
		</div>
		
	</div>
</div>	
	
	

<script>
$(document).ready(function() {
	


	
	$(document).on("click", ".save_info", function() {	
		$.ajax({
			url: base_url+'show_unverify/save_info',
			type:"post",
			data: $(this).closest('form').serialize(),
			dataType: "json",
			success: function(result){
				$.growlUI(result);
				window.location.href = base_url+'show_unverify/info';
			}			
		});
	});
	 
});
</script>	
				
					