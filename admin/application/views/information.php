
			
					<div class="page-header">
						<div class="container-fluid">
							<h1>主機資訊</h1>
							<ul class="breadcrumb">
								<li><a href="#">系統管理</a></li>
								<li><a href="#">主機資訊</a></li>
							</ul>
						</div>
					</div>					
					<div class="container-fluid">						
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
							<table class="table table-bordered table-striped" style="width:500px">						
								<tbody>
									<tr>
										<td style="width:150px">網域名稱</td>
										<td><?php echo $_SERVER["SERVER_NAME"]; ?></td>
									</tr>
									<tr>
										<td>IP地址</td>
										<td><?php if('/'==DIRECTORY_SEPARATOR){echo $_SERVER['SERVER_ADDR'];}else{echo @gethostbyname($_SERVER['SERVER_NAME']);} ?></td>
									</tr>
									<tr>
										<td>作業系統</td>
										<td><?php echo php_uname("s"); ?></td>								
									</tr>
									<tr>
										<td>網頁伺服器軟體</td>								
										<td><?php echo $_SERVER["SERVER_SOFTWARE"]; ?></td>
									</tr>
									<tr>
										<td>資料庫</td>
										<td><?php echo 'MySQL '.$this->db->version(); ?></td>
									</tr>
									<tr>
										<td>後端語言</td>
										<td><?php echo 'PHP '.phpversion(); ?></td>
									</tr>
									<tr>
										<td>Framework</td>
										<td><?php echo 'CodeIgniter  '.CI_VERSION; ?></td>
									</tr>
									<tr>
										<td>GD圖型函式庫</td>
										<td><?php $gd_info = @gd_info(); echo (function_exists("gd_info")) ? @$gd_info['GD Version'] : "<font style='color: #F14E32;'>若無此函示庫，將無法使用上傳圖片功能</font>";?></td>
									</tr>
									<tr>
										<td>網站目錄位置</td>
										<td><?php $len = strlen(BASEPATH); echo substr(BASEPATH,0,$len-7);?></td>
									</tr>
									<tr>
										<td>上傳檔案大小限制</td>
										<td><?php echo get_cfg_var("upload_max_filesize");?></td>
									</tr>
									<tr>
										<td>系統記億體限制</td>
										<td><?php echo ini_get('memory_limit');?></td>
									</tr>
									<tr>
										<td>POST 資料大小限制</td>
										<td><?php echo ini_get('post_max_size');?></td>
									</tr>
									<tr>
										<td>主機租用廠商</td>
										<td>WIS 匯智</td>
									</tr>
									<tr>
										<td>網站空間</td>
										<td>5 GB</td>
									</tr>
									<tr>
										<td>每月流量</td>
										<td>100 GB</td>
									</tr>
								</tbody>
							</table>
							</div>	
						</div>						
					</div>					
			
			