<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link href="<?=base_url('public/img/123.png');?>" rel="SHORTCUT ICON">
    <title>NEEDER。你的 管理後台</title>
	<!--jQuery-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Bootstrap core CSS -->
	<link href="<?=base_url('public/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />    
    <script src="<?=base_url('public/js/bootstrap.min.js');?>"></script>  
    <!-- Custom styles for this template -->
	<link href="<?=base_url('public/css/signin.css');?>" rel="stylesheet" type="text/css" />		
</head>
<body>
    <div class="container" style="margin-top:40px">		
		 <div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<h2 class="text-center login-title" style="color:#fff">NEEDER。你的管理後台</h2>
				<div class="account-wall">
					<img class="profile-img" src="<?=base_url('public/img/ICON.png');?>" alt="" />
					<?php echo form_open('login/login_check',array('class'=>'form-signin'));?>
						<input type="text" class="form-control" name="username" style="margin-bottom:5px;" placeholder="帳號" required autofocus>
						<input type="password" class="form-control" name="password" placeholder="密碼" required>
						<div class="form-group" style="height: 45px;">
							<input id="captcha_div" class="form-control" name="captcha" placeholder="驗證碼" name="captcha" type="text" value="" maxlength="6" >
							<div id="captcha_img" style="float: left;"><img id="captcha" src="<?=$img;?>" /></div>
						</div>
						<button class="btn btn-lg btn-primary btn-block" type="submit">登入</button>					
						<a href="#" class="pull-right need-help">忘記密碼? </a><span class="clearfix"></span>
					</form>
				</div>				
			</div>
		</div>
	</div>
	<!--	<div class="row">			
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>NEEDER。你的</h3>-<strong>管理後台</strong>
					</div>
					<div class="panel-body">
						<?php echo form_open('login/login_check');?>						
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="baby_logo" src="<?=base_url('public/img/456.png');?>" alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<?php if(isset($msg)){?>
										<div class="alert alert-danger" role="alert"><?=$msg;?></div>
										<?php }?>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" placeholder="Username" name="username" type="text" autofocus>
											</div>
										</div>										
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock" aria-hidden="true"></i>
												</span>
												<input class="form-control" placeholder="Password" name="password" type="password" value="">
											</div>
										</div>
										<div class="form-group" style="height: 45px;">
											<input id="captcha_div" class="form-control" placeholder="驗證碼" name="captcha" type="text" value="" maxlength="6" >
											<div id="captcha_img" style="float: left;"><img id="captcha" src="<?=$img;?>" /></div>
											</div>
										<div class="form-group" style="margin-top: 30px;">
											<input type="submit" class="btn btn-lg btn-primary btn-block" value="登入">
										</div>
									</div>
								</div>
							</fieldset>
						<?php echo form_close();?>
					</div>
					<div class="panel-footer ">
						<a style="margin-left:0px;" href="<?=explode("/admin",base_url())[0];?>"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;返回前台</a><div class="pull-right">© 2014</div>
					</div>
                </div>
			</div>
		</div>
	</div>-->
</body>
<script>
//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('login/securimage_jpg')?>'); 				
});	
</script>
</html>