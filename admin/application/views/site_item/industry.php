				
<div class="page-header">
	<div class="container-fluid">
		<h1>產業列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('site_item');?>">網站項目管理</a></li>
			<li><a>產業列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<a href="#addModal" data-toggle="modal"><button id="new" type="button" class="btn btn-primary">新增產業</button></a>
			<input type="hidden" id="mytable" value="industry">
			<input type="hidden" id="pk" value="id">
			<input type="hidden" id="myid" >
			<table id="eventsTable" class="quickedit"
					data-toggle="table"
					data-url="<?=base_url('site_item/get_industry_info');?>"
					data-sort-name="sort">			
				<thead>			
					<tr>
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="20" data-field="industry_code" data-editable="true">產業代號</th>	
						<th data-width="20" data-field="industry_name" data-editable="true">產業名稱</th>
						<th data-width="20" data-field="num">廠商數量</th>	
						<th data-width="20" data-field="sort">順序</th>						
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>						
	
<div class="modal fade " id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">新增</h4>
			</div>		
			<div class="modal-body clearfix" >	
				<?=form_open('manage/save_member',array('id'=>'edit_frm'));?>
				<div class="form-group col-sm-12" >
					<label class="col-sm-3 control-label">產業代號</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="industry_code"/>						
					</div>																
				</div>					
				<div class="form-group col-sm-12" >
					<label class="col-sm-3 control-label">產業名稱</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="industry_name"/>
					</div>					
				</div>					
				<div class="form-group col-sm-12" >
					<div class="col-sm-offset-3 col-sm-3">
						 <button type="button" id="save" class="btn btn-primary" data-dismiss="modal">儲存</button>
					</div>
					<div class="col-sm-6">
						 <div id="register_error" class="msg"></div>
					</div>					
				</div>
				<?php echo form_close();?>
			</div>			
		</div>
	</div>
</div>	
				
<script>
window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'site_item/delete_industry',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();					
});	

$(document).on("click", "#save", function() {		
	$.ajax({
		url: base_url+'site_item/create_industry',
		type:"post",
		data: $(this).closest('form').serialize(),
		dataType: "json",
		success: function(result){
			$.growlUI(result);
		}			
	});
});
</script>				
