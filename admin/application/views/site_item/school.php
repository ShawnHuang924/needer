				
<div class="page-header">
	<div class="container-fluid">
		<h1>學校列表</h1>
		<ul class="breadcrumb">
			<li><a href="<?=base_url('site_item');?>">網站項目管理</a></li>
			<li><a>學校列表</a></li>
		</ul>
	</div>
</div>					
<div class="container-fluid">						
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<a href="<?=base_url("site_item/create");?>"><button id="new" type="button" class="btn btn-primary">新增學校</button></a>
			<input type="hidden" id="mytable" value="school">
			<input type="hidden" id="pk" value="id">
			<input type="hidden" id="myid" >
			<table id="eventsTable" class="quickedit"
					data-toggle="table"
					data-url="<?=base_url('site_item/get_school_info');?>"
					data-sort-name="id">			
				<thead>			
					<tr>
						<th data-width="5"  data-field="id">#</th>	
						<th data-width="8"  data-field="delete" data-sortable="false" data-formatter="deleteFormatter" data-events="deleteEvents">刪除</th>
						<th data-width="20" data-field="school_name" data-editable="true">學校名稱</th>	
						<th data-width="20" data-field="area_name">地區</th>
						<th data-width="20" data-field="num">寶貝人數</th>	
						<th data-width="20" data-field="sort">順序</th>						
					</tr>
				</thead>
			</table>			
		
		</div>	
	</div>	
</div>						
									
				
<script>
window.deleteEvents = {    
    'click .deletes': function (e, value, row, index) {
		var mytr = $(this).closest('tr');		
		delete_box(mytr,'site_item/delete_school',row.id);
    }
};
$('table').on('all.bs.table,page-change.bs.table', function (e, name, args) {
	create_state();					
});	
</script>				
