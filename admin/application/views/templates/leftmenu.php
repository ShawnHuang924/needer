	<nav id="column-left" class="my_scrollbar">
		<div id="profile">
			<div><a class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url('public/img/1428870212_Client 2.png');?>"  class="" style="height:42px" /></a></div>
			<div>
				<h4><?=$logged_in['name'];?></h4>
				<small>管理員</small>
			</div>
		</div>
		<ul id="menu">
			<li id="dashboard"><a href="<?=base_url('home');?>"><i class="glyphicon glyphicon-home"></i> <span>管理首頁</span></a></li>
			<li id="system"><a class="parent"><i class="glyphicon glyphicon-cog mtitle"></i><span>系統管理</span></a>
				<ul>
					<li><a href="<?=base_url('systems/information');?>">主機資訊</a></li>
					<!--<li><a href="<?=base_url('systems/mailconfig');?>">郵件伺服器設定</a></li>-->						  
				</ul>
			</li><!--
			<li id="mail"><a class="parent"><i class="glyphicon glyphicon-list-alt mtitle"></i><span>網站項目管理</span></a>
				<ul>
					<li><a href="<?=base_url('site_item/county');?>">區域縣市</a></li>						  
				</ul>
			</li>-->
			<li id="member"><a class="parent"><i class="fa fa-user"></i><span>會員管理</span></a>
				<ul>
					<li><a href="<?=base_url('normal/info');?>">ㄧ般會員列表</a></li>
					<li><a href="<?=base_url('provider/info');?>">提供舞台者列表</a></li>					  
				</ul>
			</li>
			<li id="stage"><a class="parent"><i class="fa fa-star-o"></i><span>舞台管理</span></a>
				<ul>
					<li><a href="<?=base_url('stage/info');?>">舞台列表</a></li>
					<!--<li><a href="<?=base_url('stage/verify');?>">舞台審核<span class="badge pull-right num_company_verify"></span></a></li>	-->				  
				</ul>
			</li>
			<li id="stage"><a class="parent"><i class="fa fa-caret-square-o-right"></i><span>展演管理</span></a>
				<ul>
					<li><a href="<?=base_url('show/info');?>">展演列表(已審核)</a></li>
					<li><a href="<?=base_url('show_unverify/info');?>">展演列表(未審核)</a></li>					
				</ul>
			</li>
			<li id="stage"><a href="<?=base_url('advertising');?>"><i class="fa fa-bookmark-o"></i><span>刊登廣告</span></a></li>
			<li id="stage"><a href="<?=base_url('contact_us');?>"><i class="fa fa-envelope"></i><span>連絡我們</span></a></li>
		</ul>
	</nav>