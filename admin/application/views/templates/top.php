	<header id="header" class="navbar navbar-static-top">
		<div class="navbar-header">		
			<a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>	
			<a href="<?=base_url();?>" class="navbar-brand"><img src="<?=base_url('public/img/NEEDER_LOGO2.png');?>" style="height:24px;" /></a>
		</div>		
		<ul class="nav pull-right">
			<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span id="total_num" class="label label-danger pull-left"></span> <i class="fa fa-bell fa-lg"></i></a>
				<ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
					<li class="dropdown-header">舞台</li>
					<li><a href="<?=base_url('stage/verify');?>"><span class="notify label label-danger pull-right num_company_verify"></span>註冊審核</a></li>
					<li><a href="<?=base_url('stage/info');?>"><span class="label label-success pull-right num_company"></span>舞台數量</a></li>
					<li class="divider"></li>
					<li><a href="<?=base_url('home');?>"><span class="label label-success pull-right num_online">1</span>在線人數</a></li>
				</ul>
			</li>		
			<li><a href="<?=str_replace("/admin/","",base_url());?>" target="_blank"><span class="hidden-xs hidden-sm hidden-md">返回前台</span> <i class="glyphicon glyphicon-globe"></i></a></li>
			<li><a href="<?=base_url('login/logout');?>"><span class="hidden-xs hidden-sm hidden-md">登出</span> <i class="fa fa-sign-out fa-lg"></i></a></li>
		</ul>
	</header>