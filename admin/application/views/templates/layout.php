<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link href="<?=base_url('public/img/needer_icon.ico');?>" rel="SHORTCUT ICON">
    <title>NEEDER。你的-後台管理系統</title>
	<!-- jQuery -->
	<script src="<?=base_url('public/js/jquery/jquery-2.1.3.min.js');?>"></script>
	<!-- jQuery UI -->
	<link  href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>			
    <!-- Bootstrap -->
	<link href="<?=base_url('public/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />    
    <script src="<?=base_url('public/js/bootstrap.min.js');?>"></script>  
	<!-- Bootstrap select -->
	<link  href="<?=base_url('public/css/bootstrap-select.min.css');?>" rel="stylesheet"/>
	<script src="<?=base_url('public/js/bootstrap-select.min.js');?>"></script>	
	<!-- Bootstrap Table-->
	<link href="<?= base_url('public/plugin/bootstrap-table/dist/bootstrap-table.min.css');?>" rel="stylesheet">
	<link href="<?= base_url('public/plugin/bootstrap-table/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css');?>" rel="stylesheet">
	<script src="<?= base_url('public/plugin/bootstrap-table/src/bootstrap-table.js');?>" type="text/javascript"></script>
	<script src="<?= base_url('public/plugin/bootstrap-table/dist/locale/bootstrap-table-zh-TW.min.js');?>"></script>
	<script src="<?= base_url('public/plugin/bootstrap-table/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js');?>"></script>
	<script src="<?= base_url('public/plugin/bootstrap-table/dist/extensions/editable/bootstrap-table-editable.min.js');?>"></script>
	
	<!-- Bootstrap Switch-->
	<link href="<?= base_url('public/plugin/bootstrap-switch/dist/css/bootstrap-switch.min.css');?>" rel="stylesheet">
	<script src="<?= base_url('public/plugin/bootstrap-switch/dist/js/bootstrap-switch.min.js');?>"></script>
	<!-- Bootbox -->
	<script src="<?=base_url('public/js/bootbox.min.js');?>"></script>
	<!-- Font awesome -->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<!-- Summernote -->
	<link href="<?=base_url('public/plugin/Summernote/dist/summernote.css');?>" rel="stylesheet">
	<script src="<?=base_url('public/plugin/Summernote/dist/summernote.min.js');?>"></script>
	<script src="<?=base_url('public/plugin/Summernote/lang/summernote-zh-TW.js');?>"></script>	
	<!-- Bootstrap-switch -->
	<link href="<?=base_url('public/css/bootstrap-switch.min.css');?>" rel="stylesheet">
	<script src="<?=base_url('public/js/bootstrap-switch.min.js');?>"></script>
	<!-- blockUI -->
	<script src="<?=base_url('public/js/jquery.blockUI.js');?>"></script>
	<!-- Jquery.fileupload -->
	<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload.css');?>" rel="stylesheet" >
	<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload-ui.css');?>" rel="stylesheet" >
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/vendor/jquery.ui.widget.js');?>"></script>
	<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
	<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.iframe-transport.js');?>"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload.js');?>"></script>	
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-process.js');?>"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-image.js');?>"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-validate.js');?>"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-ui.js');?>"></script>
	<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/cors/jquery.xdr-transport.js');?>"></script>
	
	<script src="<?=base_url('public/plugin/Jcrop/js/jquery.Jcrop.min.js');?>"></script>
	<link href="<?=base_url('public/plugin/Jcrop/css/jquery.Jcrop.min.css');?>" 	rel="stylesheet" type="text/css"/>
	
	<script src="<?=base_url('public/plugin/jquery-timepicker/daterangepicker.js');?>"></script>
	<script src="<?=base_url('public/plugin/jquery-timepicker/Datepair/dist/datepair.min.js');?>"></script>
	<script src="<?=base_url('public/plugin/jquery-timepicker/Datepair/dist/jquery.datepair.min.js');?>"></script>
	
	<script src="<?=base_url('public/plugin/bootstrap-daterangepicker/moment.min.js');?>"></script>
	<!--<script src="<?=base_url('public/plugin/bootstrap-daterangepicker/jquery.timepicker.min.js');?>"></script>
	<link href="<?=base_url('public/plugin/bootstrap-daterangepicker/daterangepicker.css');?>" 	rel="stylesheet" type="text/css"/>-->
	
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/plugin/datepicker/css/bootstrap-datepicker3.standalone.min.css');?>">
    <script type="text/javascript" src="<?= base_url('public/plugin/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
	<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/locales/bootstrap-datepicker.zh-TW.min.js');?>"></script>
	
	
	<script type="text/javascript" src="<?=base_url('public/plugin/bootstrap-datetimepicker/build/js/moment.min.js');?>"></script>
	<script type="text/javascript" src="<?=base_url('public/plugin/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');?>"></script>
	<link rel="stylesheet" href="<?=base_url('public/plugin/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');?>" />
	
	
	<script type="text/javascript" src="<?=base_url('public/plugin/clockpicker/dist/bootstrap-clockpicker.min.js');?>"></script>
	<link rel="stylesheet" href="<?=base_url('public/plugin/clockpicker/dist/bootstrap-clockpicker.min.css');?>" />
	
	
	
	<!-- blueimp Gallery -->
	<link href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css" rel="stylesheet" >
	<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<link href="<?=base_url('public/css/whhg.css');?>" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?=base_url('public/css/style.css');?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('public/css/admstyle.css');?>" rel="stylesheet" type="text/css" />
	<script src="<?=base_url('public/js/jquery.twzipcode.js');?>"></script>
	
	<link href=" http://cdn.syncfusion.com/js/web/flat-azure/ej.web.all-latest.min.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="<?=base_url('public/js/ejdatepicker/globalize.min.js');?>"></script>
	<script src="<?=base_url('public/js/ejdatepicker/globalize.cultures.js');?>"></script>
	<script src="<?=base_url('public/js/ejdatepicker/ej.core.min.js');?>"></script>			
	<script src="<?=base_url('public/js/ejdatepicker/ej.datepicker.min.js');?>"></script>
	
	<link href="<?=base_url('public/css/stylesheet.css');?>" rel="stylesheet" type="text/css" />
	<script src="<?=base_url('public/js/admjs/control.js');?>"></script>
	<script src="<?=base_url('public/js/admjs/common.js');?>"></script>
	<script> var base_url="<?=base_url();?>"; </script>
	
</head>
<body>
	<div id="container">
		<?=$top;?>
		<?=$leftmenu;?>
		<div id="content" class="clearfix my_scrollbar">
			<?=$body;?>			
		</div>
	</div>
</body>
</html>
	
	
	