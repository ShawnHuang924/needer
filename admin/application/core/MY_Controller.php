<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Mobile_Detect');
	}
	
	
	//顯示template
	public function template($page_name, $data=[])
	{		
		//登入的session
		if($this->session->userdata('logged_in'))
			$data['logged_in']=$this->session->userdata('logged_in');		
		//訊息session	
		if($this->session->userdata('msg'))
			$data['msg']=$this->session->userdata('msg');				
		$this->session->set_userdata('current_url',current_url());	
		
		if($this->session->userdata('logged_in'))
		{
			// 已登入
			$data['logged_in'] = $this->session->userdata('logged_in');
			$data['top'] = $this->load->view('templates/top', $data, TRUE);	
			$data['leftmenu'] = $this->load->view('templates/leftmenu', $data, TRUE);
			$data['body'] = $this->load->view($page_name, $data, TRUE);
			$this->load->view('templates/layout', $data);	
		}
		else
		{
			redirect(base_url('login'));
		}
			
		
	}
	
	
	//寄信
	public function send_mail($to=null, $subject=null, $content=null, $from=0, $from_name=0){        
        $data = $this->libraries_model->_select("site_setting");
        foreach($data as $v)
			$data[$v['key']]=$v['value'];			
        if($data['mail_protocol'] == 'smtp'){            
            $config['protocol']     =  $data['mail_protocol'];
            $config['mailtype']     =  $data['mail_type'];
            $config['smtp_host']    =  $data['smtp_host'];
            $config['smtp_port']    =  $data['smtp_port'];
            $config['smtp_user']    =  $data['smtp_user'];;
            $config['smtp_pass']    =  $data['smtp_pass'];
            $config['validation']   =  $data['smtp_validate'];
            $config['charset']      =  'utf-8';
            $config['newline']      =  "\r\n";            
        }elseif($data['mail_protocol'] == 'sendmail' || $data['mail_protocol'] == 'mail'){             
            $config['protocol']     =  $data['mail_protocol'];
            $config['mailtype']     =  $data['mail_type'];
            $config['mailpath']     =  $data['mail_path'];
            $config['charset']      =  'utf-8';
            $config['newline']      =  "\r\n";            
        }        
        $this->email->initialize($config);        
        if(!$from && !$from_name){
            $this->email->from($data['smtp_user'],$data['website_name']);
        }else{
            $this->email->from($from,$from_name);
        } 		
        $this->email->to($to); 
        $this->email->subject($subject);		
		$this->email->message($content);		
        if(!$this->email->send()){
            echo $this->email->print_debugger();
        }
    }
	
	//取得使用者IP
	public function get_ip(){
        static $realIP;        
        if($realIP) return $realIP;
        //代理時
        $ip = getenv('HTTP_CLIENT_IP')?  getenv('HTTP_CLIENT_IP'):getenv('HTTP_X_FORWARDED_FOR');
        preg_match("/[\d\.]{7,15}/", $ip, $match);
        if(isset($match[0])) return $realIP = $match[0];
        //非代理時
        $ip = !empty($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR']:'0.0.0.0';
        preg_match("/[\d\.]{7,15}/", $ip, $match);		
        return $realIP = isset($match[0])? $match[0]:'0.0.0.0';
    }
	
	//轉成youtube格式的URL
	public function youtube($youtube){
		$string = str_replace ("\ ", "", $youtube);
		$search = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
		$replace= 'http://www.youtube.com/embed/$2';
		$url    = preg_replace($search,$replace,$string);
		$matche = preg_match('/http?:\/\/(?:www\.)?youtu(?:\.be|be\.com)/',$url);
		return ($matche==0) ? '' : $url.'?&showinfo=0';
	}

	//將暫存的圖片移到正確的地方	
	/*public function move_pic($dir)
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$dir = str_replace('-','/',$dir);
		$newfolder = ROOT.'/public/photos/'.$dir;
		$this->rrmdir($newfolder);	
		if (!is_dir($newfolder)) {
			mkdir($newfolder, 0777, true);
		}
		$path = ROOT.'/public/photos/tmp/'.$user_id.'/'.explode("/",$dir)[0].'/'.explode("/",$dir)[2];
		$filename = isset(glob($path.'/*.*')[0])?glob($path.'/*.*')[0]:false;
		//如果檔案存在搬新的過去
		if($filename && file_exists($filename)){			
			rename($filename, $newfolder.'/pic.png');
		//如果不存在則把原本的刪除	
		}else{
			
			$this->delete_pic($dir);
		}
		
		if(is_dir(ROOT.'/public/photos/tmp/'.$user_id)) {
			$this->rrmdir(ROOT.'/public/photos/tmp/'.$user_id);	
		}
		
	}
	
	//編輯圖片時先複製過來	
	public function copy_pic($dir)
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$dir = str_replace('-','/',$dir);
		$editfolder = ROOT.'/public/photos/tmp/'.$user_id.'/'.explode("/",$dir)[0].'/'.explode("/",$dir)[2];
		$this->rrmdir($editfolder);
		if (!is_dir($editfolder)) {
			mkdir($editfolder, 0777, true);
		}
		$fullname='';
		if(count(glob(ROOT.'/public/photos/'.$dir.'/*.*'))>0)
			$fullname = glob(ROOT.'/public/photos/'.$dir.'/*.*')[0];
		$sub = end(explode(".",$fullname));		
		$filename = ROOT.'/public/photos/'.$dir.'/pic.'.$sub;
		if(file_exists($filename)) {	
			copy( $filename , $editfolder.'/pic.'.$sub);	
		}
	}	*/
	
	//遞迴刪除整個資料夾
	public function rrmdir($dir) {
		if (is_dir($dir)) {
		 $objects = scandir($dir);
		 foreach ($objects as $object) {
		   if ($object != "." && $object != "..") {
			 if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
		   }
		 }
		 reset($objects);
		 rmdir($dir);
		}
	}
	
	//刪除圖片	
/*	public function delete_pic($dir)
	{
		$filename = ROOT.'/public/photos/'.$dir.'/pic.png';
		if(file_exists($filename)) {	
			unlink($filename);	
		}	
	}
	
	//刪除使用者所有圖片	
	public function delete_user_pic($type,$id)
	{
		$dir = ROOT.'/public/photos/'.$type.'/'.$id;
		if(is_dir($dir))
			$this->rrmdir($dir);			
	}
	
	//清空資料夾
	public function clean_mytmp()
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$this->rrmdir(ROOT.'/public/photos/tmp/'.$user_id);
	}

	//儲存照片
	public function save_pic(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->move_pic($data_post['type'].'-'.$data_post['id'].'-'.$data_post['folder']);
		redirect(base_url($data_post['type'].'/edit/'.$data_post['id']));
	}	*/
	
	//刪除所有照片
	public function delete_all_photos($id){
		$dir = ROOT.'/public/photos/member/'.$id;
		if(is_dir($dir))
			$this->rrmdir($dir);
		$this->db->where('user_id', $id)
				 ->delete('member_photos');		
	}
	
	
}