<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show_unverify extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
		$this->load->model('show_unverify_model');
    }

    public function index(){
		$this->info();       
    }
/***************顯示***************/

	//展演列表
	public function info(){
		$this->template('show_unverify/info');
    }
	
	/*//新增展演
	public function create(){	
		$data['industry'] = $this->admin_model->get_all_industry();
		$this->template('stage/create',$data);
    }*/	

	//編輯展演
	public function edit($id=0){		
		$data['info'] = $this->show_unverify_model->get_show_info($id);
		$data['stage_type'] = $this->admin_model->get_from_mapping('stage_type');
		//echo '<pre>'.print_r($data, true).'</pre>';die();
		$this->template('show_unverify/edit',$data);
    }	
	

/***************功能***************/
	
	//取得展演列表資料
	public function get_info(){	
		$data = $this->db->select("show_info.*,show_info.start_date,show_info.end_date,CONCAT( show_info.start_time,'~', show_info.end_time ) as activity_date,stage_type.type,c.filename,show_info.performer_name as name,county.county_name,CONCAT(stage_type.key,'-c',area.code,a.id,'-',show_info.id) as show_code",FALSE)
				 ->from('show_info')				 
				 ->join('member as a','a.id = show_info.member_id','left')
				 ->join('member as b','b.id = show_info.performer_id','left')
				 ->join('county','a.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=a.id','left')
				 ->where('is_verify', 0)
				 ->get()->result_array();
						 
		foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';
			$data[$key]['preview'] ='<a target="_blank" href="'.str_replace("/admin/","",base_url()).'/show/'.$val['show_code'].'"><img style="width:25px;" src="'.base_url('public/img/search.png').'"></a>';
			if($val['start_date']==$val['end_date'])
				$data[$key]['dates']=$val['start_date'];
			else
				$data[$key]['dates']=$val['start_date'].'~'.$val['end_date'];
			
		}
		
		echo json_encode($data);
    }
	
	//儲存展演個人資料
	public function save_info(){	
		$data_post = $this->input->post(NULL,TRUE);			
						
		$data=array(
			'is_verify'	=>	'1' 
		);
		$this->db->update('show_info', $data, array('id' => $data_post['id']));			
		echo json_encode('儲存成功');					
		
    }
	
/*	//新增展演
	public function create_stage(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		$this->form_validation->set_rules('email','email','trim|xss_clean|required');
		$this->form_validation->set_rules('password','password','trim|required');
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			redirect(base_url('stage/create'));			
		//驗證成功	
		}else{			
			$data=array(
				'email'   		=> $data_post['email'],
				'password'		=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
				'stage_name'	=> $data_post['stage_name'],		
				'tel'  	  		=> $data_post['tel'],
				'zipcode' 		=> $data_post['zipcode'],
				'address'  		=> $data_post['address'],
				'contact_name'	=> $data_post['contact_name'],
				'contact_title' => $data_post['contact_title'],	
				'contact_phone'	=> $data_post['contact_phone'],
				'founded_date'  => $data_post['founded_date'],
				'industry' 		=> $data_post['industry'],
				'register_date' => $data_post['register_date'],
				'youtube' 		=> $data_post['youtube'],
				'fb'   			=> $data_post['fb'],
				'enable'   		=> 0,
				'create_time' 	=> date("Y-m-d H:i:s")
			);
			$this->db->insert('stage_info', $data);
			redirect(base_url('stage/edit/'.$this->db->insert_id()));
		}
    }
	*/
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('show_info');	 
    }
/*	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';
		$this->form_validation->set_rules('stage_name',  	'公司名稱', $rule.'|required');
		$this->form_validation->set_rules('tel',   			'公司電話', $rule.'|required'); 
		$this->form_validation->set_rules('zipcode', 	   	'公司地區',	$rule.'|required'); 
		$this->form_validation->set_rules('address',		'公司地址', $rule);
		$this->form_validation->set_rules('contact_name', 	'聯絡人', 	$rule); 
		$this->form_validation->set_rules('contact_title',	'職稱', 	$rule);
		$this->form_validation->set_rules('contact_phone', 	'電話',  	$rule); 
		$this->form_validation->set_rules('founded_date', 	'創立日期', $rule); 
		$this->form_validation->set_rules('industry', 		'產業類別', $rule); 
		$this->form_validation->set_rules('register_date', 	'註冊日期', $rule); 
		$this->form_validation->set_rules('youtube', 		'影片',  	$rule); 
		$this->form_validation->set_rules('fb', 			'臉書',  	$rule); 
	}
*/


}
?>
