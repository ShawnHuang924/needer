<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Controller{
    
    public function __construct(){
        parent::__construct();	
    }

	//變更狀態
	public function change_state(){ 
		$data_post = $this->input->post(NULL,TRUE);	
		$table  = $data_post['table'];
		$id  = $data_post['id'];	
		$state  = $data_post['state'];
		$this->db->where('id', $id);
		$this->db->update($table, array('enable' => ($state=="true")? 1: 0 ) ); 
    }
	
	//變換VIP
	public function change_vip(){		 
		$data_post = $this->input->post(NULL,TRUE);	
		$table  = $data_post['table'];
		$id  	= $data_post['id'];	
		$state  = $data_post['state'];
		$this->db->where('id', $id);
		$this->db->update($table, array('is_vip' => ($state=="true")? 1: 0 ) );
	}
	
	//
	public function quicksave(){ 
		$table  = $this->input->post('table', true);
		$pk  	= $this->input->post('pk', true);
		$field  = $this->input->post('field', true);		
		$value  = $this->input->post('value', true);
		$data=array($field=>$value[$field]);
		$this->db->where($pk,$value[$pk])
			     ->update($table, $data);	
    }
	
}
?>
