<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Systems extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
    }

    public function index(){
		$this->information();       
    }	
	
/******************************/		
	//主機資訊
	public function information(){			
		$this->template('information');
    }
	
	//郵件伺服器設定
	public function mailconfig(){        
        $data = $this->admin_model->get_mail_config();
		foreach($data as $v)
			$data[$v['key']]=$v['value'];		
        $this->template('mailconfig',$data);
    }	
/******************************/
	
	//儲存郵件設定
	public function save_mail_config(){ 
		$data = array(
			'mail_protocol' => $this->input->post('protocol', true),
			'mail_type'		=> $this->input->post('type', true),		
			'smtp_host'  	=> $this->input->post('smtp_host', true),
			'smtp_port'  	=> $this->input->post('smtp_port', true),
			'smtp_user'  	=> $this->input->post('smtp_user', true),		
			'smtp_pass'  	=> $this->input->post('smtp_pass', true),
			'smtp_validate'	=> $this->input->post('smtp_validate', true),
			'mail_path'		=> $this->input->post('path', true)			
		);			
		$this->admin_model->save_mail_config($data);
    }
	
	//儲存郵件內容
	public function save_content(){ 
		$subject  = $this->input->post('subject', True);
		$content  = $this->input->post('content', FALSE);	
		$this->admin_model->save_mail_content('mail_content',$content);
    }
	
	//寄出信件
	public function send(){ 
		$to=$this->input->post('to', True);
		$subject=$this->input->post('subject', True);
		$content=$this->input->post('content', FALSE);
		$name='金城武';
		$mr='先生';
		$content = preg_replace( '/\#name\#/', $name, $content);
		$content = preg_replace( '/\#mr\#/', $mr, $content);
		//$this->custom->send_mail('test_content',$to,$subject);
		$this->custom->send_mail(null,$to,$subject,$content);
    }
	
	
	
	
}
?>
