<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advertising extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
    }

    public function index(){
		$this->template('advertising/info');   
    }	
	
/******************************/
		
	//取得舞台列表資料
	public function get_info(){	
		$data = $this->db->select("")					 
						 ->from('advertising')						
						 ->get()->result_array();
		echo json_encode($data);
    }
	
	
	
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('advertising');	 
    }
}
?>
