<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
		$this->load->model('member_model');
    }

    public function index(){
		$this->info();       
    }
/***************顯示***************/

	//ㄧ般會員列表
	public function normal_info(){
		$this->template('member/normal_info');
    }
	
	//提供者列表
	public function provider_info(){
		$this->template('member/provider_info');
    }
	
	//新增ㄧ般會員
	public function create(){	
		$data['industry'] = $this->admin_model->get_all_industry();
		$this->template('member/create',$data);
    }	

	//編輯ㄧ般會員
	public function edit($id=0){		
		$data = $this->member_model->get_member_info($id);
		$data['industry'] = $this->admin_model->get_all_industry();
		$this->copy_pic('member-'.$id.'-cover');
		$this->template('member/edit',$data);
    }	
	
	//編輯相簿
	public function photos($id=0){		
		$data = $this->member_model->get_member_info($id);
		$this->template('member/photos',$data);
    }		
	
	//ㄧ般會員註冊審核
	public function verify(){		
		$data['verify'] = $this->admin_model->get_verify_member();
		$this->template('member/verify',$data);
    }
	
	//放棄註冊名單
	public function giveup(){		
		$data['giveup'] = $this->admin_model->get_giveup_member();
		$this->template('member/giveup',$data);
    }
	
	

	//ㄧ般會員邀約通知
	public function contact(){		
		$data['contact'] = $this->admin_model->get_contact_member();
		$this->template('member/contact',$data);
    }

/***************功能***************/
	
	//取得ㄧ般會員列表資料
	public function get_info(){	
		$data = $this->db->select('member_info.*,industry.industry_name,CONCAT(year( from_days( datediff( now( ), founded_date ))),"年") as founded,CONCAT(a.county_name," ", a.district_name) as area,CONCAT(a.county_name," ", a.district_name," ", member_info.address) as address,GROUP_CONCAT(b.county_name,b.district_name SEPARATOR ", ") as push,COUNT(push.zipcode) as push_num',FALSE)
						 ->from('member_info')						 
						 ->join('industry','industry.industry_code=member_info.industry','left')
						 ->join('push','push.member_id=member_info.id','left')
						 ->join('district as a','a.zipcode=member_info.zipcode','left')
						 ->join('district as b','b.zipcode=push.zipcode','left')
						 ->group_by("member_info.id")
						 ->get()->result_array();
						 
		foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';
			$vip=($val['is_vip']==1) ? 'checked' : '';
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';
			$data[$key]['vip']='<input type="checkbox" class="bscheck" name="vip-checkbox" '.$vip.'>';
			$data[$key]['pic']='<img style="width:60px;" src="'.BASE.'public/photos/member/'.$val['id'].'/cover/pic.png" >';
		}	
		echo json_encode($data);
    }
	
	//儲存ㄧ般會員個人資料
	public function save_info(){	
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{				
			$data=array(
				'member_name'	=> $data_post['member_name'],		
				'tel'  	  		=> $data_post['tel'],
				'zipcode' 		=> $data_post['zipcode'],
				'address'  		=> $data_post['address'],
				'contact_name'	=> $data_post['contact_name'],
				'contact_title' => $data_post['contact_title'],	
				'contact_phone'	=> $data_post['contact_phone'],
				'founded_date'  => $data_post['founded_date'],
				'industry' 		=> $data_post['industry'],
				'register_date' => $data_post['register_date'],
				'youtube' 		=> $data_post['youtube'],
				'fb'   			=> $data_post['fb']  
			);
			$this->db->update('member_info', $data, array('id' => $data_post['id']));			
			echo json_encode('儲存成功');					
		}
    }
	
	//新增ㄧ般會員
	public function create_member(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		$this->form_validation->set_rules('email','email','trim|xss_clean|required');
		$this->form_validation->set_rules('password','password','trim|required');
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			redirect(base_url('member/create'));			
		//驗證成功	
		}else{			
			$data=array(
				'email'   		=> $data_post['email'],
				'password'		=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
				'member_name'	=> $data_post['member_name'],		
				'tel'  	  		=> $data_post['tel'],
				'zipcode' 		=> $data_post['zipcode'],
				'address'  		=> $data_post['address'],
				'contact_name'	=> $data_post['contact_name'],
				'contact_title' => $data_post['contact_title'],	
				'contact_phone'	=> $data_post['contact_phone'],
				'founded_date'  => $data_post['founded_date'],
				'industry' 		=> $data_post['industry'],
				'register_date' => $data_post['register_date'],
				'youtube' 		=> $data_post['youtube'],
				'fb'   			=> $data_post['fb'],
				'enable'   		=> 0,
				'create_time' 	=> date("Y-m-d H:i:s")
			);
			$this->db->insert('member_info', $data);
			redirect(base_url('member/edit/'.$this->db->insert_id()));
		}
    }
	
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('member_info');	
		$this->delete_user_pic('member',$id);	 
    }
	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';
		$this->form_validation->set_rules('member_name',  	'公司名稱', $rule.'|required');
		$this->form_validation->set_rules('tel',   			'公司電話', $rule.'|required'); 
		$this->form_validation->set_rules('zipcode', 	   	'公司地區',	$rule.'|required'); 
		$this->form_validation->set_rules('address',		'公司地址', $rule);
		$this->form_validation->set_rules('contact_name', 	'聯絡人', 	$rule); 
		$this->form_validation->set_rules('contact_title',	'職稱', 	$rule);
		$this->form_validation->set_rules('contact_phone', 	'電話',  	$rule); 
		$this->form_validation->set_rules('founded_date', 	'創立日期', $rule); 
		$this->form_validation->set_rules('industry', 		'產業類別', $rule); 
		$this->form_validation->set_rules('register_date', 	'註冊日期', $rule); 
		$this->form_validation->set_rules('youtube', 		'影片',  	$rule); 
		$this->form_validation->set_rules('fb', 			'臉書',  	$rule); 
	}
	
	//取得VIPㄧ般會員審核資料
	public function get_verify(){ 
		$data = $this->db->select('*')
						 ->from('baby_contact')
						 ->get()->result_array();
		foreach($data as $key => $val){			
			$data[$key]['access']='<img class="verify_access" src="'.base_url('public/img/ok_icon.png').'">';
			$data[$key]['delete']='<img class="verify_delete" src="'.base_url('public/img/remove.png').'" >';			
		}	
		echo json_encode($data);		 
	}


	
/**************審核************************************/	
/*	
	//通過審核
	public function access_member_verify(){ 
		$id = $this->input->post('id', true);
		$this->admin_model->access_member_verify($id);
		$this->admin_model->delete_member_verify($id);
    }	
	
	//刪除審核
	public function delete_member_verify(){ 
		$id = $this->input->post('id', true);
		$this->admin_model->delete_member_verify($id);		
    }
	
	//取得審核數量
	public function get_num_verify(){ 
		$num = $this->admin_model->get_num('member_verify');	
		echo json_encode($num);
    }



	*/
}
?>
