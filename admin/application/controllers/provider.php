<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provider extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
		$this->load->model('member_model');
    }

    public function index(){
		$this->info();       
    }
/***************顯示***************/
	
	//提供者列表
	public function info(){
		$this->template('provider/info');
    }
	
	//新增提供者
	public function create(){	
		$data['county'] = $this->admin_model->get_all_county();
		$data['theme'] = $this->admin_model->get_from_mapping('theme');	
		$data['company_type'] = $this->admin_model->get_from_mapping('company_type');
		$this->template('provider/create',$data);
    }	

	//編輯提供者
	public function edit($id=0){		
		$data = $this->member_model->get_provider_info($id);
		$data['county'] = $this->admin_model->get_all_county();
		$data['theme'] = $this->admin_model->get_from_mapping('theme');	
		$data['company_type'] = $this->admin_model->get_from_mapping('company_type');
		$data['detail_type'] = $this->get_detail_type2(1);
		$data['stage_type'] = $this->admin_model->get_all_stage_type();
		$data['week'] = $this->admin_model->get_from_mapping('week');
		$data['length'] = $this->admin_model->get_from_mapping('length');	
		$data['contact'] = $this->admin_model->get_from_mapping('contact');		
		$data['supply_item'] = $this->admin_model->get_from_mapping('supply_item');	
		$data['supply_device'] = $this->admin_model->get_from_mapping('supply_device');	
		foreach($data['themes'] as $key => $val){
			$data['themes'][$key]=$val['theme_id'];				
		}	
		foreach(explode(',',$data['info']['supply_item']) as $key => $val){
			$data['supply_items'][$key]=$val;				
		}
		foreach(explode(',',$data['info']['supply_device']) as $key => $val){
			$data['supply_devices'][$key]=$val;				
		}
		$this->template('provider/edit',$data);
    }	
	
	//編輯相簿
	public function photos($id=0){		
		$data = $this->member_model->get_provider_info($id);
		$this->template('provider/photos',$data);
    }		
/*	
	//提供者註冊審核
	public function verify(){		
		$data['verify'] = $this->admin_model->get_verify_provider();
		$this->template('provider/verify',$data);
    }
	
	//放棄註冊名單
	public function giveup(){		
		$data['giveup'] = $this->admin_model->get_giveup_provider();
		$this->template('provider/giveup',$data);
    }
	
	

	//提供者邀約通知
	public function contact(){		
		$data['contact'] = $this->admin_model->get_contact_provider();
		$this->template('provider/contact',$data);
    }

/***************功能***************/
	
	//取得提供者列表資料
	public function get_info(){	
		$data = $this->db->select("member.*,CONCAT('c',area.code,member.id) as company_code,county.county_name as county,district.county_name as company_district,c.value as sex,a.value as company_type,b.value as subscription,CONCAT(member.last_name,member.name) as name,CONCAT(district.county_name,' ', district.district_name,' ',member.company_address) as company_address",FALSE)
						 ->from('member')	
						 ->join('district','district.zipcode=member.company_district','left')
						 ->join('county','county.id=member.county','left')
						 ->join('area','county.area = area.id','left')
						 ->join('(SELECT * FROM mapping WHERE type="company_type") as a','a.key=member.company_type','left')
						 ->join('(SELECT * FROM mapping WHERE type="yes") as b','b.key=member.subscription','left')
						 ->join('(SELECT * FROM mapping WHERE type="sex") as c','c.key=member.sex','left')
						 ->where('member.type',2)
						 ->get()->result_array();
						 
		foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';			
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';
			$data[$key]['preview'] ='<a target="_blank" href="'.str_replace("/admin/","",base_url()).'/stage/'.$val['company_code'].'"><img style="width:25px;" src="'.base_url('public/img/search.png').'"></a>';
			//$data[$key]['pic']='<img style="width:60px;" src="'.BASE.'public/photos/provider/'.$val['id'].'/cover/pic.png" >';
		}	
		echo json_encode($data);
    }
	
	//儲存提供者個人資料
	public function save_info(){	
		$data_post = $this->input->post(NULL,TRUE);	
		//extract($data_post);
		/*$item='';
		if(isset($data_post['supply_item'])){
			$item.=implode(",", $data_post['supply_item']);
			if($data_post['other_item']!='')
				$item.=','.$data_post['other_item'];
		}else
			$item = $data_post['other_item'];
			
		$device='';	
		if(isset($data_post['supply_device'])){
			$device.=implode(",", $data_post['supply_device']);
			if($data_post['other_device']!='')
				$device.=','.$data_post['other_device'];
		}else
			$device = $data_post['other_device'];
		*/
		$this->validation();
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{		
			$data=array(
				'account'			=> $data_post['account'],
				'last_name'			=> $data_post['last_name'],		
				'name'  	  		=> $data_post['name'],
				'sex' 				=> (int)$data_post['sex'],
				'phone'  			=> $data_post['phone'],
				'birthday'			=> $data_post['birthday'],
				'county'			=> $data_post['city'],				
				'company_name' 		=> $data_post['company_name'],	
				'company_type'		=> $data_post['company_type'],
				'company_district'  => $data_post['company_district'],
				'company_address' 	=> $data_post['company_address'],
				'company_web' 		=> $data_post['company_web'],
				'contact' 			=> $data_post['contact'],
				'contact_start_time'=> $data_post['contact_start_time'],
				'contact_end_time' 	=> $data_post['contact_end_time'],
				'supply_item' 		=> isset($data_post['supply_item']) ? implode(",", $data_post['supply_item']) : '',
				'other_supply_item'	=> $data_post['other_item'],
				'supply_device' 	=> isset($data_post['supply_device']) ? implode(",", $data_post['supply_device']) : '',
				'other_supply_device'=> $data_post['other_device'],
				'update_time' 		=> date("Y-m-d H:i:s")
			);
			$this->db->update('member', $data, array('id' => $data_post['id']));
			$this->member_model->save_theme($data_post['id'],$data_post['theme']);
			echo json_encode('儲存成功');					
		}
    }
	
	//新增提供者
	public function create_provider(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		$this->form_validation->set_rules('account',  		'email', $rule.'|required');
		$this->form_validation->set_rules('password',   	'密碼', $rule.'|required'); 
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			redirect(base_url('provider/create'));			
		//驗證成功	
		}else{			
			$data=array(
				'type'   			=> 2,
				'account'   		=> $data_post['account'],
				'password'			=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
				'last_name'			=> $data_post['last_name'],		
				'name'  	  		=> $data_post['name'],
				'sex' 				=> (int)$data_post['sex'],
				'phone'  			=> $data_post['phone'],
				'birthday'			=> $data_post['birthday'],
				'county'			=> $data_post['city'],				
				'company_name' 		=> $data_post['company_name'],	
				'company_type'		=> $data_post['company_type'],
				'company_district'  => $data_post['company_district'],
				'company_address' 	=> $data_post['company_address'],
				'company_web' 		=> $data_post['company_web'],
				'enable'   			=> 0,
				'create_time' 		=> date("Y-m-d H:i:s")
			);
			$this->db->insert('member', $data);
			$insert_id = $this->db->insert_id();
			$this->member_model->save_theme($insert_id,$data_post['theme']);			
			redirect(base_url('provider/edit/'.$insert_id));
		}
    }
	
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->delete_all_photos($id);
		$this->db->where('id', $id)
				 ->delete('member'); 
    }
	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';		
		$this->form_validation->set_rules('last_name', 	   	'姓',		$rule.'|required'); 
		$this->form_validation->set_rules('name',			'名', 		$rule.'|required');
		$this->form_validation->set_rules('sex', 			'性別', 	$rule.'|required'); 
		$this->form_validation->set_rules('phone',			'聯絡電話', $rule.'|required');
		$this->form_validation->set_rules('city', 			'居住城市', $rule.'|required'); 
		$this->form_validation->set_rules('birthday', 		'出生日期', $rule.'|required'); 
		$this->form_validation->set_rules('theme[]', 		'感興趣主題',$rule.'|required'); 
		$this->form_validation->set_rules('company_name', 	'公司名稱', $rule.'|required'); 
		$this->form_validation->set_rules('company_type', 	'公司類型', $rule.'|required'); 
		$this->form_validation->set_rules('company_district','公司縣市',$rule.'|required'); 
		$this->form_validation->set_rules('company_address','公司地址', $rule.'|required');
		$this->form_validation->set_rules('company_web', 	'公司網址', $rule);
	}
	
	//
	public function get_detail_type(){
		$id = $this->input->post('id', true);
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		echo json_encode($detail_type);	 
    }
	
	public function get_detail_type2($id){
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		return $detail_type;	 
    }
	
	//儲存舞台
	public function save_stage(){
		$data_post = $this->input->post(NULL,TRUE);	
		extract($data_post);
		$detail='';
		if(isset($detail_type)){
			$detail.=implode(",", $detail_type);
			if($other_detail_type!='')
				$detail.=','.$other_detail_type;
		}else
			$detail = $other_detail_type;
		$data=array(
			'member_id'		=> $id,
			'stage_type'   	=> $stage_type,
			'detail_type'   => $detail,			
			'remark'  		=> isset($remark) ? implode(",", $remark) : '',
			'other_remark' 	=> $other_remark
		);	
		if($stage_type!=2){
			$data['activity_date'] = isset($spec_date) ? $spec_date : (isset($activity_date)?implode(",", $activity_date):'');
			$data['start_time']	   = $start_time;
			$data['end_time']	   = $end_time;
			$data['length']   	   = $length;
		}else{
			$data['start_date']	   = $start_date.'-00';
			$data['end_date']	   = $end_date.'-00';
			$data['week_length']   = $week_length;		
		}
		$this->db->insert('stage_info', $data);
    }
	
	
}
?>
