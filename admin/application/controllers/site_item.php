<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_item extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');
    }

    public function index(){
		$this->school();       
    }
/***************顯示***************/	
	//學校列表
	public function school(){		
		$this->template('site_item/school');
    }
	
	//擅長項目列表
	public function specialty(){
		$this->template('site_item/specialty');
    }
	
	//產業列表
	public function industry(){	
		$this->template('site_item/industry');
    }

/***************功能***************/

	//取得學校列表資料
	public function get_school_info(){ 
		$data = $this->db->select('school.id, school.school_name, area.area_name, c.num, school.sort, school.enable ')
						 ->from('school')
						 ->join('area','school.area=area.id')
						 ->join('(SELECT school, COUNT(school) AS num FROM baby_info GROUP BY school) AS c', 'c.school= school.id', 'left')
						 ->get()->result_array();
		echo json_encode($data);				 
	}
	
	//取得擅長項目列表資料
	public function get_specialty_info(){ 
		$data = $this->db->select('specialty.*,c.spec_num,d.coop_num')				 
						 ->from('specialty')
						 ->join('(SELECT s_id, COUNT(s_id) AS spec_num FROM specialty_index GROUP BY s_id) AS c', 'c.s_id= specialty.id', 'left')
						 ->join('(SELECT coop_item, COUNT(coop_item) AS coop_num FROM company_contact GROUP BY coop_item) AS d', 'd.coop_item= specialty.id', 'left')
						 ->get()->result_array();
		echo json_encode($data);				 
	}

	//取得產業列表資料
	public function get_industry_info(){ 
		$data = $this->db->select('industry.*,c.num')				 
						 ->from('industry')
						 ->join('(SELECT industry, COUNT(industry) AS num FROM company_info GROUP BY industry) AS c', 'c.industry= industry.industry_code', 'left')
						 ->order_by('industry.sort')
						 ->get()->result_array();
		echo json_encode($data);				 
	}
	
	//新增產業
	public function create_industry(){ 
		$data_post = $this->input->post(NULL,TRUE);	
		$this->db->select_max('sort');
		$max = $this->db->get('industry')->row_array();
		$data=array(
			'industry_code'=>$data_post['industry_code'],
			'industry_name'=>$data_post['industry_name'],
			'sort'		   =>$max['sort']+1
		);
		$this->db->insert('industry', $data);
		echo json_encode('新增成功');
    }
	
	
	//刪除產業
	public function delete_industry(){ 
		$this->db->where('id', $this->input->post('id', true))
				 ->delete('industry');	
    }
	
	
	//變更狀態
	/*public function change_state(){ 
		$table  = $this->input->post('table', true);
		$id  = $this->input->post('id', true);		
		$state  = $this->input->post('state', true);		
		$this->admin_model->save_state($table,$id,$state);
    }*/
	
	/*//編輯
	public function edit(){ 
		$table  = $this->input->post('table', true);
		$id  = $this->input->post('id', true);		
		$state  = $this->input->post('state', true);		
		$this->admin_model->save_state($table,$id,$state);
    }
	
	//刪除
	public function delete(){ 
		$table  = $this->input->post('table', true);
		$id  = $this->input->post('id', true);		
		$state  = $this->input->post('state', true);		
		$this->admin_model->delete($table,$id,$state);
    }*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>
