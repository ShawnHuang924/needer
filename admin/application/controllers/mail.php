<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('libraries_model');
		$this->load->model('admin_model');
		$this->load->library('custom');		
    }

    public function index(){
		$this->forget_password();       
    }
/***************顯示***************/	
	//忘記密碼
	public function forget_password(){		
		$data = $this->libraries_model->_select("mail","project","forget",0,1,0,0,"row"); 					
		$this->template('forget_password',$data);
    }	
	

/***************功能***************/

	//儲存郵件內容
	public function save_content(){
		$project  = $this->input->post('project', True);
		$data = array(
			'subject'  => $this->input->post('subject', True),
			'content'  => $this->input->post('content', FALSE)
		);
		$this->libraries_model->_update("mail",$data,"project",$project);		
    }
	
	
	
	//儲存忘記密碼信件
	/*public function save_forget_content(){
		$content  = $this->input->post('content', FALSE);	
		$this->admin_model->save_mail_content('forget_content',$content);
    }
/*	
	//編輯
	public function edit(){ 
		$table  = $this->input->post('table', true);
		$id  = $this->input->post('id', true);		
		$state  = $this->input->post('state', true);		
		$this->admin_model->save_state($table,$id,$state);
    }
	
	//刪除
	public function delete(){ 
		$table  = $this->input->post('table', true);
		$id  = $this->input->post('id', true);		
		$state  = $this->input->post('state', true);		
		$this->admin_model->delete($table,$id,$state);
    }
	*/
}
?>
