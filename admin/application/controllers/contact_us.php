<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
    }

    public function index(){
		$this->template('contact_us/info');   
    }	
	
/******************************/
		
	//取得舞台列表資料
	public function get_info(){	
		$data = $this->db->select("")					 
						 ->from('contact_us')						
						 ->get()->result_array();
						 
		/*foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';			
		}*/
		echo json_encode($data);
    }
	
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('contact_us');	 
    }
}
?>
