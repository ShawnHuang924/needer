<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Normal extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
		$this->load->model('member_model');
    }

    public function index(){
		$this->info();       
    }
/***************顯示***************/
	
	//ㄧ般會員列表
	public function info(){
		$this->template('normal/info');
    }
	
	//新增ㄧ般會員
	public function create(){	
		$data['county'] = $this->admin_model->get_all_county();
		$data['theme'] = $this->admin_model->get_from_mapping('theme');			
		$this->template('normal/create',$data);
    }	

	//編輯ㄧ般會員
	public function edit($id=0){		
		$data = $this->member_model->get_provider_info($id);
		$data['county'] = $this->admin_model->get_all_county();
		$data['theme'] = $this->admin_model->get_from_mapping('theme');		
		foreach($data['themes'] as $key => $val){
			$data['themes'][$key]=$val['theme_id'];				
		}	
		//$this->copy_pic('provider-'.$id.'-cover');
		$this->template('normal/edit',$data);
    }	

/***************功能***************/
	
	//取得ㄧ般會員列表資料
	public function get_info(){	
		$data = $this->db->select('member.*,county.county_name as county,c.value as sex,b.value as subscription,CONCAT(member.last_name,member.name) as name',FALSE)
						 ->from('member')
						 ->join('county','county.id=member.county','left')
						 ->join('(SELECT * FROM mapping WHERE type="yes") as b','b.key=member.subscription','left')
						 ->join('(SELECT * FROM mapping WHERE type="sex") as c','c.key=member.sex','left')
						 ->where('member.type',1)
						 ->get()->result_array();
		/*$data = $this->db->select('member.*,county.county_name as county,district.county_name as company_district,c.value as sex,a.value as company_type,b.value as subscription,CONCAT(member.last_name,member.name) as name,CONCAT(district.county_name," ", district.district_name," ",member.company_address) as company_address',FALSE)
						 ->from('member')	
						 ->join('district','district.zipcode=member.company_district','left')
						 ->join('county','county.id=member.county','left')
						 ->join('(SELECT * FROM mapping WHERE type="company_type") as a','a.key=member.company_type','left')
						 ->join('(SELECT * FROM mapping WHERE type="yes") as b','b.key=member.subscription','left')
						 ->join('(SELECT * FROM mapping WHERE type="sex") as c','c.key=member.sex','left')
						 //->join('industry','industry.industry_code=provider_info.industry','left')
						 ->where('member.type',2)
						 ->get()->result_array();*/
						 
		foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';			
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';			
			//$data[$key]['pic']='<img style="width:60px;" src="'.BASE.'public/photos/provider/'.$val['id'].'/cover/pic.png" >';
		}	
		echo json_encode($data);
    }
	
	//儲存ㄧ般會員個人資料
	public function save_info(){	
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{				
			$data=array(				
				'last_name'			=> $data_post['last_name'],		
				'name'  	  		=> $data_post['name'],
				'sex' 				=> (int)$data_post['sex'],
				'phone'  			=> $data_post['phone'],
				'birthday'			=> $data_post['birthday'],
				'county'			=> $data_post['city'],
				'update_time' 		=> date("Y-m-d H:i:s")
			);
			$this->db->update('member', $data, array('id' => $data_post['id']));
			$this->member_model->save_theme($data_post['id'],$data_post['theme']);
			echo json_encode('儲存成功');					
		}
    }
	
	//新增ㄧ般會員
	public function create_normal(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		$this->form_validation->set_rules('account',  		'email', $rule.'|required');
		$this->form_validation->set_rules('password',   	'密碼', $rule.'|required'); 
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			redirect(base_url('normal/create'));			
		//驗證成功	
		}else{			
			$data=array(
				'type'   			=> 1,
				'account'   		=> $data_post['account'],
				'password'			=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
				'last_name'			=> $data_post['last_name'],		
				'name'  	  		=> $data_post['name'],
				'sex' 				=> (int)$data_post['sex'],
				'phone'  			=> $data_post['phone'],
				'birthday'			=> $data_post['birthday'],
				'county'			=> $data_post['city'],	
				'enable'   			=> 0,
				'create_time' 		=> date("Y-m-d H:i:s")
			);
			$this->db->insert('member', $data);
			$insert_id = $this->db->insert_id();
			$this->member_model->save_theme($insert_id,$data_post['theme']);			
			redirect(base_url('normal/edit/'.$insert_id));
		}
    }
	
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('member'); 
    }
	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';		
		$this->form_validation->set_rules('last_name', 	   	'姓',		$rule.'|required'); 
		$this->form_validation->set_rules('name',			'名', 		$rule.'|required');
		$this->form_validation->set_rules('sex', 			'性別', 	$rule.'|required'); 
		$this->form_validation->set_rules('phone',			'聯絡電話', $rule.'|required');
		$this->form_validation->set_rules('city', 			'居住城市', $rule.'|required'); 
		$this->form_validation->set_rules('birthday', 		'出生日期', $rule.'|required'); 
		$this->form_validation->set_rules('theme[]', 		'感興趣主題',$rule.'|required'); 
	}
	


}
?>
