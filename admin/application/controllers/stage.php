<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stage extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('admin_model');	
		$this->load->model('stage_model');
    }

    public function index(){
		$this->info();       
    }
/***************顯示***************/

	//舞台列表
	public function info(){
		$this->template('stage/info');
    }
	
	/*//新增舞台
	public function create(){	
		$data['industry'] = $this->admin_model->get_all_industry();
		$this->template('stage/create',$data);
    }	

	//編輯舞台
	public function edit($id=0){		
		$data = $this->stage_model->get_stage_info($id);
		$data['industry'] = $this->admin_model->get_all_industry();
		$this->copy_pic('stage-'.$id.'-cover');
		$this->template('stage/edit',$data);
    }*/	
	

/***************功能***************/
	
	//取得舞台列表資料
	public function get_info(){	
		$data = $this->db->select("stage_info.*,CONCAT(DATE_FORMAT(stage_info.start_date,'%Y-%m'),'~',DATE_FORMAT(stage_info.end_date,'%Y-%m')) as exhibition_date,CONCAT(DATE_FORMAT(stage_info.start_time,'%H:%i'),'~',DATE_FORMAT(stage_info.end_time,'%H:%i')) as activity_time,member.company_name,b.value as stage",FALSE)					 
						 ->from('stage_info')				 
						 ->join('member','member.id = stage_info.member_id','left')
						 ->join('county','member.county = county.id','left')
						 ->join('area','county.area = area.id','left')
						 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
						 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
						 ->get()->result_array();
						 
		foreach($data as $key => $val){
			$status=($val['enable']==1) ? 'checked' : '';
			$status_name=($val['enable']==1) ? '啟用' : '停用';
			$data[$key]['status']='<input type="checkbox" class="bscheck" name="enable-checkbox" '.$status.'><span style="display:none">'.$status_name.'</span>';
			
		}
		echo json_encode($data);
    }
/*	
	//儲存舞台個人資料
	public function save_info(){	
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{				
			$data=array(
				'stage_name'	=> $data_post['stage_name'],		
				'tel'  	  		=> $data_post['tel'],
				'zipcode' 		=> $data_post['zipcode'],
				'address'  		=> $data_post['address'],
				'contact_name'	=> $data_post['contact_name'],
				'contact_title' => $data_post['contact_title'],	
				'contact_phone'	=> $data_post['contact_phone'],
				'founded_date'  => $data_post['founded_date'],
				'industry' 		=> $data_post['industry'],
				'register_date' => $data_post['register_date'],
				'youtube' 		=> $data_post['youtube'],
				'fb'   			=> $data_post['fb']  
			);
			$this->db->update('stage_info', $data, array('id' => $data_post['id']));			
			echo json_encode('儲存成功');					
		}
    }
	
	//新增舞台
	public function create_stage(){ 
		$data_post = $this->input->post(NULL,TRUE);			
		$this->validation();
		$this->form_validation->set_rules('email','email','trim|xss_clean|required');
		$this->form_validation->set_rules('password','password','trim|required');
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			redirect(base_url('stage/create'));			
		//驗證成功	
		}else{			
			$data=array(
				'email'   		=> $data_post['email'],
				'password'		=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
				'stage_name'	=> $data_post['stage_name'],		
				'tel'  	  		=> $data_post['tel'],
				'zipcode' 		=> $data_post['zipcode'],
				'address'  		=> $data_post['address'],
				'contact_name'	=> $data_post['contact_name'],
				'contact_title' => $data_post['contact_title'],	
				'contact_phone'	=> $data_post['contact_phone'],
				'founded_date'  => $data_post['founded_date'],
				'industry' 		=> $data_post['industry'],
				'register_date' => $data_post['register_date'],
				'youtube' 		=> $data_post['youtube'],
				'fb'   			=> $data_post['fb'],
				'enable'   		=> 0,
				'create_time' 	=> date("Y-m-d H:i:s")
			);
			$this->db->insert('stage_info', $data);
			redirect(base_url('stage/edit/'.$this->db->insert_id()));
		}
    }
	*/
	//刪除
	public function delete(){
		$id = $this->input->post('id', true);
		$this->db->where('id', $id)
				 ->delete('stage_info');	 
    }
/*	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';
		$this->form_validation->set_rules('stage_name',  	'公司名稱', $rule.'|required');
		$this->form_validation->set_rules('tel',   			'公司電話', $rule.'|required'); 
		$this->form_validation->set_rules('zipcode', 	   	'公司地區',	$rule.'|required'); 
		$this->form_validation->set_rules('address',		'公司地址', $rule);
		$this->form_validation->set_rules('contact_name', 	'聯絡人', 	$rule); 
		$this->form_validation->set_rules('contact_title',	'職稱', 	$rule);
		$this->form_validation->set_rules('contact_phone', 	'電話',  	$rule); 
		$this->form_validation->set_rules('founded_date', 	'創立日期', $rule); 
		$this->form_validation->set_rules('industry', 		'產業類別', $rule); 
		$this->form_validation->set_rules('register_date', 	'註冊日期', $rule); 
		$this->form_validation->set_rules('youtube', 		'影片',  	$rule); 
		$this->form_validation->set_rules('fb', 			'臉書',  	$rule); 
	}
*/


}
?>
