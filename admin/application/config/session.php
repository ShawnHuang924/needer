<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Session namespace
$config['sess_namespace'] = 'needer_admin';

/* End of file session.php */
/* Location: ./application/config/session.php */
