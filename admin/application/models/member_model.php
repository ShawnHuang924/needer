<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model{ 

	//取得舞台提供者資料
	public function get_provider_info($id){
		$this->db->select('member.*')
				 ->from('member')
				 ->where('member.id',$id);
		$date['info']=$this->db->get()->row_array();	
		$this->db->select('theme_id')
				 ->from('member_theme')
				 ->where('member_id',$id);
		$date['themes']=$this->db->get()->result_array();		
		return $date; 
	}

	//儲存興趣
	public function save_theme($id,$theme){
		$this->db->where('member_id', $id);
		$this->db->delete('member_theme');
		if(!empty($theme)){
			$data = array();		
			foreach($theme as $val){
				$arr=array('member_id' => $id ,'theme_id' => $val);
				array_push($data, $arr);
			}		
			$this->db->insert_batch('member_theme', $data);
		}
	}


	
	
	
}
?>
