<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{ 


/**************共用**********************************/

	
	//取得所有
	public function get_all_county(){  
		$this->db->select('*')
				 ->from('county');
		return $this->db->get()->result_array();  
	}

	public function get_from_mapping($type){  
		$this->db->select('*')
				 ->from('mapping')
				 ->where('type',$type);
		return $this->db->get()->result_array();  
	}	


	//取得所有
	public function get_all_stage_type(){  
		$this->db->select('*')
				 ->from('stage_type');
		return $this->db->get()->result_array();  
	}
	

	
	
	
}
?>
