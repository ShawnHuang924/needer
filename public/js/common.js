function create_single_upload(path,form) {
	form = form || ".upload_form";	
	//使用strict mode
	'use strict';
	//初始化
	$(form).fileupload({
		url: base_url+'upload/init_single_upload/'+path,
		maxNumberOfFiles: 1,
		maxFileSize: 5000000,					//文件大小限制
		autoUpload: true,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i	
    });
	//跨網域	
    $(form).fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	//讀取
	$(form).addClass('fileupload-processing');
	$.ajax({
		url: $(form).fileupload('option', 'url'),
		dataType: 'json',
		context: $(form)[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');		
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});	
		if(result.files.length>0){
			$(this).find('.edit_pic').css('min-height','0');
			$(this).find('.new_pic').hide();
		}
	});
	//上傳驗證
	$(form).bind('fileuploadstart', function (e, data) {
		$(this).find('.new_pic').hide();
		$(this).find('.edit_pic').css('min-height','0');
	}).bind('fileuploadadd', function (e, data) {
		var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'jpg,png';
		if (allowdtypes.indexOf(fileType) < 0) {
			alert('檔案格式不正確');
			return false;
		}	
		if(data.originalFiles[0]['size'] > 5000000) {
            alert('檔案太大');
			return false;
        }
		if($(form).find(".template-download:not('.ui-state-error')").length >= 1) {			
            alert('錯誤!!!最多只能上傳1張');
			return false;
        }
	}).bind('fileuploaddone', function (e, data) {
		$('#upload_pic').show();		
		//$(this).parent().parent().find('.btnDoCrop').attr('disabled',false);
	}).bind('fileuploaddestroyed', function (e, data) {
		$('#upload_pic').hide();
		$(this).find('.new_pic').show();
		$(this).find('.edit_pic').css('min-height','300px');
		if(flag==1)
			$('#chagepicModal').modal('hide');
		flag=0;
		//$(this).parent().parent().find('.cropfinish,.btnDoCrop').attr('disabled',true);	
	});	
};


//多張圖片上傳
function create_multiple_upload(form) {	
	//使用strict mode(嚴格模式)
	'use strict';	
	//初始化
	$(form).fileupload({       
		url: base_url+'upload/init_multiple_upload',
		maxNumberOfFiles: 5,	
		autoUpload: true,
		maxFileSize: 5000000,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		uploadTemplateId:'template-upload2',
		downloadTemplateId:'template-download2'
    });
	//跨網域	
    $(form).fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );	
	//讀取
	$(form).addClass('fileupload-processing');
	$.ajax({
		url: $(form).fileupload('option', 'url'),
		dataType: 'json',
		context: $(form)[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');		
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});		
		$.ajax({
			url: base_url+'upload/get_cover',
			type:"post",
			dataType:"json",
			success:function(result){
				console.log(result);
				$(".photos_cover").each(function(){
					if(result.filename==$(this).data("name")){
						$(this).addClass('is_cover');
					}							
				});
			}
		});
	});	
	//相簿照片上傳驗證
	$(form).bind('fileuploadadd', function (e, data) {
		var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'jpeg,jpg,png,JPEG,JPG,PNG';
		if (allowdtypes.indexOf(fileType) < 0) {
			alert_box('錯誤','檔案格式不正確');
			return false;
		}	
		if(data.originalFiles[0]['size'] > 5000000) {
            alert_box('錯誤','檔案太大');
			return false;
        }
		if($(form).find(".template-download:not('.ui-state-error')").length >= 5) {
            alert_box('錯誤','最多5張');
			return false;
        }		
	}).bind('fileuploaddone', function (e, data) {
		$.ajax({
			url: base_url+'upload/upload_photos',
			type:"post",
			dataType:'json',
			data: {"filename" 	: data.result.files[0].name},
			success: function(result){
				if(result==false){
					//console.log($('.photos_cover'));
					$('.photos_cover').each(function(){
						if($(this).data('name')==data.result.files[0].name)
							$(this).addClass('is_cover');
					});
					//alert('設置封面!');
				}else{
					
				}
					
			}	
		});
	}).bind('fileuploaddestroy', function (e, data) {	
		$.ajax({
			url: base_url+'upload/delete_photos',
			type:"post",
			data: {"filename" 	: data.name},
			success: function(){
				alert('刪除成功!');
			}	
		});
	});
};

//警告視窗
function alert_box(tittle,msg) {	
	bootbox.dialog({
		message: msg,
		title: '<img src="'+base_url+'public/img/warning_icon.png" style="height:30px;" /><span style="line-height:30px;">'+tittle+'</span>',
		className: "warning",
		buttons: {			
			cancel: {
				label: "確認",
				className: "btn-default"			
			}
		}
	});			
}
/*
//上傳圖片
function create_upload(channel,form) {
	form = form || ".upload_form";	
	//使用strict mode
	'use strict';
	//初始化
	$(form).fileupload({
		url: base_url+'upload/init_upload/'+channel,
		maxNumberOfFiles: 1,
		dropZone: $(form),
		maxFileSize: 5000000,					//文件大小限制
		autoUpload: true,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i	
    });
	//跨網域	
    $(form).fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	//讀取
	$(form).addClass('fileupload-processing');
	$.ajax({
		url: $(form).fileupload('option', 'url'),
		dataType: 'json',
		context: $(form)[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');		
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});	
		if(result.files.length>0){
			$(this).find('.edit_pic').css('min-height','0');
			$(this).find('.new_pic').hide();
		}
	});
	//上傳驗證
	$(form).bind('fileuploadstart', function (e, data) {
		$(this).find('.new_pic').hide();
		$(this).find('.edit_pic').css('min-height','0');
	}).bind('fileuploadadd', function (e, data) {
		var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'jpeg,jpg,png';
		if (allowdtypes.toUpperCase().indexOf(fileType.toUpperCase()) < 0) {
			alert('檔案格式不正確');
			return false;
		}	
		if(data.originalFiles[0]['size'] > 5000000) {
            alert('檔案太大');
			return false;
        }
		if($(form).find(".template-download:not('.ui-state-error')").length >= 1) {			
            alert('錯誤!!!最多只能上傳1張');
			return false;
        }	
	}).bind('fileuploaddone', function (e, data) {			
		//$(this).parent().parent().find('.btnDoCrop').attr('disabled',false);
	}).bind('fileuploaddestroyed', function (e, data) {		
		$(this).find('.new_pic').show();
		$(this).find('.edit_pic').css('min-height','300px');
		//me.parent().parent().find('.cropfinish,.btnDoCrop').attr('disabled',true);		
	});	
};*/

