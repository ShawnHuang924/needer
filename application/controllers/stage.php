<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stage extends MY_Controller {

	function __construct()
	{
		parent:: __construct();	
		$this->load->model('stage_model');	
	}
	
	public function index()
	{
		$data['county'] = $this->get_all_county();
		$this->template('stage/type',$data);
	}		
	
	//用代號取得詳細頁
	public function detail_id($code)
	{
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('stage'));
		$area = substr($code, 1, 1);		 
		$id = substr($code, 2);
		$this->select_join();
		$this->db->where('area.code',$area)
				 ->where('member.id',$id);				 
		$data['info'] = $this->db->get()->row_array();
		$data = $this->get_other_info($id,$data);
		//echo "<pre>".print_r($data, true)."</pre>";
		$this->template('stage/detail',$data);
	}
	
	//用公司名稱取得詳細頁
	public function detail_name($company_name)
	{
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('stage'));
		$this->select_join();
		$this->db->where('member.company_name',str_replace('_',' ',urldecode($company_name)));
		$data['info'] = $this->db->get()->row_array();		
		$data = $this->get_other_info($data['info']['id'],$data);
		//echo "<pre>".print_r($data, true)."</pre>";
		$this->template('stage/detail',$data);
	}
	
	public function select_join()
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$this->db->select("member.*,c.filename,area.id as area,a.love_member_id as favorite,member.company_name,county.county_name,GROUP_CONCAT(DISTINCT(b.key)) as stage_str,GROUP_CONCAT(DISTINCT(b.value) SEPARATOR ', ') as stage_type,CONCAT(member.last_name,member.name) as name,CONCAT(district.county_name,' ', district.district_name,' ',member.company_address) as company_address",FALSE)
				 ->from('stage_info')				 
				 ->join('member','member.id = stage_info.member_id','left')
				 ->join('district','district.zipcode=member.company_district','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('(SELECT * FROM stage_favorite WHERE member_id='.$user_id.' ) as a','a.love_member_id = member.id','left')
				 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->group_by("member.id");
	}	
	
	public function get_other_info($id,$data)
	{
		//我的所有照片
		$this->db->select('filename')
				 ->from('member_photos')
				 ->where('user_id',$id)
				 ->order_by("cover",'desc');
		$data['photos'] = $this->db->get()->result_array();
		
		//我的所有舞台
		$this->db->select("stage_info.*,CONCAT(YEAR(start_date),'/',MONTH(start_date),' - ',YEAR(end_date),'/',MONTH(end_date)) as ex_date,DATE_FORMAT(stage_info.start_time,'%H:%i') as start_time,DATE_FORMAT(stage_info.end_time,'%H:%i') as end_time,b.value as stage",FALSE)
				 ->from('stage_info')
				 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
				 ->where('member_id',$id)
				 ->order_by("stage_info.stage_type",'asc');
		$data['stages'] = $this->db->get()->result_array();
		
		//同類型同地區舞台
		$this->db->select("member.id,CONCAT('c',area.code,member.id) as company_code,c.filename,member.company_name,county.county_name,GROUP_CONCAT(DISTINCT(b.value) SEPARATOR ', ') as stage_type",FALSE)
				 ->from('stage_info')				 
				 ->join('member','member.id = stage_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->where('member.id <>',$id)
				 ->where_in('stage_info.stage_type', explode(',',$data['info']['stage_str']))
				 ->where_in('county.area',explode(',',$data['info']['area']))
				 ->group_by("member.id")
				 ->order_by("id",'random')
				 ->limit(4);
		$data['other'] = $this->db->get()->result_array();
		return $data;	
	}
	
	
}
  