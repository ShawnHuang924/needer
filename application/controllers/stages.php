<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stages extends MY_Controller {

	function __construct()
	{
		parent:: __construct();	
		$this->load->model('stage_model');	
	}
	

	
	//篩選
	public function filter(){ 
		$data_post = $this->input->post(NULL,TRUE);
		if($this->session->userdata('logged_in'))
			$user_id = $this->session->userdata('logged_in')['id'];	
		else
			$user_id = 0;
		//$data = $this->stage_model->get_stage($data_post);
		extract($data_post);
		$limit = 15;		
        $this->db->select("member.id,CONCAT('c',area.code,member.id) as company_code,c.filename,member.company_name,county.county_name,GROUP_CONCAT(DISTINCT(stage_type.type) ORDER BY stage_type.id SEPARATOR ', ') as stage_type,a.love_member_id as favorite",FALSE)
				 ->from('stage_info')				 
				 ->join('member','member.id = stage_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = stage_info.stage_type','left')
				 ->join('(SELECT * FROM stage_favorite WHERE member_id='.$user_id.' ) as a','a.love_member_id = member.id','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->group_by("member.id")
				 ->where('stage_info.enable',1)
				 ->where('member.enable',1);
        switch($action){
			case "filter":
				//篩選
				if(!empty($stage_type) && !in_array(0,$stage_type)){
					$this->db->where_in('stage_info.stage_type', $stage_type);					
				}					
				if(!empty($area) && !in_array(0,$area)){
					$this->db->where_in('county.area', $area);	
				}
				if(!empty($county) && !in_array(0,$county)){
					$this->db->where_in('county.id', $county);	
				}
				
				$this->db->limit($limit,$page*$limit);
				break;
				//排序
				switch($sort){								
					case 1:
						$this->db->order_by("stage_info.stage_type", "DESC");							
						break;
					case 2:
						$this->db->order_by("county.area", "DESC");
						break;
				}
				$this->db->limit($limit,$page*$limit);
				break;
			//搜尋	
			case "search":
				if($search=='')
					exit;
				$this->db->like('member.company_name', $search);					
				break;
        }   		
		$this->db->order_by("county.id",'asc');		
		//$this->db->order_by("id",'random');	
        $data = $this->db->get()->result_array();  
		//echo "<pre>".print_r($data, true)."</pre>";
		//echo $this->db->last_query();		
			
		echo json_encode($data);
	}
	//加入最愛
	public function love(){ 
		if(!$this->session->userdata('logged_in'))
			exit;
		$data = array(
		   'member_id' 		=> $this->session->userdata('logged_in')['id'] ,
		   'love_member_id' => $this->input->post('id',TRUE)
		);
		$this->db->insert('stage_favorite', $data); 
	}
	
	//移除最愛
	public function notlove(){
		if(!$this->session->userdata('logged_in'))
			exit;	
		$this->db->where('member_id', $this->session->userdata('logged_in')['id']);
		$this->db->where('love_member_id', $this->input->post('id',TRUE));
		$this->db->delete('stage_favorite');
	}
	//
	public function test(){ 
		//$query = $this->db->query('INSERT INTO stage_info (member_id, stage_type, detail_type) SELECT member_id, stage_type, detail_type FROM stage_info;');	
	}
	
}
  