<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {

  /**
   * 建構式，預設載入模組 {}
   */
	function __construct()
	{
		parent:: __construct();		
	}
	
	public function get_server_var($id) {
		return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
	}
	
	public function get_full_url() {
		$https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
		return
		($https ? 'https://' : 'http://').
		(!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
		(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
		($https && $_SERVER['SERVER_PORT'] === 443 ||
		$_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
		substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
	}
	
	//新增初始化上傳
	public function init_upload($channel,$field=''){
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		require_once './application/libraries/UploadHandler.php';			
		$user_id = $this->session->userdata('logged_in')['id'];
		if($field!='')
			$d='/'.$field;
			
		$options = array (
			'script_url' => $this->get_full_url().'/upload/init_upload/'.$channel.$d,
			'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')) .'/public/upload/'.$channel.'/tmp/'.$user_id.'/'.$field.'/' ,			
			'upload_url' => $this->get_full_url().'/public/upload/'.$channel.'/tmp/'.$user_id.'/'.$field.'/' /* ,	
			'file_name'  => $user_id.'.png'*/
		);
		$upload_handler = new UploadHandler($options);
	}		
	
	//切圖
	/*public function crop_cover($cover){
		if( !$this->input->post() ) redirect(base_url());
		$targ_w = 300*$this->input->post("cover_ratio",true);
		$targ_h = 300;
		$ratio= $this->input->post("origin_w",true)/$this->input->post("now_w",true);
		$jpeg_quality = 100;  
		  
		$path= 'public/photos/'.$this->session->userdata('logged_in')["type"].'/'.$this->session->userdata('logged_in')["id"].'/'.$cover.'/'.$cover.'.jpg';
		$src = base_url($path);  
		$img_r = imagecreatefromjpeg($src);  
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );  
		  
		imagecopyresampled($dst_r,$img_r,0,0,$this->input->post("x",true)*$ratio,$this->input->post("y",true)*$ratio,  
			$targ_w,$targ_h,$this->input->post("w",true)*$ratio,$this->input->post("h",true)*$ratio);  
		  
		header('Content-type: image/jpeg');  
		imagejpeg ($dst_r, $path, $jpeg_quality);
		$this->db->where('id',$this->session->userdata('logged_in')["id"])
				 ->update($this->session->userdata('logged_in')["type"].'_info', array('have_'.$cover => 1));		
	}*/
	
	
	
}
  