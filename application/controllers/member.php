<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MY_Controller {

	function __construct()
	{
		parent:: __construct();
		$this->load->model('common_model');
		$this->load->model('myaccount_model');		
	}	
		
	public function login()
	{
		$data['img']=$this->captcha_img();
		$this->template('member/login',$data);		
	}
	
	//View修改密碼
	public function reset_pwd($session_key = 0)
	{
		$session_status = $this->db->select('*')
								   ->from('member')
								   ->where('session_key',$session_key)
								   ->get()->row_array();		
        if(!$session_key || !$session_status) redirect (base_url('member/login'));
		//驗證是否過期
		$valid_status = $this->db->select('*')
								 ->from('member')
								 ->where('session_key',$session_key)
								 ->where('session_expire >= ',date("Y-m-d H:i:s"))
								 ->get()->row_array();
        $status = ($valid_status) ? 1:0;
		$data['hash']=$session_key;
		$data['status']=$status;				
		$this->template('member/reset_pwd',$data);
	}
	
	//會員驗證
	public function verify_account($session_key = 0)
	{
		$session_status = $this->db->select('*')
								   ->from('member')
								   ->where('session_key',$session_key)
								   ->get()->row_array();		
		if(!$session_key || !$session_status) redirect (base_url('member/register'));
		//驗證是否過期
		$valid_status = $this->db->select('*')
								 ->from('member')
								 ->where('session_key',$session_key)
								 ->where('session_expire >= ',date("Y-m-d H:i:s"))
								 ->get()->row_array();		
		//驗證成功							
		if($valid_status){
			$this->db->where('session_key',$session_key)
					 ->update('member',array('enable'=>1,'session_key'=>0));
			$this->session->set_userdata('msg', array('text'=> 'email驗證成功，請重新登入會員！' , 'color' => 'success'));		 
			redirect(base_url('member/login'));			
		}else{			
			$this->session->set_userdata('msg', array('text'=> 'email驗證失敗，請重新註冊會員！' , 'color' => 'danger'));	
			redirect(base_url('member/register'));
		}	
	}
	

	public function register()
	{		
		$data['img']=$this->captcha_img();
		$data['company_type'] = $this->get_dropdown('company_type');
		$data['county'] = $this->get_all_county();
		$this->template('member/register',$data);
	}	
	
	public function provide_stage()
	{		
		if($this->session->userdata('logged_in')['type']!=2)
			redirect(base_url('home'));
		$data['img']=$this->captcha_img();	
		$data['stage_type'] = $this->common_model->get_all_stage_type();	
		$data['detail_type'] = $this->get_detail_type2(1);
		$data['week'] = $this->common_model->get_from_mapping('week');
		$data['length'] = $this->common_model->get_from_mapping('length');	
		$data['weekend'] = $this->common_model->get_from_mapping('weekend');			
		$data['company_type'] = $this->common_model->get_from_mapping('company_type');		
		$this->template('member/provide_stage',$data);
	}
	
	public function provide_stage_info()
	{	
		if($this->session->userdata('logged_in')['type']!=2)
			redirect(base_url('home'));
		$id = $this->session->userdata('logged_in')['id'];
		$data 					= $this->myaccount_model->get_member_info($id);
		$data['contact'] 		= $this->common_model->get_from_mapping('contact');	
		$data['company_type'] 	= $this->common_model->get_from_mapping('company_type');
		$data['supply_item'] 	= $this->common_model->get_from_mapping('supply_item');	
		$data['supply_device'] 	= $this->common_model->get_from_mapping('supply_device');
		foreach(explode(',',$data['info']['supply_item']) as $key => $val){
			$data['supply_items'][$key]=$val;				
		}
		foreach(explode(',',$data['info']['supply_device']) as $key => $val){ 
			$data['supply_devices'][$key]=$val;				
		}
		$this->template('member/provide_stage_info',$data);
	}
	
	
	public function provide_stage_info_check()
	{	$data_post = $this->input->post(NULL,TRUE);	
		$id = $this->session->userdata('logged_in')['id'];
		$data=array(
			'company_name' 		=> $data_post['company_name'],	
			'company_type'		=> $data_post['company_type'],
			'company_district'  => $data_post['company_district'],
			'company_address' 	=> $data_post['company_address'],
			'last_name'			=> $data_post['last_name'],		
			'name'  	  		=> $data_post['name'],
			'phone'  			=> $data_post['phone'],	
			'contact' 			=> $data_post['contact'],
			'contact_start_time'=> $data_post['contact_start_time'],
			'contact_end_time' 	=> $data_post['contact_end_time'],
			'supply_item' 		=> isset($data_post['supply_item']) ? implode(",", $data_post['supply_item']) : '',
			'other_supply_item'	=> $data_post['other_item'],
			'supply_device' 	=> isset($data_post['supply_device']) ? implode(",", $data_post['supply_device']) : '',
			'other_supply_device'=> $data_post['other_device'],
			'update_time' 		=> date("Y-m-d H:i:s")
			);
		$this->db->update('member', $data, array('id' => $id));

		redirect(base_url('member/provide_stage'));
		
	
	}
	
	public function provide_check()
	{	
		if(!$this->session->userdata('logged_in')){
			$this->session->set_userdata('msg', array('text'=> '請先登入!!' , 'color' => 'danger'));
			redirect(base_url('member/provide_stage'));	
		}
		
		/*$rule='trim|xss_clean|required';
		$this->form_validation->set_rules('type'			, 'type'			,$rule);
		$this->form_validation->set_rules('account'			, 'account'			,$rule);
		if($this->form_validation->run() == FALSE){ 
			$this->session->set_userdata('msg', array('text'=> '註冊失敗' , 'color' => 'danger'));
			redirect(base_url('member/register'));
        }else{
			
		}*/
		
		$data_post = $this->input->post(NULL,TRUE);	
		foreach( $data_post as $key ){
			$data=array();
			$activity_date='';
			$remark='';
			if( isset($key['week']) ){
				$activity_date=implode(",", $key['week']);
			}
			if( isset($key['remark']) ){
				$remark=implode(",", $key['remark']);		
			}			
			
			if( isset($key['detail_type']) ){
				$detail_type=implode(",", $key['detail_type']);
			}
			if( $key['other_detail_type']!='' ){
				$detail_type=$detail_type.",".$key['other_detail_type'];
			}
			//展覽
			if( $key['stage_type']=='2' ){
				$data=array(
					'member_id' => $this->session->userdata('logged_in')['id'],
					'stage_type' => $key['stage_type'],
					'detail_type' => $detail_type,			
					'start_date' => $key['date_start'].'-00',
					'end_date' => $key['date_end'].'-00',	
					'week_length' => $key['my_show_time'],
					'remark' => $remark,
					'other_remark' => isset($key['other_remark'])?$key['other_remark']:''			
				);
			}
			//音樂、講座、教學、魔術、其他
			else{
				$data=array(
					'member_id' => $this->session->userdata('logged_in')['id'],
					'stage_type' => $key['stage_type'],
					'detail_type' => $detail_type,
					'activity_date' => isset($key['spec_date']) ? $key['spec_date'] : (isset($key['week'])?implode(",", $key['week']):''),
					'start_time' => $key['start_time'],
					'end_time' => $key['end_time'],
					'length' => $key['show_length'],										
					'remark' => $remark,
					'other_remark' => isset($key['other_remark'])?$key['other_remark']:''
				);					
			}		
			$this->db->insert('stage_info', $data);

		}
		$this->session->set_userdata('msg', array('text'=> '申請成功!!' , 'color' => 'success'));
		redirect(base_url('member/provide_stage'));		
	}
	
	public function get_detail_type(){
		$id = $this->input->post('id', true);
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		echo json_encode($detail_type);	 
    }
	
	public function get_detail_type2($id){
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		return $detail_type;	 
    }
	
	//登入檢查
	public function login_check()
	{
		if( $this->session->userdata('logged_in') ){		//已登入
            redirect(base_url('home'));
        }else{		
            $this->form_validation->set_rules('username', '帳號', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', '密碼', 'trim|required|callback_user_valid');   

            if($this->form_validation->run() == FALSE){      //驗證失敗
				$this->session->set_userdata('msg', array('text'=> '登入失敗!!' , 'color' => 'danger'));
				redirect(base_url('member/login'));                                
            }else{                							//驗證成功	
				$last = $this->session->userdata('last_url');
				if($last && !strpos($last,"register"))
					redirect($last);
				else
					redirect(base_url('home'));               
            }        
        }		
	}
	//登入驗證
	public function user_valid($password)
	{
        $username  = $this->input->post('username');
        $this->db->select('*')
				 ->from('member')
				 ->where('account',$username);
		$result=$this->db->get()->row_array();
		if($result && password_verify($password, $result['password']) ){
            $sess_array = array(
                'id'       => $result['id'],
                'account' => $result['account'],
				'name' => $result['last_name'].$result['name'],
                'type' => $result['type']
            );
            $this->session->set_userdata('logged_in',$sess_array);            
            return TRUE;
        }else{            						//認證失敗
            return FALSE;            
        }          
    }
	
	//登出
	public function logout()
	{		
        if ( $this->session->userdata('logged_in') ){
            $this->session->unset_userdata('logged_in');
            redirect(base_url('member/login'));
        }else{
            redirect(base_url('member/login'));
        }

    }
	
	//註冊驗證
	public function register_check()
	{
		if(!$this->input->post()) redirect(base_url('member/register0'));
 		$data_post = $this->input->post(NULL,true);
		if($this->check_account_exist($data_post['account'])){
			$this->session->set_userdata('msg', array('text'=> '此E-mail已被註冊!!' , 'color' => 'danger'));
			redirect(base_url('member/register'));		
		}
		//驗證碼驗證
		$this->load->library('securimage/securimage');
        $img = new Securimage();
        if( !$img->check( $data_post['captcha'] ) ) {
            $this->session->set_userdata('msg', array('text'=> '驗證碼錯誤!!' , 'color' => 'danger'));
			redirect(base_url('member/register'));  
        }
		
		$rule='trim|xss_clean|required';
		$this->form_validation->set_rules('type'			, 'type'			,$rule);
		$this->form_validation->set_rules('account'			, 'account'			,$rule);
		$this->form_validation->set_rules('password'		, 'password'		,'trim|required|min_length[6]');   
		$this->form_validation->set_rules('password_confirm', 'password_confirm','trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('last_name'		, 'last_name'		,$rule);
		$this->form_validation->set_rules('name'			, 'name'			,$rule);
		$this->form_validation->set_rules('sex'				, 'sex'				,$rule);		
		$this->form_validation->set_rules('my_county'		, 'my_county'		,$rule);
		$this->form_validation->set_rules('birthday'		, 'birthday'		,$rule);
		$this->form_validation->set_rules('phone'			, 'phone'			,$rule);
		$this->form_validation->set_rules('theme[]'			, 'theme'			,$rule);
		$this->form_validation->set_rules('provision'		, 'provision'		,$rule);
		if($data_post['type']==2){
			$this->form_validation->set_rules('company_name'	, 'company_name'	,$rule);
			$this->form_validation->set_rules('company_type'	, 'company_type'	,$rule);
			$this->form_validation->set_rules('zipcode'			, 'zipcode'			,$rule);
			$this->form_validation->set_rules('company_address'	, 'company_address'	,$rule);
			$this->form_validation->set_rules('company_web'		, 'company_web'		,'trim|xss_clean');			
		}

		if($this->form_validation->run() == FALSE){ 
			$this->session->set_userdata('msg', array('text'=> '註冊失敗' , 'color' => 'danger'));
			redirect(base_url('member/register'));
        }else{
			$session_key = md5($data_post['account'].date("Y-m-d H:i:s"));
			$data = array(
				'type'			=> $data_post['type'],
				'account'		=> $data_post['account'],
				'password'		=> password_hash($data_post['password'], PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),				
				'last_name'		=> $data_post['last_name'],
				'name'			=> $data_post['name'],
				'sex'			=> $data_post['sex'],
				'county'		=> $data_post['my_county'],
				'birthday'		=> $data_post['birthday'],
				'phone'			=> $data_post['phone'],
				'subscription'	=> isset($data_post['subscription'])?1:0,
				'enable'		=> 0,
				'session_key'   => $session_key,
				'session_expire'=> date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d")+1,date("Y")))
			);
			if($data_post['type']==2){
				$data['company_name']=$data_post['company_name'];
				$data['company_type']=$data_post['company_type'];
				$data['company_district']=$data_post['zipcode'];
				$data['company_address']=$data_post['company_address'];
				$data['company_web']=$data_post['company_web'];
			}
            $this->db->insert('member', $data);
			
			$member_type=($data_post['type']==1)?'個人':'公司';
			
			$link = base_url('member/verify_account')."/".$session_key;				
			$mail = $this->get_mail('verify');				
			$subject = $mail['subject'];
			$content = $mail['content'];
			$content = preg_replace( '/\#year\#/', date("Y"), $content);
			$content = preg_replace( '/\#month\#/', date("m"), $content);
			$content = preg_replace( '/\#date\#/', date("d"), $content);
			$content = preg_replace( '/\#type\#/', $member_type, $content);
			$content = preg_replace( '/\#link\#/', $link, $content);
			$this->send_email($data_post['account'],$subject,$content);
			
			$this->session->set_userdata('msg', array('text'=> '請前往您填寫的電子信箱收取email驗證信件，並點擊信中連結以完成註冊。' , 'color' => 'success'));
			redirect(base_url('home'));
        } 	
	}
	
	public function check_account_exist($account) 
    {
		$data = $this->db->select('*')
						 ->from('member')
						 ->where('account',$account)
						 ->get()->row_array();
		return $data;
    }
	
	//忘記密碼驗證及發送Email
	public function forget(){
		if(!$this->input->post()) redirect(base_url('member/login'));
		//驗證碼驗證
		$this->load->library('securimage/securimage');
		$img = new Securimage();
		if( !$img->check( $this->input->post('forget_captcha',true) ) ) {
			echo json_encode('驗證碼錯誤!!');
			return;
		}
		$this->form_validation->set_rules('forget_usr', 'Email', 'trim|required|xss_clean');
		if($this->form_validation->run() == true){ 					
			$email = $this->input->post('forget_usr',true);		
			$this->db->select('*')
					 ->from('member')
					 ->where('account',$email);
			$data = $this->db->get()->row_array();	
            if($data){		//帳號存在								
				$session_key = md5($email.date("Y-m-d H:i:s"));				
				
				$link = base_url('member/reset_pwd')."/".$session_key;				
				$mail = $this->get_mail('forget');				
				$subject = $mail['subject'];
				$content = $mail['content'];
				$content = preg_replace( '/\#year\#/', date("Y"), $content);
				$content = preg_replace( '/\#month\#/', date("m"), $content);
				$content = preg_replace( '/\#date\#/', date("d"), $content);
				$content = preg_replace( '/\#link\#/', $link, $content);
				$this->send_email($email,$subject,$content);
				
				$key = array(
					'session_key'   => $session_key,
					'session_expire'=> date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d")+1,date("Y")))
				);
				$this->db->where('account',$email)
						 ->update('member',$key);	
				echo json_encode(1);
				return;	
			}
			else{												//帳號不存在
				echo json_encode('此帳號不存在!');
				return;
			}
        } 	
	}
	
	//修改密碼
	public function change_pwd(){
		if(!$this->input->post()) redirect (base_url('login'));
        $pwd = $this->input->post("new_pwd",true);
        $hash = $this->input->post("hash",true);            
		$data = array(
			'password'      =>  password_hash($pwd, PASSWORD_BCRYPT, array('cost' => ENCRYPT_LENGTH)),
			'session_key'   =>  ''
		);
		$this->db->where('session_key',$hash)
				 ->update('member',$data);
		$this->session->set_userdata('msg', array('text'=> '密碼更改成功' , 'color' => 'success'));			
		redirect (base_url('member/login'));	
	}
	
	//AJAX判斷帳號是否已存在
	public function is_access()
	{
		$data = $this->db->select('id')
						 ->from('member')
						 ->where('account',$this->input->get('account',TRUE))
						 ->get()->row_array();
		if($data){				
			header('HTTP/1.1 302');
			die();
		}		
		else{
			header('HTTP/1.1 200');
			die();
		}	
	}
	
}
  