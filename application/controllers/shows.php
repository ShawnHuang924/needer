<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shows extends MY_Controller {

	function __construct()
	{
		parent:: __construct();	
	}
	
	//篩選
	public function filter(){ 
		$data_post = $this->input->post(NULL,TRUE);
		if($this->session->userdata('logged_in'))
			$user_id = $this->session->userdata('logged_in')['id'];	
		else
			$user_id = 0;
		//$data = $this->stage_model->get_stage($data_post);
		extract($data_post);
		$limit = 10;		
        $this->db->select("show_info.id,show_info.performer_name,show_info.activity_name,a.love_show_id as favorite,DATE_FORMAT(show_info.start_date,'%m/%d') as dates,DATE_FORMAT(show_info.start_date,'%a') as week,stage_type.type,show_info.price,a.id as member_id,c.filename,b.name,county.county_name,CONCAT(stage_type.key,'-c',area.code,a.id,'-',show_info.id) as show_code",FALSE)
				 ->from('show_info')				 
				 ->join('member as a','a.id = show_info.member_id','left')
				 ->join('member as b','b.id = show_info.performer_id','left')
				 ->join('county','a.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM show_favorite WHERE member_id='.$user_id.' ) as a','a.love_show_id = show_info.id','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=a.id','left')
				 ->where('show_info.end_date > NOW()')
				 ->where('show_info.enable',1)
				 ->where('a.enable',1);
        switch($action){
			case "filter":
				//篩選
				if(!empty($stage_type) && !in_array(0,$stage_type)){
					$this->db->where_in('show_info.show_type', $stage_type);					
				}					
				if(!empty($area) && !in_array(0,$area)){
					$this->db->where_in('county.area', $area);	
				}
				if(!empty($county) && !in_array(0,$county)){
					$this->db->where_in('county.id', $county);	
				}
				//if($start_date!='' && $end_date!=''){
					$this->db->where("show_info.start_date >= '".$start_date."-01'");	
					$this->db->where("show_info.start_date <= '".$end_date."-31'");	
				//}
				if($start_price!='' && $end_price!=''){
					$this->db->where('show_info.price between '.$start_price.' and '.$end_price);	
				}								
				//排序
				
				switch($sort){
					case 1:
						$this->db->order_by("show_info.start_date", "DESC");						
						break;
					case 2:
						$this->db->order_by("show_info.start_date", "ASC");	
						break;
					case 3:
						$this->db->order_by("county.id", "ASC");							
						break;
					case 4:
						$this->db->order_by("county.id", "DESC");
						break;
					case 5:
						$this->db->order_by("show_info.price", "ASC");							
						break;
					case 6:
						$this->db->order_by("show_info.price", "DESC");
						break;	
				}
				$this->db->limit($limit,$page*$limit);
				break;
			//搜尋	
			case "search":
				if($search=='')
					exit;
				$this->db->like('show_info.activity_name', $search);					
				break;
        }   		
		
		$this->db->where('show_info.start_date >= NOW()');
        $data = $this->db->get()->result_array();  
		
		
		//echo "<pre>".print_r($data, true)."</pre>";
		//echo $this->db->last_query();					
		echo json_encode($data);
	}
	//加入最愛
	public function love(){ 
		if(!$this->session->userdata('logged_in'))
			exit;
		$data = array(
		   'member_id' 		=> $this->session->userdata('logged_in')['id'] ,
		   'love_show_id' 	=> $this->input->post('id',TRUE)
		);
		$this->db->insert('show_favorite', $data); 
	}
	
	//移除最愛
	public function notlove(){
		if(!$this->session->userdata('logged_in'))
			exit;	
		$this->db->where('member_id', $this->session->userdata('logged_in')['id']);
		$this->db->where('love_show_id', $this->input->post('id',TRUE));
		$this->db->delete('show_favorite');
	}
	
	
	/*public function test(){ 
		$query = $this->db->query('INSERT INTO show_info (activity_name, date_type, activity_time,price,member_id) SELECT activity_name, date_type, activity_time,price,member_id FROM show_info;');	
	}*/
	
}
  