<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other extends MY_Controller {

	function __construct()
	{
		parent:: __construct();		
	}
	
	public function index()
	{
		
	}		
	
	public function contact_us()
	{
		$data_post = $this->input->post(NULL,TRUE);
		$data['img']=$this->captcha_img();
		//VIEW新增頁面
		if(!is_array($data_post))
		{			
			$this->template('other/contact_us',$data);		
		//POST新增頁面資料	
		}else{
			$this->form_validation->set_rules('contact_name', 	'聯絡姓名', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 			'電子信箱', 'trim|required|xss_clean');
			$this->form_validation->set_rules('phone', 			'聯絡電話', 'trim|required|xss_clean');
			$this->form_validation->set_rules('subject', 		'問題主旨', 'trim|required|xss_clean');
			$this->form_validation->set_rules('problem', 		'問題說明', 'trim|required|xss_clean');	
			$this->form_validation->set_message('required', '『%s』欄位必填');	
			if($this->form_validation->run() == FALSE)
			{
				$data['msg']=array(
					'text'=> validation_errors(),
					'color' => 'danger'
				);
				$this->template('other/contact_us',$data);
			}else{
				//驗證碼驗證
				$this->load->library('securimage/securimage');
				$img = new Securimage();
				if( !$img->check( $data_post['captcha'] ) ) {		
					$data['msg']=array(
						'text'=> '驗證碼錯誤!!',
						'color' => 'danger'
					);
					$this->template('other/contact_us',$data);
				}else{
					$data = array(
					   'contact_name' 	=> $data_post['contact_name'],
					   'email' 			=> $data_post['email'],
					   'phone' 			=> $data_post['phone'],
					   'subject' 		=> $data_post['subject'],
					   'problem' 		=> $data_post['problem']
					);
					$this->db->insert('contact_us', $data);
					
					//寄信
					$mail = $this->get_mail('contact_us');				
					$subject = $mail['subject'];
					$content = $mail['content'];
					$content = preg_replace( '/\#year\#/', date("Y"), $content);
					$content = preg_replace( '/\#month\#/', date("m"), $content);
					$content = preg_replace( '/\#date\#/', date("d"), $content);
					$content = preg_replace( '/\#contact_name\#/', $data_post['contact_name'], $content);
					$content = preg_replace( '/\#email\#/', $data_post['email'], $content);
					$content = preg_replace( '/\#phone\#/', $data_post['phone'], $content);
					$content = preg_replace( '/\#subject\#/', $data_post['subject'], $content);
					$content = preg_replace( '/\#problem\#/', $data_post['problem'], $content);
					$this->send_email($data_post['email'],$subject,$content);					
					
					$this->session->set_userdata('msg', array('text'=> '謝謝您的耐心填寫以及寶貴的意見，我們將以最快的速度回覆您！' , 'color' => 'success'));	
					redirect(base_url('contact_us'));					
				}
							
			}
		}	
	}	
	
	
	public function advertising()
	{
		$data_post = $this->input->post(NULL,TRUE);
		$data['img']=$this->captcha_img();
		//VIEW新增頁面
		if(!is_array($data_post))
		{			
			$this->template('other/advertising',$data);
		//POST新增頁面資料	
		}else{
			$this->form_validation->set_rules('company_name', 	'公司名稱', 'trim|required|xss_clean');
			$this->form_validation->set_rules('company_web', 	'公司網址', 'trim|required|xss_clean');
			$this->form_validation->set_rules('contact_name', 	'連絡姓名', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 			'電子信箱', 'trim|required|xss_clean');
			$this->form_validation->set_rules('phone', 			'連絡電話', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('ext', 			'分機號碼', 'trim|xss_clean');	
			$this->form_validation->set_rules('remark', 		'備註詳情', 'trim|xss_clean');	
			$this->form_validation->set_message('required', '『%s』欄位必填');	
			if($this->form_validation->run() == FALSE)
			{
				$data['msg']=array(
					'text'=> validation_errors(),
					'color' => 'danger'
				);
				$this->template('other/advertising',$data);
			}else{
				//驗證碼驗證
				$this->load->library('securimage/securimage');
				$img = new Securimage();
				if( !$img->check( $data_post['captcha'] ) ) {		
					$data['msg']=array(
						'text'=> '驗證碼錯誤!!',
						'color' => 'danger'
					);
					$this->template('other/advertising',$data);
				}else{
					$data = array(
					   'company_name' 	=> $data_post['company_name'],
					   'company_web' 	=> $data_post['company_web'],
					   'contact_name' 	=> $data_post['contact_name'],
					   'email' 			=> $data_post['email'],
					   'phone' 			=> $data_post['phone'],
					   'ext' 			=> $data_post['ext'],
					   'remark' 		=> $data_post['remark']
					);
					$this->db->insert('advertising', $data);	
						
					//寄信		
					$mail = $this->get_mail('advertising');				
					$subject = $mail['subject'];
					$content = $mail['content'];
					$content = preg_replace( '/\#year\#/', date("Y"), $content);
					$content = preg_replace( '/\#month\#/', date("m"), $content);
					$content = preg_replace( '/\#date\#/', date("d"), $content);
					$content = preg_replace( '/\#company_name\#/', $data_post['company_name'], $content);
					$content = preg_replace( '/\#company_web\#/', $data_post['company_web'], $content);
					$content = preg_replace( '/\#contact_name\#/', $data_post['contact_name'], $content);
					$content = preg_replace( '/\#email\#/', $data_post['email'], $content);
					$content = preg_replace( '/\#phone\#/', $data_post['phone'].' '.$data_post['ext'], $content);
					$content = preg_replace( '/\#remark\#/', $data_post['remark'], $content);
					$this->send_email($data_post['email'],$subject,$content);
					
					$this->session->set_userdata('msg', array('text'=> '謝謝您的耐心填寫，我們將以最快的速度與您連絡！' , 'color' => 'success'));	
					redirect(base_url('advertising'));					
				}
							
			}
		}	
	}
	
	
	/*public function contact_check()
	{
		
		//if(!$this->input->post()) redirect(base_url('contact_us'));
		$data_post = $this->input->post(NULL,TRUE);
		$this->form_validation->set_rules('contact_name', 	'聯絡姓名', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 			'電子信箱', 'trim|required|xss_clean');
		$this->form_validation->set_rules('phone', 			'聯絡電話', 'trim|required|xss_clean');
		$this->form_validation->set_rules('subject', 		'問題主旨', 'trim|required|xss_clean');
		$this->form_validation->set_rules('problem', 		'問題說明', 'trim|required|xss_clean');	
		$this->form_validation->set_message('required', '%s欄位必填');	
		if($this->form_validation->run() == FALSE)
		{
			$data['msg']=array(
				'text'=> validation_errors(),
				'color' => 'danger'
			);
			$this->contact_us($data);
		}else{
			//驗證碼驗證
			$this->load->library('securimage/securimage');
			$img = new Securimage();
			if( !$img->check( $data_post['captcha'] ) ) {			
				$data['msg']=array(
					'text'=> '驗證碼錯誤!!',
					'color' => 'danger'
				);
				$this->contact_us($data);
				exit;
			}
			$data = array(
			   'contact_name' 	=> $data_post['contact_name'],
			   'email' 			=> $data_post['email'],
			   'phone' 			=> $data_post['phone'],
			   'subject' 		=> $data_post['subject'],
			   'problem' 		=> $data_post['problem']
			);
			$this->db->insert('contact_us', $data);
			$this->session->set_userdata('msg', array('text'=> '發送成功!!' , 'color' => 'success'));	
			redirect(base_url('contact_us'));			
		}
		
	}	
	
	public function advertisting_check()
	{
		if(!$this->input->post()) redirect(base_url('advertising'));
		$data_post = $this->input->post(NULL,TRUE);
		//驗證碼驗證
		$this->load->library('securimage/securimage');
        $img = new Securimage();
        if( !$img->check( $data_post['captcha'] ) ) {
            $this->session->set_userdata('msg', array('text'=> '驗證碼錯誤!!' , 'color' => 'danger'));
			redirect(base_url('advertising'));  
        }
		$data = array(
		   'company_name' 	=> $data_post['contact_name'],
		   'company_web' 	=> $data_post['company_web'],
		   'contact_name' 	=> $data_post['contact_name'],
		   'email' 			=> $data_post['email'],
		   'phone' 			=> $data_post['phone'],
		   'ext' 			=> $data_post['ext'],
		   'remark' 		=> $data_post['remark']
		);
		$this->db->insert('advertising', $data);
		redirect(base_url('advertising'));
	}	
	*/
}
  