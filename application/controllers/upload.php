<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {

  /**
   * 建構式，預設載入模組 {}
   */
	function __construct()
	{
		parent:: __construct();		
	}
	
	
	
	//初始化單圖上傳
	public function init_single_upload($path){
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		require_once './application/libraries/UploadHandler.php';			
		$user_id = $this->session->userdata('logged_in')['id'];
		$new_path = str_replace('-','/',$path);
		$options = array (
			'script_url' => base_url().'upload/init_single_upload/'.$path ,
			'upload_dir' => ROOT.'/public/photos/member/'.$user_id.'/'.$new_path .'/',			
			'upload_url' => base_url().'public/photos/member/'.$user_id.'/'.$new_path .'/'
		);
		$upload_handler = new UploadHandler($options);
	}

	//初始化多圖上傳
	public function init_multiple_upload(){
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		require_once './application/libraries/UploadHandler.php';		
		$id = $this->session->userdata('logged_in')['id'];
		$options = array (
			'script_url' => base_url().'upload/init_multiple_upload',
			'upload_dir' => ROOT.'/public/photos/member/'.$id.'/' ,			
			'upload_url' => base_url().'public/photos/member/'.$id.'/',
			'image_versions' => array( 'thumbnail' => array('max_width' => 360, 'max_height' => 600 ) )
		);
		$upload_handler = new UploadHandler($options);
	}
	
	
	//切圖
	/*public function crop_cover1($dir){
		if( !$this->input->post() ) redirect(base_url());
		$post_data = $this->input->post(NULL,true);
		$targ_w = 300*$this->input->post("cover_ratio",true);
		$targ_h = 300;
		$ratio= $post_data['origin_w']/$post_data['now_w'];
		$jpeg_quality = 100;  
		$user_id = $this->session->userdata('logged_in')['id']; 
		$dir = str_replace('-','/',$dir);			
		$path = '/public/photos/tmp/'.$user_id.'/'.$dir;
		if(is_array(glob(ROOT.$path.'/*.*')))
			$fullname = glob(ROOT.$path.'/*.*')[0];
		$filename =basename($fullname);
		$url = str_replace('/admin/','',base_url());		
		$img_r = imagecreatefromjpeg($url.$path.'/'.$filename);  
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h ); 		
		imagecopyresampled($dst_r,$img_r,0,0, $post_data['x']*$ratio,$post_data['y']*$ratio, 
							  $targ_w,$targ_h,$post_data['w']*$ratio,$post_data['h']*$ratio);  		
		header('Content-type: image/jpeg');  
		imagejpeg ($dst_r, $fullname, $jpeg_quality);			
	}*/
	
	//切圖
	public function crop_cover($dir){
		if( !$this->input->post() ) redirect(base_url());
		$post_data = $this->input->post(NULL,true);
		$user_id = $this->session->userdata('logged_in')['id'];
		$targ_w = 300*$this->input->post("cover_ratio",true);
		$targ_h = 300;
		$ratio= $post_data['origin_w']/$post_data['now_w'];
		$png_quality = 9; 
		
		$dir = str_replace('-','/',$dir);			
		$path = '/public/photos/tmp/'.$user_id.'/'.$dir;
		if(is_array(glob(ROOT.$path.'/*.*')))
			$fullname = glob(ROOT.$path.'/*.*')[0];
		$sub = end(explode(".",$fullname));
		$new_fullname = str_replace($sub,'png',$fullname);	
		
		$filename =basename($fullname);
		$url = str_replace('/admin/','',base_url());

		if($sub=='jpg')
			$img_r = imagecreatefromjpeg($url.$path.'/'.$filename); 
		else if($sub=='png')
			$img_r = imagecreatefrompng($url.$path.'/'.$filename); 
		
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h ); 		
		imagecopyresampled($dst_r,$img_r,0,0, $post_data['x']*$ratio,$post_data['y']*$ratio, $targ_w,$targ_h,$post_data['w']*$ratio,$post_data['h']*$ratio);  		
		header('Content-type: image/png');  
		imagepng ($dst_r, $new_fullname, $png_quality);	
		if($sub=='jpg')
			unlink($fullname);	
	}
	
	//相簿上傳照片
	public function upload_photos(){
		if( !$this->session->userdata('logged_in') ) redirect(base_url());		
		$filename = $this->input->post("filename",true);
		$user_id = $this->session->userdata('logged_in')['id'];
		
		$this->db->select('id')
				 ->from('member_photos')
				 ->where('user_id', $user_id);
		$result = $this->db->get();
		if($result->num_rows()>0){
			$cover=0;
			echo json_encode(true);
		}			
		else{
			$cover=1;
			echo json_encode(false);
		}			
		$data = array(
			'user_id'	=> $user_id,							
			'filename'	=> $filename,
			'cover'		=> $cover
		);	
		$this->db->insert('member_photos', $data);		
	}
	
	//刪除照片
	public function delete_photos(){
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		$filename = $this->input->post("filename",true);
		$user_id = $this->session->userdata('logged_in')['id'];
		$this->db->where('user_id', $user_id)
				 ->where('filename', $filename)
				 ->delete('member_photos');			
	}
	
	
	
	//設置封面
	public function set_cover(){	
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		$filename = $this->input->post("filename",true);
		$user_id = $this->session->userdata('logged_in')['id'];
		$this->db->where('user_id', $user_id)
				 ->update('member_photos', array('cover'=>0));	
		$this->db->where('user_id', $user_id)
				 ->where('filename', $filename)
				 ->update('member_photos', array('cover'=>1));		
	}
		
	//取得封面
	public function get_cover(){		
		if( !$this->session->userdata('logged_in') ) redirect(base_url());
		$user_id = $this->session->userdata('logged_in')['id'];
		$this->db->select('filename')
				 ->from('member_photos')
				 ->where('user_id', $user_id)
				 ->where('cover', 1);
		$result = $this->db->get()->row_array();
		echo json_encode($result);
	}
	
	
	//
	/*public function resave_photos(){
		if( !$this->input->post() ) redirect(base_url());
		$filename = $this->input->post("filename",true);
		$type = 'member';		
		$user_id = $this->session->userdata('logged_in')['id'];
		foreach($filename as $val)
			echo $val.'<br>';
		
		$this->db->where('user_id', $user_id)				 
				 ->delete($type.'_photos');	
		foreach($filename as $val){
			$data = array(
				'user_id'	=> $user_id,							
				'filename'	=> $val
			);	
			$this->db->insert($type.'_photos', $data);		
			$insert_id = $this->db->insert_id();
			$data2 = array(
				'user_id'	=> $user_id,							
				'data_no'	=> $insert_id
			);	
			$this->db->insert($type.'_photos_sequence', $data2);			
		}		 
		
	}*/
	
}
  