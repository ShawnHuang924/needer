<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends MY_Controller {

	function __construct()
	{
		parent:: __construct();
		$this->load->model('common_model');
		$this->load->model('myaccount_model');
	}
	
	public function index()
	{		
		$this->basic_info();		
	}	
	
	/*********VIEW******************************/
	
	//基本資料
	public function basic_info()
	{	
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('home'));
		$id = $this->session->userdata('logged_in')['id'];
		$data 					= $this->myaccount_model->get_member_info($id);
		$data['history'] 		= $this->myaccount_model->get_history_info($id);
		$data['license'] 		= $this->myaccount_model->get_license_info($id);
		$data['county'] 		= $this->common_model->get_all_county();
		$data['theme'] 			= $this->common_model->get_from_mapping('theme');	
		$data['company_type'] 	= $this->common_model->get_from_mapping('company_type');
		$data['detail_type'] 	= $this->get_detail_type2(1);
		$data['stage_type'] 	= $this->common_model->get_all_stage_type();
		$data['week'] 			= $this->common_model->get_from_mapping('week');
		$data['length'] 		= $this->common_model->get_from_mapping('length');	
		$data['contact'] 		= $this->common_model->get_from_mapping('contact');		
		$data['supply_item'] 	= $this->common_model->get_from_mapping('supply_item');	
		$data['supply_device'] 	= $this->common_model->get_from_mapping('supply_device');	

		if(count($data['history'])==0){
			$data['history'][0]['year']=0;
			$data['history'][0]['content']='';
		}
		if(count($data['license'])==0){
			$data['license'][0]['year']=0;
			$data['license'][0]['content']='';
		}	
		foreach($data['themes'] as $key => $val){
			$data['themes'][$key]=$val['theme_id'];				
		}	
		foreach(explode(',',$data['info']['supply_item']) as $key => $val){
			$data['supply_items'][$key]=$val;				
		}
		foreach(explode(',',$data['info']['supply_device']) as $key => $val){ 
			$data['supply_devices'][$key]=$val;				
		}
		//echo "<pre>".print_r($data, true)."</pre>";
		$this->template('myaccount/basic_info',$data);
	}
	
	//最愛舞台
	public function favorite_stage()
	{	
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('home'));
		$user_id = $this->session->userdata('logged_in')['id'];		
		$this->db->select("member.id,CONCAT('c',area.code,member.id) as company_code,c.filename,member.company_name,county.county_name,GROUP_CONCAT(DISTINCT(stage_type.type) ORDER BY stage_type.id) as stage_type,a.love_member_id as favorite",FALSE)
				 ->from('stage_info')				 
				 ->join('member','member.id = stage_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = stage_info.stage_type','left')				 
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->join('(SELECT * FROM stage_favorite WHERE member_id='.$user_id.' ) as a','a.love_member_id = member.id','right')
				 ->group_by("member.id")
				 ->where('member.enable',1);	
		$data['favorite_stage'] = $this->db->get()->result_array();
		//echo "<pre>".print_r($data, true)."</pre>";			
		$this->template('myaccount/favorite_stage',$data);
	}
	
	//最愛展演
	public function favorite_show()
	{	
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('home'));
		$user_id = $this->session->userdata('logged_in')['id'];	
		$this->db->select("show_info.id,show_info.activity_name,a.love_show_id as favorite,DATE_FORMAT(show_info.start_date,'%m/%d') as dates,DATE_FORMAT(show_info.start_date,'%a') as week,stage_type.type,show_info.price,a.id as member_id,c.filename,b.name,county.county_name,CONCAT(stage_type.key,'-c',area.code,a.id,'-',show_info.id) as show_code",FALSE)
				 ->from('show_info')				 
				 ->join('member as a','a.id = show_info.member_id','left')
				 ->join('member as b','b.id = show_info.performer_id','left')
				 ->join('county','a.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=a.id','left')
				 ->join('(SELECT * FROM show_favorite WHERE member_id='.$user_id.' ) as a','a.love_show_id = show_info.id','right')
				 ->where('a.enable',1);	
		$data['favorite_show'] = $this->db->get()->result_array();		 
		//echo "<pre>".print_r($data, true)."</pre>";		
		$this->template('myaccount/favorite_show',$data);
	}
	
	//我的舞台
	public function mystage()
	{		
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('home'));
		$id = $this->session->userdata('logged_in')['id'];
		$data 					= $this->myaccount_model->get_member_info($id);
		$data['contact'] 		= $this->common_model->get_from_mapping('contact');		
		$data['supply_item'] 	= $this->common_model->get_from_mapping('supply_item');	
		$data['supply_device'] 	= $this->common_model->get_from_mapping('supply_device');
		foreach(explode(',',$data['info']['supply_item']) as $key => $val){
			$data['supply_items'][$key]=$val;				
		}
		foreach(explode(',',$data['info']['supply_device']) as $key => $val){ 
			$data['supply_devices'][$key]=$val;				
		}
		$this->db->select("stage_info.*,CONCAT(YEAR(start_date),'/',MONTH(start_date),'~',YEAR(end_date),'/',MONTH(end_date)) as ex_date,DATE_FORMAT(stage_info.start_time,'%H:%i') as start_time,DATE_FORMAT(stage_info.end_time,'%H:%i') as end_time,b.value as stage",FALSE)
				 ->from('stage_info')
				 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
				 ->where('member_id',$id)
				 ->order_by("stage_info.stage_type",'asc');
		$data['stages'] = $this->db->get()->result_array();
		$this->template('myaccount/mystage',$data);
	}
	
	
	//媒合提報
	public function match()
	{
		if(!$this->session->userdata('logged_in'))
			redirect(base_url('home'));	
		$id = $this->session->userdata('logged_in')['id'];
		$data 					= $this->myaccount_model->get_member_info($id);
		$data['stage_type'] 	= $this->common_model->get_all_stage_type();	
		$data['notice_type'] 	= $this->common_model->get_from_mapping('notice');		
		$this->clean_mytmp();
		$this->template('myaccount/match',$data);
	}
	
	
	
	
	
	/*******功能**************************/
	
	//儲存提供者個人資料
	public function save_info(){	
		$id = $this->session->userdata('logged_in')['id'];
		$data_post = $this->input->post(NULL,TRUE);	
		$this->validation();
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{		
			$data=array(
				'last_name'			=> $data_post['last_name'],		
				'name'  	  		=> $data_post['name'],
				'sex' 				=> (int)$data_post['sex'],
				'phone'  			=> $data_post['phone'],
				'county'  			=> $data_post['city'],				
				'birthday'			=> $data_post['birthday'],				
				'video_web'			=> $data_post['video_web'],				
				'pic_web' 			=> $data_post['pic_web'],	
				'personal_web' 		=> $data_post['personal_web'],					
				'update_time' 		=> date("Y-m-d H:i:s")
			);
			if($this->session->userdata('logged_in')['type']==2){
				$data['company_type']		= $data_post['company_type'];
				$data['company_district']   = $data_post['company_district'];
				$data['company_address'] 	= $data_post['company_address'];
				$data['company_web'] 		= $data_post['company_web'];
				/*$data['contact']			= $data_post['contact'];
				$data['contact_start_time'] = $data_post['contact_start_time'];
				$data['contact_end_time'] 	= $data_post['contact_end_time'];
				$data['supply_item'] 		= isset($data_post['supply_item']) ? implode(",", $data_post['supply_item']) : '';
				$data['other_supply_item']	= $data_post['other_item'];
				$data['supply_device'] 	 	= isset($data_post['supply_device']) ? implode(",", $data_post['supply_device']) : '';
				$data['other_supply_device']= $data_post['other_device'];*/
			}			
			
			$this->db->update('member', $data, array('id' => $id));
			$this->myaccount_model->save_theme($id,$data_post['theme']);
							
			
			//經歷
			$this->db->where('member_id', $id);
			$this->db->delete('history');
			$result=array();
			$data=array();
			foreach( $data_post['history_year'] as $key => $val){
				if( $val=='0' )
					continue;	
				$data=array(
					'member_id'	=>	$id,
					'year'		=>	$data_post['history_year'][$key],
					'content'	=>	$data_post['history_content'][$key]
				);
				array_push($result, $data);
			}
			if(count($result)>0)
				$this->db->insert_batch('history', $result);
			//echo "<pre>".print_r($data_post, true)."</pre>";
			
			//證照資格
			$this->db->where('member_id', $id);
			$this->db->delete('license');
			$result=array();
			$data=array();			
			foreach( $data_post['license_year'] as $key => $val){	
				if( $val=='0' )
					continue;
				$data=array(
					'member_id'	=>	$id,
					'year'		=>	$data_post['license_year'][$key],
					'content'	=>	$data_post['license_content'][$key]
				);
				array_push($result, $data);
			}
			if(count($result)>0)
				$this->db->insert_batch('license', $result);
			
			echo json_encode('儲存成功');
		}
    }
	
	//儲存提供者個人資料
	public function save_mystage_info(){	
		$id = $this->session->userdata('logged_in')['id'];
		$data_post = $this->input->post(NULL,TRUE);
		$rule='trim|xss_clean';	
		$this->form_validation->set_rules('contact', 		 	'希望被表演者之聯繫方式',  	$rule.'|required'); 
		$this->form_validation->set_rules('contact_start_time', '希望被聯繫時段',  			$rule.'|required'); 
		$this->form_validation->set_rules('contact_end_time', 	'希望被聯繫時段',  			$rule.'|required'); 
		$this->form_validation->set_rules('supply_item[]', 		'可供應之項目',				$rule);
		$this->form_validation->set_rules('other_supply_item', 	'其他',						$rule);
		$this->form_validation->set_rules('supply_device[]', 	'可提供之設備',  			$rule); 
		$this->form_validation->set_rules('other_supply_device','其他',  					$rule);	
		//驗證失敗	
		if($this->form_validation->run() == FALSE){ 
			echo json_encode('資料有誤');				
		//驗證成功	
		}else{		
			$data=array(
				'contact'				=> $data_post['contact'],		
				'contact_start_time'  	=> $data_post['contact_start_time'],
				'contact_end_time' 		=> $data_post['contact_end_time'],
				'supply_item'  			=> isset($data_post['supply_item']) ? implode(",", $data_post['supply_item']) : '',
				'other_supply_item'  	=> $data_post['other_item'],				
				'supply_device'			=> isset($data_post['supply_device']) ? implode(",", $data_post['supply_device']) : '',				
				'other_supply_device'	=> $data_post['other_device']	
			);			
			$this->db->update('member', $data, array('id' => $id));
			echo json_encode('儲存成功');
		}
    }
	
	
	
	//表單驗證
	public function validation(){
		$rule='trim|xss_clean';		
		$this->form_validation->set_rules('last_name', 	     '姓',		  $rule.'|required'); 
		$this->form_validation->set_rules('name',			 '名', 		  $rule.'|required');
		$this->form_validation->set_rules('sex', 			 '性別', 	  $rule.'|required'); 
		$this->form_validation->set_rules('phone',			 '聯絡電話',  $rule.'|required');
		$this->form_validation->set_rules('city', 			 '居住城市',  $rule.'|required'); 
		$this->form_validation->set_rules('birthday', 		 '出生日期',  $rule.'|required'); 
		$this->form_validation->set_rules('video_web', 		 '影音網址',  $rule); 
		$this->form_validation->set_rules('pic_web', 		 '相簿網址',  $rule); 
		$this->form_validation->set_rules('personal_web', 	 '個人網址',  $rule); 
		$this->form_validation->set_rules('theme[]', 		 '感興趣主題',$rule.'|required'); 
		if($this->session->userdata('logged_in')['type']==2){
			$this->form_validation->set_rules('company_name', 	 '公司名稱',  $rule.'|required'); 
			$this->form_validation->set_rules('company_type', 	 '公司類型',  $rule.'|required'); 
			$this->form_validation->set_rules('company_district','公司縣市',  $rule.'|required'); 
			$this->form_validation->set_rules('company_address', '公司地址',  $rule.'|required');
			$this->form_validation->set_rules('company_web', 	 '公司網址',  $rule);
		}
	}	
	
	
	//新增媒合提報
	public function save_match(){	
		$id = $this->session->userdata('logged_in')['id'];
		$data_post = $this->input->post(NULL,TRUE);	
		//echo "<pre>".print_r($data_post, true)."</pre>";
		$data=array(
			'show_type'		=> 	$data_post['show_type'],
			'activity_name'	=> 	$data_post['activity_name'],
			'intro'			=> 	$this->input->post('intro',FALSE),
			'start_date'	=> 	$data_post['start_date'],
			'end_date'		=> 	$data_post['end_date'],
			'start_time'	=> 	$data_post['start_time'],
			'end_time'		=> 	$data_post['end_time'],
			'price'			=> 	$data_post['price'],
			'price_limit'	=> 	$data_post['price_limit'],
			'performer_name'=> 	$data_post['performer_name'],
			'performer_email'=> $data_post['performer_email'],
			'company_name'	=> 	$data_post['company_name'],
			'company_web'	=> 	$data_post['company_web'],
			'company_address'=> $data_post['company_address'],
			'member_id'		=>	$id,
			'is_verify'		=>	0,
			'enable'		=>	0	
		);
		//echo "<pre>".print_r($data, true)."</pre>";
		$this->db->insert('show_info', $data) ;
		$insert_id = $this->db->insert_id();
		
		//注意事項
		$result=array();
		$data=array();
		//echo "<pre>".print_r($data_post['notices'], true)."</pre>";die();
		if( isset($data_post['notices']) ){
			foreach( $data_post['notices'] as $key => $val){
				$data=array(
					'show_id'		=>	$insert_id,
					'notices_id'	=>	$val
				);
				array_push($result, $data);
			}
			$this->db->insert_batch('show_notices', $result);
		}
		
		$this->move_show($insert_id);        
		
		redirect(base_url('myaccount/match'));
		//echo json_encode('儲存成功');		
    }
	
	
	public function get_detail_type(){
		$id = $this->input->post('id', true);
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		echo json_encode($detail_type);	 
    }
	
	public function get_detail_type2($id){
		$this->db->select('*')
				 ->from('stage_type')
				 ->where('id',$id);
		$data = $this->db->get()->row_array();
		$detail_type=explode(",",$data['detail_type']);	
		
		return $detail_type;	 
    }
	
	//AJAX判斷表演者帳號是否存在
	public function is_access()
	{
		//echo $this->input->get('performer_email',TRUE);
		$data = $this->db->select('id')
						 ->from('member')
						 ->where('account',$this->input->get('performer_email',TRUE))
						 ->get()->row_array();
		if(!$data){				
			header('HTTP/1.1 302');
			die();
		}		
		else{
			header('HTTP/1.1 200');
			die();
		}
	}
	
	
}
  