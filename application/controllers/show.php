<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show extends MY_Controller {

	function __construct()
	{
		parent:: __construct();	
		$this->load->model('myaccount_model');
		$this->load->model('common_model');
	}
	
	public function index()
	{
		$data['county'] = $this->get_all_county();
		$this->db->select("show_info.*,CONCAT(stage_type.key,'-c',area.code,a.id,'-',show_info.id) as show_code",FALSE)
				 ->from('show_info')
				 ->join('member as a','a.id = show_info.member_id','left')
				 ->join('county','a.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')				 
				 ->where('show_info.end_date > NOW()')
				 ->where('show_info.enable',1)
				 ->where('a.enable',1)
				 ->order_by("show_info.id",'random')
				 ->limit(5);
		$data['other'] = $this->db->get()->result_array();	
		
		$this->template('show/type',$data);
	}		
	
	
	//用代號取得詳細頁
	public function detail_id($code)
	{
		/*if(!$this->session->userdata('logged_in'))
			redirect(base_url('stage'));*/	
		$id = explode("-",$code)[2];
		$this->select_join();
		$this->db->where('show_info.id',$id);
		$data['info'] = $this->db->get()->row_array();
		$data = $this->get_other_info($id,$data);
		$data['history']= $this->get_history_info($data['info']['performer_id']);
		$data['license']= $this->get_license_info($data['info']['performer_id']);
		$data['stage_type'] = $this->common_model->get_from_mapping('stage_type');
		//echo '<pre>'.print_r($data,true).'</pre>';
		$this->template('show/detail',$data);
	}
	
	//用公司名稱取得詳細頁
	public function detail_name($show_name)
	{
		/*if(!$this->session->userdata('logged_in'))
			redirect(base_url('show'));*/
		$this->select_join();
		$this->db->where('show_info.activity_name',str_replace('_',' ',urldecode($show_name)));
		$data['info'] = $this->db->get()->row_array();		
		//$data = $this->get_other_info($data['info']['id'],$data);
		$this->template('show/detail',$data);
	}
	
	public function select_join()
	{
		
		$user_id = $this->session->userdata('logged_in')['id'];
		if($user_id)
			$this->db->select("show_info.id,a.love_show_id as favorite,show_info.activity_name,show_info.show_type,show_info.intro,DATE_FORMAT(show_info.start_date,'%m/%d') as dates,DATE_FORMAT(show_info.start_date,'%a') as week,stage_type.type,show_info.price, c.filename,b.name,county.county_name,county.area,CONCAT(district.county_name,' ', district.district_name,' ',a.company_address) as company_address,a.company_web, DATE_FORMAT(show_info.start_time,'%H:%i')as start_time, DATE_FORMAT(show_info.end_time,'%H:%i') as end_time, show_info.performer_id, show_info.performer_name, b.video_web, b.pic_web, b.personal_web, a.company_name, show_info.price_limit",FALSE);
		else		 
			$this->db->select("show_info.id,show_info.activity_name,show_info.show_type,show_info.intro,DATE_FORMAT(show_info.start_date,'%m/%d') as dates,DATE_FORMAT(show_info.start_date,'%a') as week,stage_type.type,show_info.price,c.filename,b.name,county.county_name,county.area,CONCAT(district.county_name,' ', district.district_name,' ',a.company_address) as company_address,a.company_web, DATE_FORMAT(show_info.start_time,'%H:%i')as start_time, DATE_FORMAT(show_info.end_time,'%H:%i') as end_time, show_info.performer_id, show_info.performer_name, b.video_web, b.pic_web, b.personal_web, a.company_name, show_info.price_limit",FALSE);
		$this->db->from('show_info')				 
				 ->join('member as a','a.id = show_info.member_id','left')
				 ->join('member as b','b.id = show_info.performer_id','left')
				 ->join('district','district.zipcode=a.company_district','left')
				 ->join('county','a.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=a.id','left');

		if($user_id)
			$this->db->join('(SELECT * FROM show_favorite WHERE member_id='.$user_id.' ) as a','a.love_show_id = a.id','left');				 
	}	
	
	public function get_other_info($id,$data)
	{
		//同類型同地區舞台
		$this->db->select("member.id,c.filename,show_info.activity_name,CONCAT(stage_type.key,'-c',area.code,member.id,'-',show_info.id) as show_code",FALSE)
				 ->from('show_info')				 
				 ->join('member','member.id = show_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->where('show_info.id <>',$id)
				 ->where('show_info.show_type',$data['info']['show_type'] )
				 ->where('county.area',$data['info']['area'])
				 ->order_by("show_info.id",'random')
				 ->limit(4);
		$data['other'] = $this->db->get()->result_array();
		//echo "<pre>".print_r($data, true)."</pre>";
		return $data;	
	}
	
	public function get_stage_info($id,$data)
	{
		//同類型同地區舞台
		$this->db->select("show_info.start_time, show_info.end_time",FALSE)
				 ->from('show_info')				 
				 ->join('member','member.id = show_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('area','county.area = area.id','left')
				 ->join('stage_type','stage_type.id = show_info.show_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->where('show_info.id <>',$id)
				 ->where('show_info.show_type',$data['info']['show_type'] )
				 ->where('county.area',$data['info']['area'])
				 ->order_by("show_info.id",'random')
				 ->limit(4);
		$data['other'] = $this->db->get()->result_array();
		//echo "<pre>".print_r($data, true)."</pre>";
		return $data;	
	}
		//取得經歷
	public function get_history_info($id){
		$this->db->select('history.*')
				 ->from('history')
				 ->where('history.member_id',$id);
		$date=$this->db->get()->result_array();			
		return $date; 
	}
	
	//取得證照資格
	public function get_license_info($id){
		$this->db->select('license.*')
				 ->from('license')
				 ->where('license.member_id',$id);
		$date=$this->db->get()->result_array();			
		return $date; 
	}
	
}
  