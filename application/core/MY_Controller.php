<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}	
	
	//主要template
	public function template($page_name, $data=[])
	{
		// 訊息session
		if($this->router->fetch_class()=='home')
			$data['top']='';
		else
			$data['top']='navbar-fixed-top shadow';
		
		if($this->session->userdata('msg'))
		{
			$data['msg'] = $this->session->userdata('msg');
		}

		if($this->session->userdata('logged_in'))
		{
			$data['logged_in'] = $this->session->userdata('logged_in');			
		}
		if($this->session->userdata('current_url'))
			$this->session->set_userdata('last_url', $this->session->userdata('current_url'));
		$this->session->set_userdata('current_url', current_url());
		
		$data['header'] = $this->load->view('templates/header', $data, TRUE);
		if($this->router->fetch_class()=='myaccount'){
			$data['leftmenu'] = $this->load->view('templates/leftmenu', $data, TRUE);
			$data['leftmenu_end'] = $this->load->view('templates/leftmenu_end', $data, TRUE);
		}
		$data['body'] = $this->load->view($page_name, $data, TRUE);
		$data['footer'] = $this->load->view('templates/footer', $data, TRUE);
		$this->load->view('templates/layout', $data);
		
		$this->session->unset_userdata('msg');
	}
	
	//取得使用者IP
	public function get_ip(){
        static $realIP;        
        if($realIP) return $realIP;
        //代理時
        $ip = getenv('HTTP_CLIENT_IP')?  getenv('HTTP_CLIENT_IP'):getenv('HTTP_X_FORWARDED_FOR');
        preg_match("/[\d\.]{7,15}/", $ip, $match);
        if(isset($match[0])) return $realIP = $match[0];
        //非代理時
        $ip = !empty($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR']:'0.0.0.0';
        preg_match("/[\d\.]{7,15}/", $ip, $match);		
        return $realIP = isset($match[0])? $match[0]:'0.0.0.0';
    }
	
	//取得下拉選項
	public function get_dropdown($type){
        return $this->db->select('*')
						->from('mapping')
						->where('type',$type)
						->get()->result_array();
    }
	
	//取得下拉選項
	public function get_all_county(){
        return $this->db->select('*')
						->from('county')						
						->get()->result_array();
    }
	
	public function send_email($to=null, $subject=null, $content=null, $from=0, $from_name=0){		  
		
		$this->config->load('email');	
		$config['protocol']     =  $this->config->item('protocol');
		$config['mailtype']     =  $this->config->item('mailtype');
		$config['smtp_host']    =  $this->config->item('smtp_host');
		$config['smtp_port']    =  $this->config->item('smtp_port');
		$config['smtp_user']    =  $this->config->item('smtp_user');
		$config['smtp_pass']    =  $this->config->item('smtp_pass');
		$config['validation']   =  $this->config->item('validation');
		$config['charset']      =  $this->config->item('charset');
		$config['newline']      =  $this->config->item('newline');
		$this->email->initialize($config);
		
		if(!$from && !$from_name){
			$this->email->from($config['smtp_user'],'NEEDER。你的');
		}else{
			$this->email->from($from,$from_name);
		}       
		
		$this->email->to($to); 
		$this->email->subject($subject);		
		$this->email->message($content);
		
		if(!$this->email->send()){
			echo $this->email->print_debugger();
		}
	}
	
	//驗證碼
	public function captcha_img() 
    {
        return base_url('member/securimage_jpg');
    }
	//驗證碼設定
    public function securimage_jpg() 
    {
        $this->load->library('securimage/securimage');
        $img = new Securimage();
		$img->charset = '0123456789';
        $img->image_width = 100;
        $img->image_height = 45;
        $img->perturbation = 0;	//干擾雜訊度
		$img->num_lines = 0;		//線條數
		$img->text_transparency_percentage = 20; // 100 為全透明 
		$img->image_bg_color = new Securimage_Color("#f6f6f6");
        $img->use_transparent_text = true;        
        //$img->use_wordlist = true; 		
        $img->show(); // 套背景圖並顯示		
    }
	
	//複製資料夾
	public function copydir($source, $dest){		
		if(is_dir($source)) {
			$dir_handle=opendir($source);
			while($file=readdir($dir_handle)){
				if($file!="." && $file!=".."){
					if(is_dir($source."/".$file)){
						if(!is_dir($dest."/".$file)){
							mkdir($dest."/".$file);
						}
						$this->copydir($source."/".$file, $dest."/".$file);
					} else {
						copy($source."/".$file, $dest."/".$file);
					}
				}
			}
			closedir($dir_handle);
		} else {
			copy($source, $dest);
		}
	}
	
	//刪除資料夾
	public function rrmdir($dir) {
	   if (is_dir($dir)) {
		 $objects = scandir($dir);
		 foreach ($objects as $object) {
		   if ($object != "." && $object != "..") {
			 if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
		   }
		 }
		 reset($objects);
		 rmdir($dir);
	   }
	}
	
	//將暫存的圖片移到正確的地方	
	public function move_pic()
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$dir = ROOT.'/public/photos/member/'.$user_id.'/';
		$this->rrmdir($dir.'pic');
		rename($dir.'tmp',$dir.'pic');
	}
	
	public function move_show($show_id)
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$dir = ROOT.'/public/photos/member/'.$user_id.'/tmp';
		$show_dir = ROOT.'/public/photos/show/'.$show_id;		
		rename($dir,$show_dir);
		$file=isset(glob($show_dir.'/*.*')[0])?glob($show_dir.'/*.*')[0]:false;
		if($file)
			rename($file,$show_dir.'/picture.png');		
	}
	
	//清空暫存資料夾
	public function clean_mytmp()
	{
		$user_id = $this->session->userdata('logged_in')['id'];
		$tmp_dir = ROOT.'/public/photos/member/'.$user_id.'/tmp';
		$this->rrmdir($tmp_dir);
	}
	
	public function get_mail($project)	
	{
		return $this->db->select('*')
						->from('mail')
						->where('project',$project)
						->get()->row_array();		
	}
	
	
}