<div id="main">
<div id="detail_container" class="container">
	<div id="detail" class="row show_detail">
		<?php list($width, $height) = getimagesize(ROOT.'/public/photos/show/'.$info['id'].'/picture.png');		
				if($width>$height)
					$x='12';
				else
					$x='6';
		?>
		<div class="col-sm-<?=$x?>">			
			<!--大圖-->
			<img style="width:100%" src="<?=base_url('public/photos/show/'.$info['id'].'/picture.png')?>" />			
		</div>
		<div class="col-sm-<?=$x?>">
			<h2 style="font-weight: bold;"><?=$info['dates']?><small>&nbsp&nbsp<?php echo ($info['start_time']!=$info['end_time'])?$info['start_time'].' - '.$info['end_time']:$info['start_time'];?></small></h2>
			<h2><?=$info['activity_name']?></h2>
			<h4>演出者：<?=$info['performer_name']?></h4>	<br>		
			<h4><?=$info['company_name']?></h4>
			<?php foreach( $stage_type as $key => $val ){?>	
			<?php if($info['show_type']==$val['key']){?>
			<h4>類型：<?=$val['value']?></h4>
			<?php }}?>
			<h4>票價：<?=$info['price']?>元</h4>
			<h4>最低消費：<?=$info['price_limit']?>元</h4>
			<h4><?=$info['company_address']?></h4>
			<a href="<?=$info['company_web']?>">前往店家網址</a><br>
			<div class="fb-like" style="margin-bottom:10px;" data-href="<?=base_url('show/'.$info['activity_name']);?>" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
			<div class="fb-share-button" data-href="<?=base_url('show/'.$info['activity_name']);?>" data-layout="box_count"></div>	
			<div class="love_block">
				<button type="button" data-id="<?=$info['id'];?>" class="btn love <?=(isset($info['favorite']) && $info['favorite']!='')?'is_love':'';?>"><i class="fa fa-heart"></i></button>	
				<p class="love">加入最愛</p>
			</div>	
		</div>
		<div class="col-sm-12 ">
			<div class="show_intro">
				<h3>展演介紹</h3><hr>
				<div><?=$info['intro']?></div>	
			</div>	
		</div>
		<div class="col-sm-12 ">
			<div class="show_intro">
				<hr><h3>演出者介紹</h3><hr>				
				<div>
				<table>
					<h2><?=$info['performer_name']?></h2>
					<?php if(!empty($history)){ foreach( $history as $key => $val ){?>
					<tr>
						<td><h4>經歷&nbsp</h4></td>
						<?php 						
								echo '<td>'.$val['content'].'</td>';							
							}
						;?>
					</tr>
					<?php }?>
					<?php if(!empty($license)){ foreach( $license as $key => $val ){?>
					<tr>
						<td><h4>證照&nbsp</h4></td>
						<?php 						
								echo '<td>'.$val['content'].'</td>';							
							}
						;?>
					</tr>
					<?php }?>
				</table>	
					<?php if($info['video_web']!=""){?>
					<a href="<?=$info['video_web']?>">前往觀賞我的表演片段</a><br>
					<?php }?>
					<?php if($info['pic_web']!=""){?>
					<a href="<?=$info['pic_web']?>">前往瀏覽我的作品集</a><br>
					<?php }?>
					<?php if($info['personal_web']!=""){?>
					<a href="<?=$info['personal_web']?>">前往我的個人網站</a><br>
					<?php }?>
				</div>	
				
			</div>	
		</div>
		
		<div class="col-sm-12">
			<hr><h3>你可能會喜歡</h3><hr>
			<?php foreach($other as $val){?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<div class="product-thumb transition">
						<div class="image">
							<a href="<?=base_url('show/'.$val['show_code'])?>">
								<div class="photo-wrapper66">
									<div class="photo" style="background-image: url(<?=base_url('public/photos/show/'.$val['id'].'/picture.png')?>)" ></div>
								</div>
							</a>
						</div>
						<div class="caption">
							<h3><a href="<?=base_url('show/'.$val['activity_name'])?>"><?=$val['activity_name']?></a></h3>							
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		
	</div>
</div>	
</div>
<script>
$(document).on("click", '.preview .photo', function() {
	console.log($(this).css('background-image'));
	$('.view .photo').css('background-image',$(this).css('background-image'));
});

$.ajax({ 
	url: "http://maps.googleapis.com/maps/api/geocode/json?address=<?=$info['company_address']?>&sensor=false", 
	type: "GET", 
	success: function(response){
		var mylat=response.results[0]['geometry'].location.lat;
		var mylng=response.results[0]['geometry'].location.lng;
		var latlng = new google.maps.LatLng(mylat,mylng);                            
		var myOptions = {
			//scrollwheel: false,
			zoom: 15,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};      
		var map = new google.maps.Map(document.getElementById("mymap"),myOptions);
		var marker=new google.maps.Marker({
			position:latlng/*,
			icon:'public/img/contact_icon.png'*/
		});
		marker.setMap(map);					
	}
});
</script>