<div id="main">
<div id="show_container" class="container">
	<!-- 固定欄位寬度為300px -->
	<div class="col-fixed hidden-xs left_filter">
		<?php if(isset($logged_in) && $logged_in['type']==2 ){?>
		<!--<a class="btn match" href="<?=base_url('myaccount/match');?>"><span>媒合提報</span><i class="fa fa-home"></i></a>-->
		<?php } ?>
		
		<div class="sidebar-nav">
			<div class="base">
				<input type="hidden" id="action" value="filter"/>
				<ul class="nav nav-list">
					<li class="nav-header">搜尋</li>
					<li>
						<input type="search" id="search" class="input-small form-control" placeholder="請輸入展演名稱">						
					</li>
					<li class="divider"></li>
				</ul>
				<ul class="nav nav-list">
					<li class="nav-header">排序</li>
					<li>
						<select class="selectpicker" id="sort" data-width="100%" data-size="8" >
							<option value="1">日期由近到遠</option>
							<option value="2">日期由遠到近</option>
							<option value="3">地區由北到南</option>
							<option value="4">地區由南到北</option>
							<option value="5">票價由低到高</option>
							<option value="6">票價由高到低</option>
						</select>											
					</li>
					<li class="divider"></li>
				</ul>
				<ul class="nav nav-list">
					<li class="nav-header">篩選 <!--<button type="button" class="btn filters">開始篩選</button></li>-->
				</ul>
				<ul class="nav nav-list filter blue" data-filter="stage">
					<li class="nav-header title"><i class="fa fa-plus-circle noselect" ></i><i class="fa fa-minus-circle noselect"></i>舞台類型</li>
					<div class="myblock">					
						<li class="item_all active" data-type="0"><a><i class="fa fa-th"></i>所有類型</a></li>
						<li class="item" data-type="1"><a><i class="fa fa-music"></i>音樂</a></li>
						<li class="item" data-type="2"><a><i class="fa fa-picture-o"></i>展覽</a></li>
						<li class="item" data-type="3"><a><i class="fa fa-users"></i>講座</a></li>
						<li class="item" data-type="4"><a><i class="fa fa-paper-plane-o"></i>教學</a></li>
						<li class="item" data-type="5"><a><i class="fa fa-magic"></i>魔術</a></li>
						<li class="item" data-type="6"><a><i class="fa fa-ellipsis-h"></i>其他</a></li>
						<li class="divider"></li>	
					</div>					
				</ul>	
				<ul class="nav nav-list filter" data-filter="area">	
					<li class="nav-header title"><i class="fa fa-plus-circle noselect"></i><i class="fa fa-minus-circle noselect"></i>地區</li>					
					<div class="myblock">
						<li class="nav-header">					
							<div class="prettyradio clearfix">
								<div class="radios">
									<input type="radio" id="area" class="prettyCheckable" value="area" name="area_filter" data-label="區域" data-customclass="margin-right" style="display: none;" checked="checked">
								</div>
								<div class="radios">
									<input type="radio" id="county" class="prettyCheckable" value="county" name="area_filter" data-label="縣市" data-customclass="margin-right" style="display: none;">
								</div>
							</div>		
						</li>					
						<li class="item_all aa active" data-area="0"><a><i class="fa fa-map-marker"></i>所有區域</a></li>
						<li class="item aa" data-area="1"><a><i class="fa fa-map-marker"></i>北部</a></li>
						<li class="item aa" data-area="2"><a><i class="fa fa-map-marker"></i>中部</a></li>
						<li class="item aa" data-area="3"><a><i class="fa fa-map-marker"></i>南部</a></li>
						<li class="item aa" data-area="4"><a><i class="fa fa-map-marker"></i>東部</a></li>
						<li class="item aa" data-area="5"><a><i class="fa fa-map-marker"></i>離島</a></li>
						
						<li class="item_all bb" data-county="0" ><a>所有縣市</a></li>
						<?php foreach($county as $val){?>
						<li class="item bb" data-county="<?=$val['id']?>"><a><?=$val['county_name']?></a></li>
						<?php }?>	
						<li class="divider"></li>
					</div>
				</ul>
				<ul class="nav nav-list filter blue" data-filter="date">
					<li class="nav-header title"><i class="fa fa-plus-circle noselect" ></i><i class="fa fa-minus-circle noselect"></i>年份、月份</li>
					<div class="myblock">
						<div class="input-daterange input-group" id="datepicker">
							<input type="text" class="input-sm form-control" name="start" value="<?=date('Y').'-01'?>" />
							<span class="input-group-addon">to</span>
							<input type="text" class="input-sm form-control" name="end" value="<?=date('Y').'-12'?>" />
						</div>
						<li class="divider"></li>	
					</div>					
				</ul>
				<ul class="nav nav-list filter blue" data-filter="date">
					<li class="nav-header title"><i class="fa fa-plus-circle noselect" ></i><i class="fa fa-minus-circle noselect"></i>票價</li>
					<div class="myblock">
						<input type="hidden" id="start_price">
						<input type="hidden" id="end_price">
						<p><input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;"></p>
						<div id="slider-range"></div>
					</div>					
				</ul>	
			</div>
		</div>
	</div>	
	
	<div class="rows row clearfix" style="margin-top:-10px;">
		<div class="col-lg-9 col-md-12 col-sm-12" id="shows">
			<div class="progress" style="width: calc(100% - 20px);margin: 20px 15px;">
				<div id="more" class="progress-bar progress-bar-striped progress-bar-info active load_more" data-page="0">載入更多展演</div>
			</div>				
		</div>
			
		<div class="col-lg-3 col-md-12 col-sm-12">
			<h3 class="text-center" style="margin-top:0;">你可能會喜歡</h3>
			<div class="row clearfix">
				<?php foreach($other as $val){?>
				<a href="<?=base_url('show/'.$val['show_code'])?>">
				<div class="other_item col-lg-12 col-md-3">
					<div class="photo-wrapper66">
						<div class="photo" style="background-image: url(<?=base_url('public/photos/show/'.$val['id'].'/picture.png')?>)"></div>
					</div>
					<div class="caption">
						<h4><a href="<?=$val['activity_name'];?>"><?=$val['activity_name'];?></a></h4>
					</div>
				</div>
				</a>
				<?php }?>
			</div>
		</div>
	</div>
	
	
	
</div>	
</div>





<script>

$(document).on("click", '.title i', function() {
	$(this).parent().find('i').show();
	$(this).hide();
	$(this).closest('ul').find('.myblock').toggle(200);
});	


function get_active(item){
	var arr = [];	
	$('[data-'+item+']').each(function() {
		if($(this).hasClass('active')) {
			arr.push($(this).data(item));
		}
	});
	return arr;
	//return arr.join();
}


function list(){
	$('#more').hide();
	var stage_type = get_active('type');
	var area = get_active('area');
	var county = get_active('county');
	//console.log(stage_type);
	//console.log(area);
	$.ajax({
		url: base_url+'shows/filter',
		type:"post",
		data: {
			action		:	$('#action').val(),
			stage_type	:	stage_type,
			area		:	area,
			sort		:	$('#sort').val(),
			county		:	county,
			start_date	:	$('[name=start]').val(),
			end_date	:	$('[name=end]').val(),
			start_price	:	$('#start_price').val(),
			end_price	:	$('#end_price').val(),
			page		:	$('#more').data('page'),
			search		:	$.trim($('#search').val())
		},
		dataType: "json",
		success: function(result){	
			//console.log(result);
			for(var key in result){ 
				$('.progress').before(
					'<div class="row clearfix show_item">'+
						'<div class="col-sm-12 clearfix one_show">'+			
							'<div class="image">'+	
								'<a href="'+base_url+'show/'+result[key].show_code+'">'+
									'<div class="photo-wrapper66">'+	
										'<div class="photo" style="background-image: url('+base_url+'/public/photos/show/'+result[key].id+'/picture.png)"></div>'+	
									'</div>'+	
								'</a>'+	
							'</div>'+	
							'<div class="info clearfix">'+	
								'<ul>'+	
									'<li class="title">'+
										'<h4 class="date">'+result[key].dates+'</h4>'+
										'<h4 class="week"> '+result[key].week+'</h4>'+										
									'</li>'+	
									'<li><a class="name" href="'+base_url+'show/'+result[key].show_code+'">'+result[key].activity_name+'</a></li>'+
									'<li><span>演出者：'+result[key].performer_name+'</span></li>'+	
									'<li><span>'+result[key].type+'</span> <span>'+result[key].price+'元</span></li>'+	
								'</ul>'+
								'<div class="icon">'+
									'<span><i class="fa fa-map-marker"></i> '+result[key].county_name+'</span>'+							
									<?php if(isset($logged_in)){?>
									'<span class="love-group pointer '+((result[key].favorite)?'love':'')+'" data-id="'+result[key].id+'" ><i class="fa fa-heart"></i> 加入最愛</span>'+	
									<?php }else{?>
									'<span class="pointer" href="#notloginModal" data-toggle="modal" title="加入最愛"><i class="fa fa-heart"></i> 加入最愛</span>'+
									<?php }?>
								'</div>'+		
							'</div>	'+						
						'</div>'+	
					'</div>'
				);
			}	
			//$('[data-toggle="tooltip"]').tooltip();
			$('#more').addClass('load_more').show();
		}			
	});
};

$(document).ready(function(){
	$('input.prettyCheckable').prettyCheckable({
		labelPosition: 'right'
	});
	$( "#slider-range" ).slider({
		range: true,
		min: 0,
		max: 10000,
		values: [ 0, 10000 ],
		slide: function( event, ui ) {
			$( "#start_price" ).val(ui.values[ 0 ]);
			$( "#end_price" ).val(ui.values[ 1 ]);
			$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );	
	$('#datepicker').datepicker({
		format: 'YYYY-MM',
		orientation: "top left",
		format: "yyyy-mm",
		startView: 2,
		viewMode: "months",
		minViewMode: "months",
		autoclose:true,
		language: "zh-TW"
	}).on('changeDate', function(e){
        $('#action').val('filter');
		$('.show_item').remove();
		$('#more').data('page',0);
		setTimeout(function(e){
			list();			
		}, 100);		
    });		
	list();
});

$(document).on("click", '.item_all', function() {
	$(this).closest('.filter').find('.active').removeClass('active');
	$(this).addClass('active');
});	
$(document).on("click", '.item', function() {
	$(this).closest('.filter').find('.item_all').removeClass('active');
	if($(this).hasClass('active'))
		$(this).removeClass('active');
	else
		$(this).addClass('active');
});	
$(document).on("click", '.radios', function() {
	$('.aa,.bb').hide();	
	if($(this).find('input').val()=='area'){
		$('.aa').show();
	}		
	else{
		$('.bb').show();
	}		
});	
$(document).on("click", '.bb', function() {
	$('.aa').removeClass('active');
});
$(document).on("click", '.aa', function() {
	$('.bb').removeClass('active');
});
/*
$(document).on("click", '.filters', function() {
	$('#action').val('filter');
	$('.show_item').remove();
	$('#more').data('page',0);
	list();
});
*/
$(document).on("click", '.item_all, .item', function() {
	$('#action').val('filter');
	$('.show_item').remove();
	$('#more').data('page',0);
	list();
});

$(document).on("change", '#sort', function() {
	$('#action').val('filter');
	$('.show_item').remove();
	$('#more').data('page',0);
	list();
});
/*
var flags=0;
$(document).on("change", '#datepicker input', function() {
	if(flags==0){
		flags=1;
		$('#action').val('filter');
		$('#shows').empty();
		$('#more').data('page',0);		
		list();				
	}
	flags=0;
	
});*/


$("#slider-range").slider();

var startPos = '$("#slider-range").slider("value");',endPos = '';

$("#slider-range").on("slidestop", function(event, ui) {
	endPos = ui.value;
	if (startPos != endPos) {
		$('#action').val('filter');
		$('.show_item').remove();
		$('#more').data('page',0);
		list();	
	}
	startPos = endpos;
});




$(document).on("click", '#more', function() {	
	var page = parseInt($('#more').data('page'));
	$('#more').data('page',page+1);
	$('#more').removeClass('load_more');
	$(this).data('type',parseInt($(this).data('type'))+1);
	list();
});

//搜尋框enter
$("#search").keypress(function(event){       
	if (event.keyCode == 13){ 
		$('#action').val('search');	
		$('.show_item').remove();
		list();
	}
});

<?php if(isset($logged_in)){?>
$(document).on("click", '.love-group', function() {
	if($(this).hasClass('love')){
		$(this).removeClass('love');
		//$(this).attr('data-original-title','加入我的最愛');	
		$.ajax({
			url: base_url+'shows/notlove',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});
	}else{		
		$(this).addClass('love');
		//$(this).attr('data-original-title','移除最愛');
		$.ajax({
			url: base_url+'shows/love',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});		
	}
});	
<?php } ?>

</script>