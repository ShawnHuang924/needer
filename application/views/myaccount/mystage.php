
	
	<div class="rows row clearfix">
		<div class="col-sm-12">
			<form class="form-horizontal tasi-form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
			<h2 style="text-align:center">我的舞台</h2>
			<div style="width:100%;float:left;">
				<div class="row">
					<div class="col-sm-12">
						<div id="photos_upload">
							<span class="btn btn-success fileinput-button" style="margin: 6px;">
								<i class="glyphicon glyphicon-plus"></i>
								<span>新增照片</span>
								<input id="fileupload" type="file" name="files[]" multiple>
							</span>						
							<div class="files" role="presentation"></div>					
						</div>
					</div>	
				</div>	
				<div id="detail">
					<div class="row details">
						<?php foreach($stages as $val){?>
						<div class="col-sm-3" style="min-height: 200px;">
							<h3><?=$val['stage']?><span><?=$val['detail_type']?></span></h3>						
							<?php if($val['stage_type']!=2){?>
							<h4>活動日期：每週<?=$val['activity_date']?></h4>
							<h4>活動時段：<?=$val['start_time'].'~'.$val['end_time']?></h4>
							<h4>活動時間：<?=$val['length']?></h4>
							<?php }else{?>
							<h4>展覽日期：<?=$val['ex_date']?></h4>
							<h4>展期長度：<?=$val['week_length']?></h4>				
							<?php }?>	
							<?php if( $val['remark'].$val['other_remark']!="" ){?>					
							<h4>備註：<?=$val['remark'].$val['other_remark']?></h4>
							<?php }else{?>	
							<h4><br></h4>
							<?php }?>
						</div>
						<?php } ?>	
					</div>	
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;">希望被表演者之聯繫方式 *</label>
					<div class="info_input" >
						<select name="contact" class="selectpicker" data-width="250">	
							<?php foreach($contact as $value){ ?>
							<option value="<?=$value['key']?>" <?=($info['contact']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;">希望被聯繫時段 *</label>
					<div class="info_input" >
						<div class="input-group " >
							<input type="text" name="contact_start_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_start_time']));?>">
							<span class="input-group-addon">to</span>
							<input type="text" name="contact_end_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_end_time']));?>">
						</div>					
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;">可供應之項目</label>
					<div class="info_input">
						<select name="supply_item[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
							<?php foreach($supply_item as $key => $value){ ?>
							<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_items))?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>	
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;"></label>
					<div class="info_input">
						<input type="text" name="other_item" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_item']?>" />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;">可提供之設備</label>
					<div class="info_input">
						<select name="supply_device[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
							<?php foreach($supply_device as $key => $value){ ?>
							<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_devices))?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>	
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label" style="width: 200px;"></label>
					<div class="info_input">
						<input type="text" name="other_device" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_device']?>" />
					</div>						
				</div>
		
				
				<div class="form-group">
					<label class="info_label" style="width: 200px;"></label>
					<button type="button" class="btn btn-primary save_info" >儲存</button>				
				</div>
		
			</div>
			</form>		
		</div> 	
	</div>	
<!-- 上傳前的template -->
<script id="template-upload2" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="col-lg-15 col-md-4 col-sm-4 template-upload fade" style="padding:0 5px 0 5px;margin-bottom:10px;">			
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>

<!-- 上傳完後的template -->
<script id="template-download2" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}    
    <div class="col-lg-15 col-md-4 col-sm-4 template-download fade" data-key="" style="padding:0 5px 0 5px;margin-bottom:10px;">	
		<div class="photo-wrapper115" style="">
			<div class="photo" style="background-image: url({%=file.url%});"></div>			
		</div>
		<span class="photos_cover" data-name="{%=file.name%}">設為封面</span>	 
		<span class="delete photos_delete" style="top:1px;right: 6px;" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			<i class="fa fa-times"></i>	
			<input type="hidden" name="filename[]" value="{%=file.name%}">	
		</span>	 
    </div>
{% } %}
</script>



<script>


$('.clockpicker').clockpicker({
	placement: 'bottom',
	align: 'left',
	autoclose: true
});

$('.selectpicker').on('change', function() {
	$('.selectpicker').selectpicker('refresh');
});

$(document).on("click", ".save_info", function() {
	$.ajax({
		url: base_url+'myaccount/save_mystage_info',
		type:"post",
		data: $(this).closest('form').serialize(),
		dataType: "json",
		success: function(result){
			alert(result);
		}			
	});
});
$( document ).ready(function() {
   create_multiple_upload('#photos_upload');
});
$(document).on('click','.photos_cover',function(){	
	var me=$(this);
	$.ajax({
		url: base_url+'upload/set_cover',
		type:"post",
		data: {
			'id':$('#id').val(),
			'filename':$(this).data('name')				
		},
		success:function(){
			$('.photos_cover').removeClass('is_cover');
			me.addClass('is_cover');
			alert('設置封面!');				
		}
	});
});

</script>