	
	<div class="rows row clearfix">
		<div class="col-sm-12" id="shows">			
			<h2 style="text-align:center">最愛展演</h2>			
			<?php foreach($favorite_show as $key=>$val){?>
			<div class="row clearfix">
				<div class="col-sm-12 clearfix one_show">	
					<div class="image">
						<a href="<?=base_url('show/'.$val['show_code'])?>">
							<div class="photo-wrapper66">
								<div class="photo" style="background-image: url(<?=base_url('public/photos/show/'.$val['id'].'//picture.png')?>)"></div>
							</div>
						</a>
					</div>
					<div class="info">
						<ul>
							<li class="title">
								<h4 class="date"><?=$val['dates']?></h4>
								<h4 class="week"> <?=$val['week']?></h4>
							</li>
							<li><a class="name" href="<?=base_url('show/'.$val['activity_name'])?>"><?=$val['activity_name']?></a></li>
							<li><span>演出者：<?=$val['name']?></span></li>
							<li>
								<span><?=$val['type']?></span> 
								<span>NTD <?=$val['price']?></span>
								<span><i class="fa fa-map-marker"></i> <?=$val['county_name']?></span>
							</li>
						</ul>
					</div>	
				</div>
			</div>
			<?php } ?>
		</div>		
	</div>	
