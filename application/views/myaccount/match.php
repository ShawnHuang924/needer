	
	<div class="rows row clearfix">
		<div class="col-sm-12">
			<form class="form-horizontal tasi-form upload_form" action="<?=base_url('myaccount/save_match')?>" enctype="multipart/form-data" method="post"  onsubmit="return postForm()" data-toggle="validator" role="form">
			<h2 style="text-align:center">媒合提報</h2>
			<div style="width:100%;float:left;">
				<div class="form-group">
					<label class="info_label">活動類型</label>
					<div class="info_input">							
						<select name="show_type" class="selectpicker" data-width="250" required>	
							<?php foreach($stage_type as $val){ ?>
							<option value="<?=$val['id']?>" ><?=$val['type']?></option>
							<?php } ?>
						</select>
						<span class="help-block with-errors"></span>
					</div>					
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" name="activity_name" class="form-control"  placeholder="活動名稱" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">
					<div class="col-sm-12">						
						<textarea  id="summernote" type="text" name="intro" class="form-control" placeholder="活動簡介" required ></textarea>
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">活動日期</label>
					<div class="info_input" >
						<div class="input-daterange input-group col-sm-6">
							<input type="text" class="form-control" name="start_date" required />
							<span class="input-group-addon">~</span>
							<input type="text" class="form-control" name="end_date" required />
						</div>	
						<div class="help-block with-errors"></div>
					</div>					
				</div>
				<div class="form-group">
					<label class="info_label">活動時間</label>
					<div class="info_input" >
						<div class="input-group " >
							<input type="text" name="start_time" class="form-control clockpicker" required>
							<span class="input-group-addon">~</span>
							<input type="text" name="end_time" class="form-control clockpicker" required>
						</div>		
						<div class="help-block with-errors"></div>					
					</div>
				</div>
				<div class="form-group">
					<label class="info_label">活動票價</label>
					<div class="info_input">
						<input type="text" name="price" class="form-control" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">最低消費限制</label>
					<div class="info_input">
						<input type="text" name="price_limit" class="form-control" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">注意事項</label>
					<div class="info_input">							
						<select name="notices[]" class="selectpicker" multiple data-width="250" data-size="8" >
							<?php foreach($notice_type as $val){ ?>
							<option value="<?=$val['key']?>" ><?=$val['value']?></option>
							<?php } ?>							
						</select>
					</div>					
				</div>
				<div class="form-group">					
					<div class="col-sm-12">
						<input type="text" name="company_name" class="form-control" value="<?=$info['company_name']?>"  placeholder="店家名稱" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">					
					<div class="col-sm-12">
						<input type="text" name="company_web" class="form-control" value="<?=$info['company_web']?>"  placeholder="店家網址" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">					
					<div class="col-sm-12">
						<input type="text" name="company_address" class="form-control" value="<?=$info['company_address']?>" placeholder="店家地址" required />
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group col-sm-12">					
					<div class="form-group col-sm-6">
						<input type="text" name="performer_name" class="form-control" placeholder="請輸入表演者姓名" required />
						<span class="help-block with-errors"></span>
					</div>
					<div class="form-group col-sm-6">
						<input type="text" name="performer_email" class="form-control" placeholder="請輸入表演者於NEEDER註冊之email" required  data-remote="is_access?performer_email=<value>" data-error="帳號不存在" />
						<span class="help-block with-errors"></span>
					</div>					
				</div>
				<div class="form-group col-sm-12">
					<div class="edit_pic" style="min-height:300px;width: 100%;">  
						<div class="files" role="presentation"></div>						
						<div class="new_pic">							
							請將照片拖曳至此 或 點選新增照片
							<span class="btn fileinput-button fileinput_all" >
								<input type="file" name="files[]" accept="image/*" style="cursor:pointer;">
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-6">
					<button type="submit" class="btn btn-primary save_match" style="margin-top:20px;" >送出提報</button>	
				</div>	
			</div>
		
			</div>
		</form>
		
	</div>
	
</div>	
</div>




<script>
$('.input-daterange').datepicker({
    format: "yyyy-mm-dd",
	autoclose: true,
	language: "zh-TW",
});
$('.clockpicker').clockpicker({
	placement: 'bottom',
	align: 'left',
	autoclose: true
});	
$('.selectpicker').on('change', function() {
	$('.selectpicker').selectpicker('refresh');
});

$('#summernote').summernote();
var postForm = function() {
	var content = $('textarea[name="content"]').html($('#summernote').code());
}
 $('form').submit(function() {
	if($('.edit_pic .target').length==0){
		alert('請先上傳圖片');
		return false;
	}
});
/*
$(document).on("click", ".save_match", function() {
		$.ajax({
			url: base_url+'myaccount/save_match',
			type:"post",
			data: $(this).closest('form').serialize(),
			dataType: "json",
			success: function(result){
				$.growlUI(result);
			}			
		});
});*/
</script>