
	
	<div class="rows row clearfix">
		<div class="col-sm-12">
			<form class="form-horizontal tasi-form upload_form" action="" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
			<h2 style="text-align:center">基本資料</h2>
			<div style="width:100%;float:left;">
				<div class="form-group">
					<label class="info_label">email</label>
					<div class="info_input">
						<input type="email" name="account2" class="form-control" value="<?=$info['account']?>" disabled placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">姓</label>
					<div class="info_input">
						<input type="text" name="last_name" class="form-control" value="<?=$info['last_name']?>" placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">名</label>
					<div class="info_input">
						<input type="text" name="name" class="form-control" value="<?=$info['name']?>" placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">性別</label>
					<div class="info_input">								
						<select name="sex" class="selectpicker" data-width="250">	
							<option value="1" <?=($info['sex']==1)?'selected':'';?>>男生</option>	
							<option value="0" <?=($info['sex']==0)?'selected':'';?>>女生</option>														
						</select>
					</div>					
				</div>
				<div class="form-group">
					<label class="info_label">聯絡電話</label>
					<div class="info_input">
						<input type="text" name="phone" class="form-control" value="<?=$info['phone']?>" placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">居住城市</label>
					<div class="info_input">
						<select name="city" class="selectpicker" data-width="250" data-size="8">	
							<?php foreach($county as $value){ ?>
							<option value="<?=$value['id']?>" <?=($info['county']==$value['id'])?'selected':'';?>><?=$value['county_name']?></option>
							<?php } ?>								
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">出生日期</label>
					<div class="info_input">
						<input type="text" name="birthday" id="birthday" class="form-control" value="<?=$info['birthday']?>" placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">感興趣主題</label>
					<div class="info_input">							
						<select name="theme[]" class="selectpicker" multiple data-width="250" data-size="8">									
							<?php foreach($theme as $val){ ?>
							<option value="<?=$val['key']?>" <?=(in_array($val['key'], $themes))?'selected':'';?>><?=$val['value']?></option>
							<?php } ?>
						</select>
					</div>					
				</div>
				
				<div class="form-group">
					<label class="info_label">經歷</label>	
					<div class="form-group" style="float:left;margin-left:0px;margin-bottom:0">
						<?php $x=0; foreach($history as $key => $val){ $x++; ?>
						<div class="one_item">
							<div class="info_input" style="float:left;display:block;">
								<div class="input-group">
									<select name="history_year[]" id="history" class="selectpicker " data-width="100">												
										<option value="0" ></option>
										<?php for( $i=1;$i<=10;$i++ ){ ?>
										<option value="<?=$i?>" <?=($val['year']==$i)?'selected':'';?>><?=$i.'年'?></option>				
										<?php } ?>								
									</select>
									<input type="text" name="history_content[]" value="<?=$val['content']?>" class="form-control " placeholder="填入敘述"/>
								</div>
							</div>
							<?php if($x==1){?>
							<i class="fa fa-plus-circle noselect pointer add" style="padding:8px;" ></i>
							<?php }else{?>
							<i class="fa fa-minus-circle noselect pointer delete" style="padding:8px;" ></i>
							<?php } ?>
						</div>	
						<?php } ?>
					</div>							
				</div>
				
				<div class="form-group">
					<label class="info_label">證照</label>	
					<div class="form-group" style="float:left;margin-left:0px;margin-bottom:0">
						<?php $x=0; foreach($license as $key => $val){ $x++; ?>
						<div class="one_item">
							<div class="info_input" style="float:left;display:block;">
								<div class="input-group" >
									<select name="license_year[]" class="selectpicker " data-width="100">												
										<option value="0" ></option>
										<?php for( $i=1;$i<=10;$i++ ){ ?>
										<option value="<?=$i?>" <?=($val['year']==$i)?'selected':'';?>><?=$i.'年'?></option>				
										<?php } ?>								
									</select>
									<input type="text" name="license_content[]" value="<?=$val['content']?>" class="form-control " placeholder="填入敘述"/>
								</div>
							</div>
							<?php if($x==1){?>
							<i class="fa fa-plus-circle noselect pointer add" style="padding:8px;" ></i>
							<?php }else{?>
							<i class="fa fa-minus-circle noselect pointer delete" style="padding:8px;" ></i>
							<?php } ?>
						</div>	
						<?php } ?>
					</div>							
				</div>
				
				

				<div class="form-group">
					<label class="info_label">影音網址</label>
					<div class="info_input" style="width:390px;">
						<input type="url" name="video_web" value="<?=$info['video_web']?>" class="form-control" placeholder="如表演片段、個人錄音或錄影片段" />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">相簿網址</label>
					<div class="info_input" style="width:390px;">
						<input type="url" name="pic_web" value="<?=$info['pic_web']?>" class="form-control" placeholder="如作品集、攝影集之照片" />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">個人網址</label>
					<div class="info_input" style="width:390px;">
						<input type="url" name="personal_web" value="<?=$info['personal_web']?>" class="form-control" placeholder="如個人網站、FACEBOOK" />
					</div>						
				</div>
			</div>
			
			<div style="width:100%;float:left;">
			<?php if($logged_in['type']==2){?>
				<div class="form-group">
					<label class="info_label">公司名稱</label>
					<div class="info_input">
						<input type="text" name="company_name" class="form-control" value="<?=$info['company_name']?>" placeholder="必填" required />
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">公司類型</label>
					<div class="info_input">
						<select name="company_type" class="selectpicker" data-width="250" data-size="8" required>	
							<?php foreach($company_type as $value){ ?>
							<option value="<?=$value['key']?>" <?=($info['company_type']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>								
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="info_label">公司地址</label>
						<div id="twzipcode">
						<div data-role="county"
							 data-name="county"
							 data-style="county-district selectpicker"
							 style="float:left;"></div>							
						<div data-role="district"
							 data-name="district"
							 data-style="county-district selectpicker"
							 style="float:left;"></div>
						<div data-role="zipcode"
							 data-name="company_district"
							 data-style="zipcode-style"
							 style="display:none;"></div>
					</div>
				</div>	
				<div class="form-group">
					<label class="info_label"></label>
					<div class="info_input" style="width:390px;">
						<input type="text" name="company_address" value="<?=$info['company_address']?>" class="form-control"  placeholder="必填" required />
					</div>							
				</div>
				<div class="form-group">
					<label class="info_label">公司網址</label>
					<div class="info_input" style="width:390px;">
						<input type="url" name="company_web" value="<?=$info['company_web']?>" class="form-control" />
					</div>						
				</div>
			<?php } ?>	
				
				<div class="form-group">
					<label class="info_label"></label>
					<button type="button" class="btn btn-primary save_info" >儲存</button>				
				</div>
		
			</div>
		</form>
		
	</div> 	
</div>	




<script>
$("#birthday").datepicker({
	format: "yyyy-mm-dd",
	startView: 2,
	autoclose: true,
	language: "zh-TW",
	todayBtn: "linked"
});
	

$('#twzipcode').twzipcode({	
	<?php if($info['company_district']!=0){?>
	'zipcodeSel': '<?=$info['company_district'];?>'
	<?php }?>
});

$('.clockpicker').clockpicker({
	placement: 'bottom',
	align: 'left',
	autoclose: true
});

$('.selectpicker').on('change', function() {
	$('.selectpicker').selectpicker('refresh');
});

$(document).on("click", ".save_info", function() {
	$.ajax({
		url: base_url+'myaccount/save_info',
		type:"post",
		data: $(this).closest('form').serialize(),
		dataType: "json",
		success: function(result){
			alert(result);
		}			
	});
});
	
$(document).on("click", ".delete", function() {
	$(this).closest('.one_item').remove();
});

$(document).on("click", ".add", function() {
	var clone=$(this).closest('.one_item').clone();
	clone.find('i.add').remove();
	clone.find('input').val('');
	clone.find('.bootstrap-select').remove();
	clone.find('select.selectpicker').selectpicker('val',0);
	clone.append('<i class="fa fa-minus-circle noselect pointer delete" style="padding:8px;" ></i>');
	$(this).closest('.form-group').append(clone);
});

</script>