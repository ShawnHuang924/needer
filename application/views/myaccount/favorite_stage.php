	
	<div class="rows row clearfix">
		<div class="col-sm-12">			
			<h2 style="text-align:center">最愛舞台</h2>			
			<?php foreach($favorite_stage as $key=>$val){?>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="product-thumb transition">
					<div class="image">
						<a href="<?=base_url('stage/'.$val['company_code'])?>">						
							<div class="photo-wrapper65">
								<div class="photo" style="background-image: url(<?=base_url('public/photos/member/'.$val['id'].'/'.$val['filename'])?>)" ></div>
							</div>
						</a>
					</div>
					<div class="caption">
						<h4><a href="<?=base_url('stage/'.$val['company_name'])?>"><?=$val['company_name']?></a></h4>
						<p class="price"><span class="price-tax" style="font-size:0.7em"><?=$val['stage_type']?></span></p>
						<p class="price"><span class="price-tax" style="font-size:0.7em"><?=$val['county_name']?></span></p>
					</div>					
				</div>
			</div>
			<?php } ?>
		</div>		
	</div>	
