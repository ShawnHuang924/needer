<div id="main">
<div class="container">
	<div id="detail" class="row">
		<div class="col-lg-6 col-md-7 clearfix">
			<!--預覽圖-->
			<div class="preview hidden-xs">
				<?php foreach($photos as $val){?>
				<div class="photo-wrapper100">
					<div class="photo" style="background-image: url(<?=base_url('public/photos/member/'.$info['id'].'/'.$val['filename'])?>)" ></div>
				</div>
				<?php } ?>
				<div class="fb-like" style="margin-bottom:10px;" data-href="<?=base_url('stage/'.$info['company_name']);?>" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
				<div class="fb-share-button" data-href="<?=base_url('stage/'.$info['company_name']);?>" data-layout="box_count"></div>					
				<button type="button" data-id="<?=$info['id'];?>" class="btn love <?=($info['favorite']!='')?'is_love':'';?>"><i class="fa fa-heart"></i></button>	
				<p class="love">加入最愛</p>
			</div>
			<!--大圖-->
			<div class="view">
				<div class="image">
					<div class="photo-wrapper100">
						<div class="photo" style="background-image: url(<?=base_url('public/photos/member/'.$info['id'].'/'.$info['filename'])?>)" ></div>
					</div>					
				</div>			
			</div>
			<!--預覽圖-->
			<div class="preview visible-xs">
				<?php foreach($photos as $val){?>
				<div class="photo-wrapper100">
					<div class="photo" style="background-image: url(<?=base_url('public/photos/member/'.$info['id'].'/'.$val['filename'])?>)" ></div>
				</div>
				<?php } ?>
			</div>	
			<div class="preview visible-xs">	
				<div class="fb-like" style="margin-bottom:10px;" data-href="<?=base_url('stage/'.$info['company_name']);?>" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
				<div class="fb-share-button" data-href="<?=base_url('stage/'.$info['company_name']);?>" data-layout="box_count"></div>	
				<div class="love_block">
					<button type="button" data-id="<?=$info['id'];?>" class="btn love <?=($info['favorite']!='')?'is_love':'';?>"><i class="fa fa-heart"></i></button>	
					<p class="love">加入最愛</p>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-5 clearfix">
			<h2 class="company_name"><?=$info['company_name']?></h2>
			<h4 class="stage_type"><?=$info['stage_type']?></h4>			
			<h4><img src="<?=base_url('public/img/g1.jpg')?>" /><?=$info['name']?></h4>
			<?php if( $info['contact']==0 ){?>
				<h4><img src="<?=base_url('public/img/g3.jpg')?>" /><?=$info['phone']?></h4>
			<?php }else if( $info['contact']==1 ){?>
				<h4><img src="<?=base_url('public/img/g2.jpg')?>" /><?=$info['account']?></h4>
			<?php }else {?>
				<h4><img src="<?=base_url('public/img/g2.jpg')?>" /><?=$info['account']?></h4>
				<h4><img src="<?=base_url('public/img/g3.jpg')?>" /><?=$info['phone']?></h4>
			<?php } ?>
			<a target="_blank" href="<?=$info['company_web']?>"><h4><img src="<?=base_url('public/img/g4.jpg')?>" />前往店家網址</h4></a>
			<h4><img src="<?=base_url('public/img/g5.jpg')?>" /><?=$info['company_address']?></h4>
			<div id="mymap" style="height:240px;"></div>
			<!--<button type="button" class="btn">投遞履歷</button>-->
			
		</div>
		<div class="col-sm-12 details">
			<h3 class="titles">舞台詳情</h3>
			<div class="row clearfix">
			<?php foreach($stages as $val){?>
			<div class="col-sm-3 one_item">
				<h3><?=$val['stage']?><span><?=str_replace(",",", ",$val['detail_type'])?></span></h3>
				<table>
				<?php if($val['stage_type']!=2){?>
				<tr>
					<td><h4>活動日期：</h4></td>
					<td><h4>每週<?=str_replace(",",", ",$val['activity_date'])?></h4></td>					
				</tr>
				<tr>
					<td><h4>活動時段：</h4></td>
					<td><h4><?=$val['start_time'].' - '.$val['end_time']?></h4></td>					
				</tr>
				<tr>
					<td><h4>活動時間：</h4></td>
					<td><h4><?=$val['length']?></h4></td>					
				</tr>				
				<?php }else{?>
				<tr>
					<td><h4>展覽日期：</h4></td>
					<td><h4><?=$val['ex_date']?></h4></td>					
				</tr>
				<tr>
					<td><h4>展期長度：</h4></td>
					<td><h4><?=$val['week_length']?></h4></td>					
				</tr>						
				<?php }?>	
				<?php if( $val['remark']!=""){?>	
				<tr>
					<td><h4>其他備註：</h4></td>
					<td><h4><?=$val['remark']?></h4></td>					
				</tr>
					<?php if( $val['other_remark']!=""){?>					
					<tr>
						<td><h4></h4></td>
						<td><h4><?=$val['other_remark']?></h4></td>					
					</tr>
					<?php }?>
				<?php }else if( $val['remark']=="" && $val['other_remark']!="" ){?>	
				<tr>
					<td><h4>其他備註：</h4></td>
					<td><h4><?=$val['other_remark']?></h4></td>					
				</tr>
				<?php }?>
				</table>
			</div>
			<?php } ?>	
			</div>
			<hr>	
		</div>
		<div class="col-sm-12">
			<h4>希望被聯繫時段：<?=date('H:i',strtotime($info['contact_start_time']));?>-<?=date('H:i',strtotime($info['contact_end_time']))?></h4>
			<?php if( $info['supply_item']!="" ){?>	
			<h4>可供應之項目：<?=$info['supply_item']?></h4>
			<?php }?>
			<?php if( $info['supply_device']!="" ){?>	
			<h4>可提供之設備：<?=$info['supply_device']?></h4>
			<?php }?>
			<hr>
		</div>
		<div class="col-sm-12">
			<h3>看過這個舞台的人，也查看了這些舞台：</h3>
			<div class="row clearfix">
				<?php foreach($other as $val){?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="product-thumb transition">
							<div class="image">
								<a href="<?=base_url('stage/'.$val['company_code'])?>">
									<div class="photo-wrapper65">
										<div class="photo" style="background-image: url(<?=base_url('public/photos/member/'.$val['id'].'/'.$val['filename'])?>)" ></div>
									</div>
								</a>
							</div>
							<div class="caption">
								<h3><a href="<?=base_url('stage/'.$val['company_name'])?>"><?=$val['company_name']?></a></h3>
								<p></p>
								<p class="price">
									<span class="price-tax"><?=$val['stage_type']?></span>
								</p>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-12">	
			<div id="disqus_thread"></div>
			<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES * * */
				var disqus_shortname = 'needertw';
				
				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function() {
					var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
					dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
					(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				})();
				  /* * * CONFIGURATION VARIABLES * * */
				var disqus_shortname = 'needertw';
				
				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function () {
					var s = document.createElement('script'); s.async = true;
					s.type = 'text/javascript';
					s.src = '//' + disqus_shortname + '.disqus.com/count.js';
					(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
				}());
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
		</div>
	</div>
</div>	
</div>
<script>
$(document).on("click", '.preview .photo', function() {
	console.log($(this).css('background-image'));
	$('.view .photo').css('background-image',$(this).css('background-image'));
});

$.ajax({ 
	url: "http://maps.googleapis.com/maps/api/geocode/json?address=<?=$info['company_address']?>&sensor=false", 
	type: "GET", 
	success: function(response){
		var mylat=response.results[0]['geometry'].location.lat;
		var mylng=response.results[0]['geometry'].location.lng;
		var latlng = new google.maps.LatLng(mylat,mylng);                            
		var myOptions = {
			//scrollwheel: false,
			zoom: 15,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};      
		var map = new google.maps.Map(document.getElementById("mymap"),myOptions);
		var marker=new google.maps.Marker({
			position:latlng/*,
			icon:'public/img/contact_icon.png'*/
		});
		marker.setMap(map);					
	}
});

<?php if(isset($logged_in)){?>
$(document).on("click", 'button.love', function() {
	if($(this).hasClass('is_love')){
		$(this).removeClass('is_love');
		//$(this).attr('data-original-title','加入我的最愛');	
		$.ajax({
			url: base_url+'stages/notlove',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});
	}else{		
		$(this).addClass('is_love');
		//$(this).attr('data-original-title','移除最愛');
		$.ajax({
			url: base_url+'stages/love',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});		
	}
});	
<?php } ?>
</script>