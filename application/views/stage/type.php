<div id="main">
<div id="stage_container" class="container-fluid">
	<!-- 固定欄位寬度為300px -->
	<div class="col-fixed hidden-xs">
		<div class="sidebar-nav">
			<div class="base">
				<input type="hidden" id="action" value="filter"/>
				<ul class="nav nav-list">
					<li class="nav-header">搜尋</li>
					<li>
						<input type="search" id="search" class="input-small form-control" placeholder="請輸入舞台名稱">						
					</li>
					<li class="divider"></li>
				</ul>
				<ul class="nav nav-list">
					<li class="nav-header">排序</li>
					<li>
						<select class="selectpicker" id="sort" data-width="100%" data-size="8" >							
							<option value="1">依舞台類型</option>
							<option value="2">依地區</option>
						</select>											
					</li>
					<li class="divider"></li>
				</ul>
				<ul class="nav nav-list">
					<li class="nav-header">篩選 <!--<button type="button" class="btn filters">開始篩選</button></li>-->
				</ul>
				<ul class="nav nav-list filter blue" data-filter="stage">
					<li class="nav-header title"><i class="fa fa-plus-circle noselect" ></i><i class="fa fa-minus-circle noselect"></i>舞台類型</li>
					<div class="myblock">					
						<li class="item_all active" data-type="0"><a><i class="fa fa-th"></i>所有類型</a></li>
						<li class="item" data-type="1"><a><i class="fa fa-music"></i>音樂</a></li>
						<li class="item" data-type="2"><a><i class="fa fa-picture-o"></i>展覽</a></li>
						<li class="item" data-type="3"><a><i class="fa fa-users"></i>講座</a></li>
						<li class="item" data-type="4"><a><i class="fa fa-paper-plane-o"></i>教學</a></li>
						<li class="item" data-type="5"><a><i class="fa fa-magic"></i>魔術</a></li>
						<li class="item" data-type="6"><a><i class="fa fa-ellipsis-h"></i>其他</a></li>
						<li class="divider"></li>	
					</div>					
				</ul>	
				<ul class="nav nav-list filter" data-filter="area">	
					<li class="nav-header title"><i class="fa fa-plus-circle noselect"></i><i class="fa fa-minus-circle noselect"></i>地區</li>					
					<div class="myblock">
						<li class="nav-header">					
							<div class="prettyradio clearfix">
								<div class="radios">
									<input type="radio" id="area" class="prettyCheckable" value="area" name="area_filter" data-label="區域" data-customclass="margin-right" style="display: none;" checked="checked">
								</div>
								<div class="radios">
									<input type="radio" id="county" class="prettyCheckable" value="county" name="area_filter" data-label="縣市" data-customclass="margin-right" style="display: none;">
								</div>
							</div>		
						</li>					
						<li class="item_all aa active" data-area="0"><a><i class="fa fa-map-marker"></i>所有區域</a></li>
						<li class="item aa" data-area="1"><a><i class="fa fa-map-marker"></i>北部</a></li>
						<li class="item aa" data-area="2"><a><i class="fa fa-map-marker"></i>中部</a></li>
						<li class="item aa" data-area="3"><a><i class="fa fa-map-marker"></i>南部</a></li>
						<li class="item aa" data-area="4"><a><i class="fa fa-map-marker"></i>東部</a></li>
						<li class="item aa" data-area="5"><a><i class="fa fa-map-marker"></i>離島</a></li>
						
						<li class="item_all bb" data-county="0" ><a>所有縣市</a></li>
						<?php foreach($county as $val){?>
						<li class="item bb" data-county="<?=$val['id']?>"><a><?=$val['county_name']?></a></li>
						<?php }?>	
						<li class="divider"></li>
					</div>
				</ul>
				<!--<button type="button" class="btn filters">篩選</button>-->
			</div>
		</div>
	</div>	
	<div class="rows row clearfix" id="stages"></div>
	<div class="rows row clearfix text-center">
		<div class="progress">
			<div id="more" class="progress-bar progress-bar-striped progress-bar-info active load_more" data-page="0">載入更多舞台</div>
		</div>
	</div>
</div>	
</div>




<!-- loginModal -->
<div class="modal" id="typeModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:80%;">
		<div class="modal-content">
			<div class="modal-body clearfix" >	
				<div class="col-sm-9">
					<div class="row">
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/music');?>">								
								<div class="text-center">
									<img src="<?=base_url('public/img/MUSIC256.png')?>" >
									<div class="caption">
										<hr>
										<h2>音樂</h2>
										<h4>演奏、唱歌</h4>
									</div>
								</div>								
							</a>
						</div>
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/exhibition');?>">											
								<div class="text-center">
									<img src="<?=base_url('public/img/Exhibition256.png')?>" >
									<div class="caption">
										<hr>
										<h2>展覽</h2>
										<h4>畫作、攝影、設計</h4>	
									</div>
								</div>								
							</a>
						</div>
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/lecture');?>">								
								<div class="text-center">
									<img src="<?=base_url('public/img/Conference256.png')?>" >
									<div class="caption">
										<hr>
										<h2>講座</h2>
										<h4>藝文、旅遊、健康、科技、經濟</h4>	
									</div>
								</div>								
							</a>	
						</div>
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/teaching');?>">								
								<div class="text-center">
									<img src="<?=base_url('public/img/Teaching256.png')?>" >
									<div class="caption">
										<hr>
										<h2>教學</h2>
										<h4>繪畫、樂器、手工藝、烘培、品飲</h4>
									</div>
								</div>								
							</a>
						</div>
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/magic');?>">								
								<div class="text-center">
									<img src="<?=base_url('public/img/Magic256.png')?>" >
									<div class="caption">
										<hr>
										<h2>魔術</h2>
										<h4>舞台魔術、近距離魔術</h4>
									</div>
								</div>								
							</a>
						</div>
						<div class="col-sm-4 block">
							<a href="<?=base_url('stage/type/other');?>">								
								<div class="text-center">
									<img src="<?=base_url('public/img/OTHERS256.png')?>" >
									<div class="caption">
										<hr>
										<h2>其他</h2>
										<h4>舞蹈、說故事、表演藝術、行動藝術</h4>
									</div>
								</div>								
							</a>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>






<script>
$(document).on("click", '.title i', function() {
	$(this).parent().find('i').show();
	$(this).hide();
	$(this).closest('ul').find('.myblock').toggle(200);
});	
//
function list(){
	$('#more').hide();
	var stage_type = get_active('type');
	var area = get_active('area');
	var county = get_active('county');
	$.ajax({
		url: base_url+'stages/filter',
		type:"post",
		data: {
			action		:	$('#action').val(),
			stage_type	:	stage_type,
			area		:	area,
			county		:	county,
			sort		:	$('#sort').val(),
			page		:	$('#more').data('page'),
			search		:	$.trim($('#search').val())
		},
		dataType: "json",
		success: function(result){			
			for(var key in result){ 
				$('#stages').append(
					'<div class="col-lg-15 col-md-4 col-sm-6 col-xs-12">'+
						'<div class="product-thumb transition">'+
							'<div class="image">'+
								<?php if(isset($logged_in)){?>
								'<a href="'+base_url+'stage/'+result[key].company_code+'">'+
								<?php }else{?>
								'<a href="#notloginModal" data-toggle="modal">'+
								<?php }?>
									'<div class="photo-wrapper65">'+
										'<div class="photo" style="background-image: url('+base_url+'/public/photos/member/'+result[key].id+'/'+result[key].filename+')" ></div>'+
									'</div>'+
								'</a>'+
							'</div>'+
							'<div class="caption">'+
								'<h3>'+
									<?php if(isset($logged_in)){?>
									'<a href="'+base_url+'stage/'+result[key].company_name.replace(/\ /g, "_")+'">'+result[key].company_name+'</a>'+
									<?php }else{?>
									'<a href="#notloginModal" data-toggle="modal">'+result[key].company_name+'</a>'+
									<?php }?>									
								'</h3>'+
								'<p></p>'+
								'<p class="price">'+
									'<span class="price-tax">'+result[key].stage_type+'</span>'+
								'</p>'+
							'</div>'+
							'<div class="button-group">'+
								//'<button type="button" ></button>'+		
								'<button type="button" class="type-group">'+result[key].county_name+'</button>'+
								<?php if(isset($logged_in)){?>
								'<button type="button" class="love-group '+((result[key].favorite)?'love':'')+'" data-id="'+result[key].id+'" data-toggle="tooltip" data-original-title="'+((result[key].favorite)?'移除最愛':'加入我的最愛')+'" ><i class="fa fa-heart"></i></button>'+
								<?php }else{?>
								'<button type="button" class="love-group" href="#notloginModal" data-toggle="modal" title="加入我的最愛" data-original-title="Add to Wish List"><i class="fa fa-heart"></i></button>'+
								<?php }?>
								
							'</div>'+
						'</div>'+
					'</div>'
				);
			}		
			$('[data-toggle="tooltip"]').tooltip();	
			$('#more').addClass('load_more').show();
		}			
	});
};

$(document).ready(function(){
	$('input.prettyCheckable').prettyCheckable({
		labelPosition: 'right'
	});
	//$('#typeModal').modal('show');
	list();
});
$(document).on("click", '.item_all', function() {
	$(this).closest('.filter').find('.active').removeClass('active');
	$(this).addClass('active');
});	
$(document).on("click", '.item', function() {
	$(this).closest('.filter').find('.item_all').removeClass('active');
	if($(this).hasClass('active'))
		$(this).removeClass('active');
	else
		$(this).addClass('active');
});	
$(document).on("click", '.radios', function() {
	$('.aa,.bb').hide();	
	if($(this).find('input').val()=='area'){
		$('.aa').show();
	}		
	else{
		$('.bb').show();
	}		
});	
$(document).on("click", '.bb', function() {
	$('.aa').removeClass('active');
});
$(document).on("click", '.aa', function() {
	$('.bb').removeClass('active');
});

function get_active(item){
	var arr = [];	
	$('[data-'+item+']').each(function() {
		if($(this).hasClass('active')) {
			arr.push($(this).data(item));
		}
	});
	return arr;
	//return arr.join();
}
/*
$(document).on("click", '.filters', function() {
	$('#action').val('filter');
	$('#stages').empty();
	$('#more').data('page',0);
	list();
});	*/
$(document).on("click", '.item_all, .item', function() {
	$('#action').val('filter');
	$('#stages').empty();
	$('#more').data('page',0);
	list();
});
$(document).on("change", '#sort', function() {
	$('#action').val('filter');
	$('#stages').empty();
	$('#more').data('page',0);
	list();
});
$(document).on("click", '#more', function() {	
	var page = parseInt($('#more').data('page'));
	$('#more').data('page',page+1);
	$('#more').removeClass('load_more');
	$(this).data('type',parseInt($(this).data('type'))+1);
	list();
});

//搜尋框enter
$("#search").keypress(function(event){       
	if (event.keyCode == 13){ 
		$('#action').val('search');	
		$('#stages').empty();
		list();
	}
});

<?php if(isset($logged_in)){?>
$(document).on("click", '.love-group', function() {
	if($(this).hasClass('love')){
		$(this).removeClass('love');
		$(this).attr('data-original-title','加入我的最愛');	
		$.ajax({
			url: base_url+'stages/notlove',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});
	}else{		
		$(this).addClass('love');
		$(this).attr('data-original-title','移除最愛');
		$.ajax({
			url: base_url+'stages/love',
			type:"post",
			data: {	id	:$(this).data('id')	}
		});		
	}
});	
<?php } ?>
</script>