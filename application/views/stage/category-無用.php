<div id="main">
<div class="container">
	<div id="stages" class="row">
		<div class="col-sm-9">
			<div class="row">
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/music');?>">
						<div class="thumbnail outer">
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/MUSIC256.png')?>" >
								<div class="caption">
									<hr>
									<h2>音樂</h2>
									<h4>演奏、唱歌</h4>
								</div>
							</div>
						</div>	
					</a>
				</div>
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/exhibition');?>">
						<div class="thumbnail outer">					
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/Exhibition256.png')?>" >
								<div class="caption">
									<hr>
									<h2>展覽</h2>
									<h4>畫作、攝影、設計</h4>	
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/lecture');?>">
						<div class="thumbnail outer">
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/Conference256.png')?>" >
								<div class="caption">
									<hr>
									<h2>講座</h2>
									<h4>藝文、旅遊、健康、科技、經濟</h4>	
								</div>
							</div>
						</div>
					</a>	
				</div>
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/teaching');?>">
						<div class="thumbnail outer">
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/Teaching256.png')?>" >
								<div class="caption">
									<hr>
									<h2>教學</h2>
									<h4>繪畫、樂器、手工藝、烘培、品飲</h4>
								</div>
							</div>
						</div>	
					</a>
				</div>
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/magic');?>">
						<div class="thumbnail outer">
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/Magic256.png')?>" >
								<div class="caption">
									<hr>
									<h2>魔術</h2>
									<h4>舞台魔術、近距離魔術</h4>
								</div>
							</div>
						</div>	
					</a>
				</div>
				<div class="col-sm-4 block">
					<a href="<?=base_url('stage/type/other');?>">
						<div class="thumbnail outer">
							<div class="text-center thumbnail inner">
								<img src="<?=base_url('public/img/OTHERS256.png')?>" >
								<div class="caption">
									<hr>
									<h2>其他</h2>
									<h4>舞蹈、說故事、表演藝術、行動藝術</h4>
								</div>
							</div>
						</div>
					</a>	
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<a href="<?=base_url('stage/type/other');?>">
				<div class="thumbnail outer">
					<div class="text-center thumbnail inner">
						<img src="<?=base_url('public/img/All256.png')?>" >
						<div class="caption">
							<hr>
							<h2>其他</h2>
							<h4>舞蹈、說故事、表演藝術、行動藝術</h4>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-12 how">		
			<h1>如何提供舞台？</h1>
			<div class="more">瞭解更多</div>
		</div>
	</div>
</div>	
</div>
<script>
$(document).on("click", '.goto-next', function() {
	
});
</script>