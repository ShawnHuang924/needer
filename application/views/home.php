<section id="cover">
	<div class="content">
		<div class="home_text">		
			<p>在NEEDER。你的</p>
			<p>你可以找到舞台 創造精采</p> 
			<p>也可以享受展演 放鬆沉澱</p>
		</div>
	</div>	
	<div class="next"><a data-go="#stage" data-num="0" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>
</section>
<div id="fix_nav"></div>
<!--各式舞台-->
<section id="stage" class="bg channel" data-section="0">
	<!--固定nav-->
	<nav id="fixtop" class="navbar navbar-static-top top hidden-xs  hidden-sm" data-fix="0" role="banner">				
		<nav class="collapse navbar-collapse">	
			<ul class="nav navbar-nav area">
				<div class="nav_dash"></div>
				<li class="noselect" data-num="0"><a class="active">各式舞台</a></li>
				<li class="noselect" data-num="1"><a >展演匯集</a></li>
				<li class="noselect" data-num="2"><a >問與答</a></li>
			</ul>	
			<ul class="nav navbar-nav navbar-right">
				<li class="noselect pad"><a class="btn topbtn" href="<?=base_url('stage')?>" style="padding:6px 12px;">尋找舞台</a></li>
				<li class="noselect pad"><a class="btn topbtn" href="<?=base_url('show')?>" style="padding:6px 12px;">查看展演</a></li>
				<?php if(!isset($logged_in)){?>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('member/login')?>" style="padding:6px 12px;">登入 | 註冊</a></li>
				<?php }else{ ?>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('myaccount')?>" style="padding:6px 12px;">會員中心</a></li>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('member/logout')?>" style="padding:6px 12px;">登出</a></li>
				<?php } ?>
				<!--<li id="go_top" class="noselect"><a >回到頂端</a></li>-->
			</ul>	
		</nav>		
	</nav>
	<div class="contents text-center">
		<div class="container">
			<div class="item_h1 item">
				<div class="item_center">
					<h1>就像擁有專屬經紀人</h1>
					<div class="next visible-xs"><a data-go="#p1" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>
				</div>
			</div>
			<div class="row info">
				<div id="p1" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle" src="<?=base_url('public/img/p1.png');?>" alt="...">
						<div class="caption">
							<h3>更簡單  更省時</h3>
							<p>不用再一家一家登門詢問是否提供展演機會，因為NEEDER已經幫你整理好了！</p>
						</div>
					</div>
					<div class="next visible-xs"><a data-go="#p2" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>	
				</div>
				<div id="p2" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle" src="<?=base_url('public/img/p2.png');?>" alt="...">
						<div class="caption">
							<h3>各地舞台  手到擒來</h3>
							<p>網羅全台各地的舞台，包含音樂、舞蹈、講座、教學、展覽等類別，就等你一展長才！</p>
						</div>	
					</div>	
					<div class="next visible-xs"><a data-go="#p3" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>	
				</div>
				<div id="p3" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle" src="<?=base_url('public/img/p3.png');?>" alt="...">
						<div class="caption">
							<h3>資訊詳情  觸手可及</h3>
							<p>舞台資訊完整、公開透明，搭配便利的篩選功能，輕鬆找到適合你的理想舞台！</p>
						</div>	
					</div>	
					<div class="next visible-xs"><a data-go="#h1" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>	
				</div>
			</div>	
		</div>	
	</div>	
	<div class="next"><a data-go="#showcase" data-num="1" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>
</section>
<!--展演匯集-->
<section id="showcase" class="bg channel" data-section="1">
	<div class="contents text-center">
		<div class="container">
			<div id="h1" class="item_h1 item">
				<div class="item_center">
					<h1>就是要你輕鬆享樂</h1>
					<div class="next visible-xs"><a data-go="#p4" class="goto-next"><img src="<?=base_url('public/img/arrow2.svg')?>"></a></div>
				</div>	
			</div>	
			<div class="row info">
				<div id="p4" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle w100" src="<?=base_url('public/img/p4.png');?>" alt="...">
						<div class="caption">
							<h3>為你匯整一切</h3>
							<p>全台都有NEEDER的合作夥伴，讓你不論在哪裡都有展演可看！</p>
						</div>	
					</div>
					<div class="next visible-xs"><a data-go="#p5" class="goto-next"><img src="<?=base_url('public/img/arrow2.svg')?>"></a></div>		
				</div>
				<div id="p5" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle w100" src="<?=base_url('public/img/p5.png');?>" alt="...">
						<div class="caption">
							<h3>更貼近你想要的</h3>
							<p>針對你的喜好、所在地區、預算，聰明快速地找出你有興趣的展演！</p>
						</div>
					</div>	
					<div class="next visible-xs"><a data-go="#p6" class="goto-next"><img src="<?=base_url('public/img/arrow2.svg')?>"></a></div>						
				</div>
				<div id="p6" class="col-sm-4 item">
					<div class="item_center">
						<img class="img-circle w100" src="<?=base_url('public/img/p6.png');?>" alt="...">
						<div class="caption">
							<h3>精采的不再錯過</h3>
							<p>將你有興趣的展演活動加入收藏，不再懊悔錯過任何一場你期待的展演！</p>
						</div>		
					</div>		
				</div>
			</div>	
		</div>	
	</div>	
	<div class="next"><a data-go="#faq" data-num="2" class="goto-next"><img src="<?=base_url('public/img/arrow2.svg')?>"></a></div>
</section>
<!--問與答-->
<section id="faq" class="bg channel" data-section="2">
	<div class="content">
		<div class="col-sm-6 center-block item">
			<div class="item_center">
				<div class="col-sm-offset-4 col-sm-9">
					<h2>尋找舞台</h2>				
				</div>
				<div class="col-sm-offset-4 col-sm-9 one">	
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何收費？</h3>					
					<p>我們的定位是一個免費提供資訊的平台，而非仲介的角色，所以NEEDER平台所有的服務都是完全免費的！</p>				
				</div>
				<div class="col-sm-offset-4 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">媒合方式為何？</h3>				
					<ol>
					  <li><p>瀏覽「尋找舞台」頁面找出您感興趣的舞台、活動</p></li>
					  <li><p>依照要求與備註的方式，積極主動地聯繫舞台提供者，用各種方式推薦自己</p></li>
					  <li><p>確定舞台、活動詳情與細節，做好萬全的準備</p></li>
					  <li><p>站上舞台、表現自己，讓大家看見你</p></li>
					</ol> 					
				</div>
				<div class="col-sm-offset-4 col-sm-9 one">
						<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
						<h3 class="noselect">如何知道實際舞台情況？</h3>
						<p>所有的舞台都先經過我們初步的審核，待確定其聯絡資訊皆為正確後，才會放上NEEDER平台供您瀏覽。但實際舞台情況，還是必須透過您在活動前與舞台提供者面談了解後，方能確認。</p>				
				</div>
				<div class="col-sm-offset-4 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何增加我的競爭優勢？</h3>
					<p>我們建議您可以透過下列方式</p>
					<ol>
					  <li><p>積極且禮貌地聯絡舞台提供者</p></li>
					  <li><p>提供您的相關經歷、照片</p></li>
					  <li><p>提供自己與該舞台相關的影音檔案或連結</p></li>
					</ol>
				</div>	
			</div>
			<div class="next visible-xs"><a data-go="#f2" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>
		</div>	
		<div id="f2" class="col-sm-6 center-block">
			
				<div class="col-sm-offset-1 col-sm-9">				
					<h2>提供舞台</h2>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何收費？</h3>
					<p>NEEDER的定位是一個提供資訊與活動媒合的平台，希望表演者能夠與舞台提供者彼此合作、創造雙贏，所以舞台的提供是不用負擔任何費用的。</p>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">媒合的方式為何？</h3>
					<ol>
					  <li><p>您提供一個適合的舞台</p></li>
					  <li><p>您的舞台資訊將呈現在NEEDER的「尋找舞台」頁面</p></li>
					  <li><p>尋找舞台者將依照您所要求的方式與您聯繫洽談</p></li>
					  <li><p>您可在會員中心的左側或直接在查看展演頁面點選「媒合回報」，向NEEDER回報活動媒合成功。</p></li>
					  <li><p>NEEDER將把您的詳細活動資訊與介紹曝光於「查看展演」頁面，讓更多觀眾可以知道您的活動訊息。</p></li>
					</ol>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何提供舞台？</h3>					
					<ol>
					  <li><p>請先至註冊頁面點選「公司會員」成為NEEDER的合作夥伴。</p></li>
					  <li><p>至頁尾點選「提供舞台」，依照表單內容填寫舞台提供資訊。</p></li>
					  <li><p>上傳5張貴公司的照片，照片內容包含招牌外觀、店內環境，以及您預計挪用為舞台的空間。</p></li>
					  <li><p>待審核通過，確認資料無誤，您的舞台資訊將在「尋找舞台」上呈現！</p></li>
					</ol>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">尋找舞台者將如何與我聯繫？</h3>
					<p>您可在填寫「提供舞台」的表格時，選擇希望被聯繫的方式（電子信箱、電話、兩者皆可），我們會依照您所選擇的方式，將您的連繫資訊提供給尋找舞台者。</p>
				</div>	
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何確保活動品質？</h3>
					<p>我們建議您:</p>
					  <p>在填寫「提供舞台」的表格時，可備註要求尋找舞台者提供他們個人的影音連結或檔案，讓您能更加了解尋找舞台者是否適合您所需的活動內容。</p>
					  <p>與尋找舞台者們面談，除了能了解他們是否為您的合適人選，更可讓雙方對活動細節有更深入的了解，以確保活動之品質。</p>				
				</div>		
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">任何單位都可以申請提供舞台嗎？</h3>
					<p>是的，只要您是合法的單位，並願意提供適合的舞台，都可以申請提供舞台。</p>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">店內的空間不大是否能夠提供舞台？</h3>
					<p>表演與活動的形式五花八門，只要您能提供一個適當的空間與機會，任何地方都能夠是一個好舞台。而適合小空間的類型有：畫展、攝影展、現場素描、近距離魔術、個人演奏、雕像藝人互動等性質都相當適合小空間的舞台。</p>
				</div>
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">申請後要多久才能夠在「尋找舞台」頁面中被看到？</h3>
					<p>NEEDER在確認完您提供的舞台資訊與活動詳情後，立即將您的資訊放在NEEDER「尋找舞台」頁面，替您尋求媒合，約1個工作天內即可看見您的舞台資訊曝光。</p>
				</div>		
				<div class="col-sm-offset-1 col-sm-9 one">
					<span class="glyphicon glyphicon-menu-right arrow arrows pointer noselect" aria-hidden="true"></span>
					<h3 class="noselect">如何回報活動訊息？</h3>
					<p>登入會員後，至會員中心的左側或直接在查看展演頁面點選「媒合回報」，並填寫活動資訊。</p>
					<p>NEEDER確認資訊無誤後會將您的活動資訊曝光在「查看展演」頁面，並提供您分享的功能，讓您可以進一步的替活動做宣傳。</p>
				</div>
		</div>
	</div>	
</section>

<!--
<section id="concept" class="bg">	
	<img class="concept_img  hidden-md hidden-lg" src="<?=base_url('public/img/pic02.jpg')?>">
	<div class=" concept_text">
		<div class="col-md-4 text">
			<h2>理念</h2>
			<p>Nascetur eu nibh vestibulum amet gravida nascetur praesent</p>
		</div>	
		<div class="col-md-4 text">
		Feugiat accumsan lorem eu ac lorem amet sed accumsan donec. Blandit orci porttitor semper. Arcu phasellus tortor enim mi nisi praesent dolor adipiscing. Integer mi sed nascetur cep aliquet augue varius tempus lobortis porttitor accumsan consequat adipiscing lorem dolor.
		</div>	
		<div class="col-md-4 text">
		Morbi enim nascetur et placerat lorem sed iaculis neque ante adipiscing adipiscing metus massa. Blandit orci porttitor semper. Arcu phasellus tortor enim mi mi nisi praesent adipiscing. Integer mi sed nascetur cep aliquet augue varius tempus. Feugiat lorem ipsum dolor nullam.
		</div>	
	</div>		
	<div class="next"><a data-go="#faq" class="goto-next"><img src="<?=base_url('public/img/arrow.svg')?>"></a></div>
</section>
-->




<script>
$(document).on("click", '.goto-next', function() {	
	var target = $($(this).data('go'));
	num = $(this).data('num');
	$('.nav_dash').animate({"left":num*120}, 800,'easeInOutSine',function(){scrolling=0});
	$("body,html").animate({"scrollTop":target.offset().top}, 800,"easeInOutSine");
});
$(document).on("click", '#go_top', function() {
	var body = $("html, body");
	scrolling=1;
	$('.nav_dash').animate({"left":120}, 500,'easeOutQuad',function(){scrolling=0});
	body.animate({scrollTop:0}, '500', 'swing');
});

//FAQ文字顯示
$(document).on("click", '#faq h3,#faq .arrows', function() {	
	var p = $(this).parent().find('li,p');
	$('#faq li,#faq p').hide(100);
	$('#faq .arrows').removeClass('turn90');
	if(p.is(":visible")){
		$(this).parent().find('.arrows').removeClass('turn90');
		p.hide(100);
	}
	else{
		$(this).parent().find('.arrows').addClass('turn90');
		p.show(100);
	}
});

//計算每個section的位置
function calculate_section(){	
	$("[data-section]").each(function(){
		section[$(this).data('section')]=$(this).offset().top;
	});	
	console.log(section);
}
$(window).load(function(){ calculate_section(); });
$(window).resize(function(){ calculate_section(); });
var fixed=0;	
var section= new Array();
var now_section=0;
var lastScrollTop=0;	
var scrolling=0;

//滾輪
$(document).on("scroll", function() {				
	var now = $(document).scrollTop();
	var top = now + $("#fixtop").outerHeight();						
	var cover_bot = $('#cover').offset().top+$('#cover').outerHeight();
	
	//固定的nav
	if(now>=cover_bot && fixed==0 ){
		$("#fixtop").clone().addClass('navbar-fixed-top').appendTo("#fix_nav");
		$('.nav_dash').animate({"left":num*120}, 500,'easeOutQuad');
		fixed=1;
	}else if(now<cover_bot && fixed==1){
		$('#fix_nav').empty();					
		fixed=0;
	}
	if(scrolling==0){
		var st = $(this).scrollTop();
		if (st > lastScrollTop){
			if(top >= section[now_section]){
				now_section++;
				$('.nav_dash').animate({"left":(now_section-1)*120}, 200,'easeOutQuad',function(){
					$('#fixtop li a').removeClass('active');
					$('[data-num="'+(now_section-1)+'"]').find('a').addClass('active');									
				});					
			}
			
		} else {
			if(top < section[now_section-1]){
				if(now_section>1)
					now_section--;
				$('.nav_dash').animate({"left":(now_section-1)*120}, 200,'easeOutQuad',function(){
					$('#fixtop li a').removeClass('active');
					$('[data-num="'+(now_section-1)+'"]').find('a').addClass('active');									
				});						
			}			
		}
		lastScrollTop = st;
	}
});

var num;
$(document).on("click", '#fixtop li', function() {
	if(scrolling==1){
		scrolling=0;
		return false;
	}			
	scrolling=1;
	if($(this).find('.active').length>0)					
		return false;
	num = $(this).data('num');
	$('#fixtop li a').removeClass('active');
	$(this).find('a').addClass('active');				
	var gogo = $("[data-section='" + num + "']").offset().top;
	$('.nav_dash').animate({"left":num*120}, 500,'easeOutQuad',function(){scrolling=0});
	$("html, body").animate({"scrollTop":gogo}, 500, 'easeOutQuad');
	now_section=num+1;
});




</script>