<div id="main" style="background: #1c1d26;color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<section id="register" class="row black_form">
			<form class="form-horizontal" method="post" action="" accept-charset="utf-8" data-toggle="validator" role="form">				
				<h2 class="text-center">刊登廣告</h2>
				<p>若您想與我們洽談廣告刊登事宜，歡迎您填寫下方表格，我們將與您討論合作方式與內容。</p>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" value="<?=set_value('company_name');?>"  name="company_name" placeholder="公司名稱*" required>
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="url" class="form-control clean" value="<?=set_value('company_web');?>"  name="company_web" placeholder="公司網址*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" value="<?=set_value('contact_name');?>"  name="contact_name" placeholder="連絡姓名*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="email" class="form-control clean" value="<?=set_value('email');?>"  name="email" placeholder="電子信箱*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group" style="margin-right:0;margin-left: 0;">
					<div class="form-group col-sm-10">
						<input type="text" class="form-control clean" value="<?=set_value('phone');?>"  name="phone" placeholder="連絡電話*" required >
						<span class="help-block with-errors"></span>
					</div>	
					<div class="form-group col-sm-2" style="float:right;">					
						<input type="text" class="form-control clean" value="<?=set_value('ext');?>"  name="ext" placeholder="分機號碼">
					</div>	
				</div>
				<div class="form-group" style="height:auto;">
					<div class="col-sm-12">
						<textarea type="text" class="form-control clean" name="remark" style="resize:none;" rows="5" placeholder="備註詳情"><?=set_value('remark');?></textarea>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group" style="height:auto;">
					<div class="col-sm-2">							
						<input type="text" class="form-control captcha_code clean" name="captcha"  maxlength="6" placeholder="請填入驗證碼" required>						
					</div>
					<div class="col-sm-4">
						<img id="captcha" class="captcha_img pointer" src="<?=$img;?>" />
					</div>
				</div>	
				<input type="button" id="clear" value="清除欄位重填">
				<input type="submit" id="go_register" value="確認送出">
			</form>
		</section>
	</div>	
</div>
<script>

//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('member/securimage_jpg')?>'); 				
});	

$(document).on("click", '#clear', function() {
	$('.clean').each(function(){
		$(this).val('');
	});
	$('input[type=checkbox]:checked,input[type=radio]:checked').each(function(){
		$(this).attr('checked', false);
	});
	$('select').each(function(){
		$(this).val($(this).find("option:first").val());
	});
});	
</script>