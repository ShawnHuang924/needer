<div id="main" style="background: #1c1d26;color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<section id="register" class="row black_form">
			<form class="form-horizontal" method="post" accept-charset="utf-8" action="" data-toggle="validator" role="form">				
				<h2 class="text-center">聯絡我們</h2>
				<p>若您有任何疑慮或問題想詢問我們，歡迎您填寫下方表格，或是透過電子信箱service@needer.tw與我們聯繫，待收到您的詢問後，我們將以最快的速度回覆您。</p>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" value="<?=set_value('contact_name');?>" name="contact_name"  placeholder="聯絡姓名*" required>
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="email" class="form-control clean" value="<?=set_value('email');?>" name="email" placeholder="電子信箱*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" value="<?=set_value('phone');?>" name="phone" placeholder="聯絡電話*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" value="<?=set_value('subject');?>" style="resize:none;" name="subject" placeholder="問題主旨*" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group" style="height:auto;">
					<div class="col-sm-12">
						<textarea type="text" class="form-control clean" name="problem" style="resize:none;" rows="5" placeholder="問題說明*" required><?=set_value('problem');?></textarea>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group" style="height:auto;">
					<div class="col-sm-2 col-xs-2">							
						<input type="text" class="form-control captcha_code clean" name="captcha"  maxlength="6" placeholder="請填入驗證碼" required>						
					</div>
					<div class="col-sm-4 col-xs-2">
						<img id="captcha" class="captcha_img pointer" src="<?=$img;?>" />
					</div>
				</div>	
				<input type="button" id="clear" value="清除欄位重填">
				<input type="submit" id="go_register" value="確認送出">
			</form>
		</section>
	</div>	
</div>
<script>

//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('member/securimage_jpg')?>'); 				
});	

$(document).on("click", '#clear', function() {
	$('.clean').each(function(){
		$(this).val('');
	});
	$('input[type=checkbox]:checked,input[type=radio]:checked').each(function(){
		$(this).attr('checked', false);
	});
	$('select').each(function(){
		$(this).val($(this).find("option:first").val());
	});
});	
</script>