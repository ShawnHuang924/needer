<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>	
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,user-scalable=no">	
	<meta name="google" 		content="notranslate" />
    <!-- Facebook Open Graph Tags-->
    <meta property="og:title" 		content="NEEDER。你的"/> 				
    <meta property="og:type" 		content="website"/> 						
    <meta property="og:url" 		content="<?=base_url();?>"/> 		
    <meta property="og:image" 		content="<?=base_url('public/img/logo.png');?>"/> 		
    <meta property="og:site_name" 	content="NEEDER。你的"/>            
	<meta property="og:description" content="有時候你需要的只是一個機會，但也不該只是在原地等待機會。"/>
	<!-- SEO -->
	<title>NEEDER。你的</title>	
	<meta name="description"	content="有時候你需要的只是一個機會，但也不該只是在原地等待機會。" />
	<meta name="keywords" 		content="NEEDER,你的,chance,機會,stage,舞台,表演,演出,講座,音樂,舞蹈,魔術,教學,藝術" />	
	<!-- Web Icon -->
	<link rel="shortcut icon" href="<?=base_url('public/img/needer_icon.ico');?>" />
	<!-- Jquery -->
	<script src="<?=base_url('public/plugin/jquery/jquery-2.1.3.min.js');?>" type="text/javascript"></script>
	<script src="<?=base_url('public/plugin/jquery/jquery.easing.min.js');?>" type="text/javascript"></script>
	<!-- jQuery UI -->
	<link  href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>			
	<!-- Bootstrap -->
    <link href="<?=base_url('public/plugin/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
    <script src="<?=base_url('public/plugin/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
	<!-- Bootstrap select -->
	<link  href="<?=base_url('public/css/bootstrap-select.min.css');?>" rel="stylesheet"/>
	<script src="<?=base_url('public/js/bootstrap-select.min.js');?>"></script>	
	<!-- Bootbox -->
	<script src="<?=base_url('public/js/bootbox.min.js');?>"></script>
	<!-- Summernote -->
	<link href="<?=base_url('public/plugin/Summernote/dist/summernote.css');?>" rel="stylesheet">
	<script src="<?=base_url('public/plugin/Summernote/dist/summernote.min.js');?>"></script>
	<script src="<?=base_url('public/plugin/Summernote/lang/summernote-zh-TW.js');?>"></script>	
	<!-- Combodate -->
	<script src="<?=base_url('public/plugin/combodate/moment.min.1.7.2.js');?>" type="text/javascript"></script>
	<script src="<?=base_url('public/plugin/combodate/combodate.js');?>" type="text/javascript"></script>
	
	<script type="text/javascript" src="<?=base_url('public/plugin/bootstrap-datetimepicker/build/js/moment.min.js');?>"></script>
	
	<link rel="stylesheet" type="text/css" href="<?= base_url('public/plugin/datepicker/css/bootstrap-datepicker3.standalone.css');?>">
    <script type="text/javascript" src="<?= base_url('public/plugin/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
	<script type="text/javascript" src="<?= base_url('public/plugin/datepicker/locales/bootstrap-datepicker.zh-TW.min.js');?>"></script>

	<script type="text/javascript" src="<?=base_url('public/plugin/clockpicker/dist/bootstrap-clockpicker.min.js');?>"></script>
	<link rel="stylesheet" href="<?=base_url('public/plugin/clockpicker/dist/bootstrap-clockpicker.min.css');?>" />	
	
	<script src="<?=base_url('public/plugin/bootstrap-validator/dist/validator.min.js');?>" type="text/javascript"></script>
	
	<link href=" http://cdn.syncfusion.com/js/web/flat-azure/ej.web.all-latest.min.css" rel="stylesheet">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="<?=base_url('public/js/ejdatepicker/globalize.min.js');?>"></script>
	<script src="<?=base_url('public/js/ejdatepicker/globalize.cultures.js');?>"></script>
	<script src="<?=base_url('public/js/ejdatepicker/ej.core.min.js');?>"></script>			
	<script src="<?=base_url('public/js/ejdatepicker/ej.datepicker.min.js');?>"></script>
	
	<script src="<?=base_url('public/plugin/twzipcode/jquery.twzipcode.min.js');?>" type="text/javascript"></script>
	<script src="<?=base_url('public/js/prettyCheckable.js');?>" type="text/javascript"></script>
	<script src="<?=base_url('public/js/spin.min.js');?>" type="text/javascript"></script>
	
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('public/plugin/font-awesome/css/font-awesome.min.css');?>">
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="<?=base_url('public/css/mystyle.css');?>">	
	<script> var base_url="<?=base_url();?>"; </script>	
	<script src="<?=base_url('public/js/common.js');?>" type="text/javascript"></script>
</head>
<body class="my_scrollbar">
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '453521551472966',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/zh_TW/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
$(document).ready(function(){  
   $('[data-toggle="tooltip"]').tooltip();
});
</script>
<?=$header;?>
<?php 
if(isset($leftmenu))
	echo $leftmenu;
?>
<?=$body;?>	
<?php 
if(isset($leftmenu_end))
	echo $leftmenu_end;
?>
<?=$footer;?>				
</body>
</html>