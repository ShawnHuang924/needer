<!-- Jquery.fileupload -->
<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload.css');?>" rel="stylesheet" >
<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload-ui.css');?>" rel="stylesheet" >
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/vendor/jquery.ui.widget.js');?>"></script>
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.iframe-transport.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload.js');?>"></script>	
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-process.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-image.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-validate.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-ui.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/cors/jquery.xdr-transport.js');?>"></script>


<div id="main">
<div class="container">
	<!-- 固定欄位寬度為300px -->
	<div class="col-fixed hidden-xs left_filter">
		<div class="thumbnail" style="padding-bottom:0;position:relative" >
			<div class="photo-wrapper100">
				<?php
				$user_id = $this->session->userdata('logged_in')['id'];
				$dir = ROOT.'/public/photos/member/'.$user_id.'/pic';				
				$file = isset(glob($dir.'/*.*')[0])?glob($dir.'/*.*')[0]:false;
				if($file)
					$filename = base_url('public/photos/member/'.$user_id.'/pic/'.basename($file));
				else
					$filename = base_url('public/img/member.png');
				?>
				<div id="mypic" class="photo" style="background-image: url(<?=$filename;?>)"></div>
			</div>	
			<div class="change_pic fadeIn" href="#chagepicModal" data-toggle="modal">更換大頭照</div>	
		</div>		
		<div class="panel-body">
			<div class="list-group">
				<a href="<?=base_url('myaccount/basic_info');?>" class="list-group-item">基本資料</a>
				<a href="<?=base_url('myaccount/favorite_stage');?>" class="list-group-item">最愛舞台</a>
				<a href="<?=base_url('myaccount/favorite_show');?>" class="list-group-item">最愛展演</a>
				<?php if($logged_in['type']==2){?>
				<a href="<?=base_url('myaccount/mystage');?>" class="list-group-item">我的舞台</a>
				<a href="<?=base_url('myaccount/match');?>" class="list-group-item">媒合提報</a>
				<?php } ?>
			</div>
		</div>		
	</div>