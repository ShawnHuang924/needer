</div>	
</div>


<div class="modal fade" id="chagepicModal" tabindex="-1" role="dialog" aria-labelledby="chagepicModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:350px;">
		<div class="modal-content">
			<div class="modal-body clearfix upload_form" >
				<div class="edit_pic" style="min-height:300px;width: 100%;">  
					<div class="files" role="presentation"></div>						
					<div class="new_pic">							
						請將照片拖曳至此 或 點選新增照片
						<span class="btn fileinput-button fileinput_all" >
							<input type="file" name="files[]" accept="image/*" style="cursor:pointer;">
						</span>
					</div>
				</div>
				<button id="upload_pic" class="btn" style="width: 100%;background: #e44c65;color: #ffffff;">上傳</button>
			</div>
		</div>
	</div>
</div>

<!-- 上傳前的template -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-upload fade">		
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>
<!-- 上傳完後的template -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="template-download fade">
	<span class="delete photos_delete" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		<i class="fa fa-times"></i>				
	</span>		
	<img class="target" src="{%=file.url+'?'+(new Date()).getTime()%}" style="width: 100%;height:auto ">						
	</div>
{% } %}
</script>
<script>
var flag=0;
create_single_upload('tmp');
$(document).on('click','#upload_pic',function(){		
	$.ajax({
		url: '<?=base_url("upload/move_pic");?>',
		type:"post",
		success: function(){
			flag=1;
			$('#mypic').css('background-image','url('+$('.target').attr('src').replace('tmp', "pic")+')');
			$('#chagepicModal .photos_delete').trigger('click');
			$('#upload_pic').hide();						
		}			
	});
});
</script>