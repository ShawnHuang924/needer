<nav id="top" class="navbar navbar-static-top top <?=$top?>" role="banner">
	<div class="container-fluid">	
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?=base_url('home')?>"><img src="<?=base_url('public/img/needer_logo_56nr.png')?>"></a>
		</div>
		<nav id="navbar" class="collapse navbar-collapse">			
			<ul class="nav navbar-nav navbar-right">
				<?php if($this->router->fetch_class()=='stage' && isset($logged_in) && $logged_in['type']==2){?>
				<li class="noselect pad"><a class="btn btn-info topbtn" href="<?=base_url('member/provide_stage_info')?>" style="padding:6px 12px;background-color: #5bc0de;">提供舞台</a></li>
				<?php } ?>
				<?php if($this->router->fetch_class()=='show' && isset($logged_in) && $logged_in['type']==2){?>
				<li class="noselect pad"><a class="btn btn-info topbtn" href="<?=base_url('myaccount/match')?>" style="padding:6px 12px;background-color: #5bc0de;">媒合提報</a></li>
				<?php } ?>
				<li class="noselect pad"><a class="btn topbtn" href="<?=base_url('stage')?>" style="padding:6px 12px;">尋找舞台</a></li>
				<li class="noselect pad"><a class="btn topbtn" href="<?=base_url('show')?>" style="padding:6px 12px;">查看展演</a></li>
				<?php if(!isset($logged_in)){?>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('member/login')?>" style="padding:6px 12px;">登入 | 註冊</a></li>
				<?php }else{ ?>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('myaccount')?>" style="padding:6px 12px;">會員中心</a></li>
				<li class="noselect pad"><a class="btn login" href="<?=base_url('member/logout')?>" style="padding:6px 12px;">登出</a></li>
				<?php } ?>
			</ul>					  
		</nav>	
	</div>
</nav>
