<a id="scrollToTop" class="scrollToTop scrollToTop_cycle"><i class="fa fa-angle-up"></i></a>
<script>
$(document).on("click", '#scrollToTop', function() {
	var body = $("html, body");	
	body.animate({scrollTop:0}, '500', 'swing');
});
</script>
<footer class="container-fluid">
	<div class="container">
		<div class="row clearfix icons">
			<div class="col-sm-2 col-xs-6">
				<div class="text-center">
					<a href="<?=base_url('advertising')?>">
						<img class="icon_img" src="<?=base_url('public/img/c1.png')?>">
						<div class="caption">
							<h4>刊登廣告</h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
				<div class="text-center">
					<?php if(isset($logged_in) && $logged_in['type']==2){?>
					<a href="<?=base_url('member/provide_stage_info')?>">
					<?php }else{ ?>
					<a href="#notcompanyModal" data-toggle="modal">
					<?php } ?>
						<img class="icon_img" src="<?=base_url('public/img/c2.png')?>">
						<div class="caption">
							<h4>提供舞台</h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">		
				<div class="text-center">
					<a href="<?=base_url('contact_us')?>">
						<img class="icon_img" src="<?=base_url('public/img/c3.png')?>">
						<div class="caption">
							<h4>聯絡我們</h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
				<div class="text-center">
					<a target="_blank" href="https://www.flickr.com/photos/130458266@N02/sets">
						<img class="icon_img" src="<?=base_url('public/img/c4.png')?>">
						<div class="caption">
							<h4>展演照片</h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
				<div class="text-center">
					<a target="_blank" href="https://www.youtube.com/channel/UCYgyAJoU3txrG7M3NM3UJcg/videos">
						<img class="icon_img" src="<?=base_url('public/img/c5.png')?>">
						<div class="caption">
							<h4>展演影片</h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">			
				<div class="text-center">
					<a target="_blank" href="https://www.facebook.com/needer.tw">
						<img class="icon_img" src="<?=base_url('public/img/c6.png')?>">
						<div class="caption">
							<h4>粉絲專頁</h4>
						</div>
					</a>
				</div>			
			</div>
		</div>
		<div class="row clearfix">	
			<div class="col-sm-12 text-center">
				<p>Copyright © 2015 NEEDER。你的 版權所有</p>
			</div>
		</div>
	</div>
</footer>

<!-- notloginModal -->
<div class="modal fade alert_modal" id="notloginModal" tabindex="-1" role="dialog" aria-labelledby="notloginModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width:300px;">
		<div class="modal-content">
			<div class="modal-body clearfix" >	
				<div style="margin-bottom:10px;">登入會員後就能開始瀏覽使用囉！</div>
				<a href="<?=base_url('member/register')?>">註冊</a>
				<a href="<?=base_url('member/login')?>">登入</a>
			</div>
		</div>
	</div>
</div>

<!-- notcompanyModal -->
<div class="modal fade alert_modal" id="notcompanyModal" tabindex="-1" role="dialog" aria-labelledby="notcompanyModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width:300px;">
		<div class="modal-content">
			<div class="modal-body clearfix" >	
				<div style="margin-bottom:10px;">此功能僅開放公司會員使用，請登入或註冊公司會員，謝謝！</div>
				<a href="<?=base_url('member/register')?>">註冊</a>
				<a href="<?=base_url('member/login')?>">登入</a>
			</div>
		</div>
	</div>
</div>
