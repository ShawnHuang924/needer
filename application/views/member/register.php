<div id="main" style="background: #1c1d26;color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<section id="register" class="row black_form">
			<form class="form-horizontal" method="post" action="<?=base_url('member/register_check');?>" accept-charset="utf-8" action="" data-toggle="validator" role="form">				
				<h2 class="text-center">會員註冊</h2>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>				
				<div class="form-group" id="member_type">					
					<label class="col-lg-1 col-md-2 col-sm-2 control-label la45">會員類型*</label>
					<div class="form_item1 form_stage control-label la45" style="text-align:center!important">個人會員
						<input class="hidding" name="type" id="normal" type="radio" value="1" checked>	
						<label for="normal" class="circles2 pointer noselect" ></label>	
					</div>
					<div class="form_item1 form_stage control-label la45" style="text-align:center!important">公司會員
						<input class="hidding" name="type" id="provide" type="radio" value="2">
						<label for="provide" class="circles2 pointer noselect" ></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="email" class="form-control clean" name="account" placeholder="電子信箱*" required  data-remote="is_access?account=<value>" data-remote-error="帳號已存在" >
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control clean" id="pwd" name="password" placeholder="請輸入密碼（6碼以上）*" required data-minlength="6" data-minlength-error="密碼長度不夠"  >
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="password" class="form-control clean" name="password_confirm" placeholder="密碼確認*" required data-match="#pwd" data-match-error="密碼不吻合">
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<input type="text" class="form-control clean" name="last_name" placeholder="姓*" required>
						<span class="help-block with-errors"></span>
					</div>	
					<div class="col-sm-3">
						<input type="text" class="form-control clean" name="name" placeholder="名*" required>
						<span class="help-block with-errors"></span>
					</div>
					<label class="col-sm-2 control-label la45" style="text-align:right!important">性別*</label>
					<div class="col-sm-1 control-label la45" style="text-align:center!important">男
						<input class="hidding" name="sex" id="boy" type="radio" value="1" checked>	
						<label for="boy" class="circles pointer noselect"></label>	
					</div>
					<div class="col-sm-1 control-label la45" style="text-align:center!important">女
						<input class="hidding" name="sex" id="girl" type="radio" value="0">
						<label for="girl" class="circles pointer noselect"></label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" class="form-control clean" name="phone" placeholder="聯絡電話 (手機或市話皆可) *" required>
						<span class="help-block with-errors"></span>
					</div>				
				</div>
				<div class="form-group">
					<label for="my_county" class="col-lg-1 col-sm-2 control-label la45">居住城市*</label>
					<div class="col-sm-8">
						<select name="my_county" id="my_county" required>
							<?php foreach($county as $key => $val){?>
							<option value="<?=$val['id']?>"><?=$val['county_name']?></option>
							<?php } ?>
						</select>
					</div>				
				</div>
				<div class="form-group">
					<label for="birthday" class="col-lg-1 col-sm-2 control-label la45">出生日期*</label>
					<div class="col-sm-8">
						<input type="text" id="birthday" data-firstItem="none" data-format="YYYY-MM-DD" data-template="YYYY MM DD" value="1990-01-01" name="birthday" required>
						<span class="help-block with-errors"></span>
					</div>					
				</div>
				<div class="form-group" id="mytheme" style="height:auto;">
					<div class="col-sm-12">
						<div class="boder clearfix">
						<label for="type" class="col-sm-12 control-label la45">感興趣主題 (複選) *</label>
						<div class="col-sm-12">	
							<div class="col-sm-1 control-label la45" style="text-align:center!important">音樂
								<input class="hidding" name="theme[]" id="music" type="checkbox" value="1">
								<label for="music" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">舞蹈
								<input class="hidding" name="theme[]" id="dance" type="checkbox" value="2">
								<label for="dance" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">語言
								<input class="hidding" name="theme[]" id="language" type="checkbox" value="3">
								<label for="language" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">藝文
								<input class="hidding" name="theme[]" id="art" type="checkbox" value="4">
								<label for="art" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">電影
								<input class="hidding" name="theme[]" id="movie" type="checkbox" value="5">
								<label for="movie" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">魔術
								<input class="hidding" name="theme[]" id="magic" type="checkbox" value="6">
								<label for="magic" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">旅遊
								<input class="hidding" name="theme[]" id="travel" type="checkbox" value="7">
								<label for="travel" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">攝影
								<input class="hidding" name="theme[]" id="photo" type="checkbox" value="8">
								<label for="photo" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">設計
								<input class="hidding" name="theme[]" id="design" type="checkbox" value="9">
								<label for="design" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">科技
								<input class="hidding" name="theme[]" id="technology" type="checkbox" value="10">
								<label for="technology" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">美食
								<input class="hidding" name="theme[]" id="food" type="checkbox" value="11">
								<label for="food" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">烹飪
								<input class="hidding" name="theme[]" id="cooking" type="checkbox" value="12">
								<label for="cooking" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">園藝
								<input class="hidding" name="theme[]" id="gardening" type="checkbox" value="13">
								<label for="gardening" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">寵物
								<input class="hidding" name="theme[]" id="pet" type="checkbox" value="14">
								<label for="pet" class="circles pointer noselect"></label>
							</div>
							<div class="col-lg-1 col-md-2 col-sm-2 control-label la45" style="text-align:center!important">戶外運動
								<input class="hidding" name="theme[]" id="sport" type="checkbox" value="15">
								<label for="sport" class="circles pointer noselect"></label>
							</div>
							<div class="col-sm-1 control-label la45" style="text-align:center!important">其他
								<input class="hidding" name="theme[]" id="other" type="checkbox" value="16">
								<label for="other" class="circles pointer noselect"></label>
							</div>
						</div>
						</div>
						<span id="theme_error" class="help-block with-errors" style="display:none;color:#a94442"><ul class="list-unstyled"><li>至少選擇一個主題</li></ul></span>
					</div>
				</div>
				
				<div id="company" style="display:none">
					<!--<hr style="border-top: 1px dashed #e44c65;">-->
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control clean" name="company_name" placeholder="公司/單位名稱*">
							<span id="company_error" class="help-block with-errors" style="display:none;color:#a94442"><ul class="list-unstyled"><li>請填寫這個欄位。</li></ul></span>
						</div>				
					</div>
					<div class="form-group">
						<label for="company_type" class="col-lg-2 col-sm-2 control-label la45">公司/單位類型*</label>
						<div class="col-sm-8">
							<select id="company_type" name="company_type">
								<?php foreach($company_type as $key => $val){?>
								<option value="<?=$val['key']?>"><?=$val['value']?></option>
								<?php } ?>
							</select>
						</div>				
					</div>
					<div class="form-group">
						<label for="company_address" class="col-lg-2 col-md-2 col-sm-2 control-label la45">公司/單位地址*</label>
						<div class="col-lg-10 col-md-10 col-sm-10">
							<div id="twzipcode">
								<div data-role="county"
									 data-name="county"
									 data-style="county-style">
								</div>
								<div data-role="district"
									 data-name="district"
									 data-style="district-style"></div>
								<div data-role="zipcode"
									 data-style="zipcode-style"></div>
							</div>	
							<input type="text" class="form-control clean" name="company_address" style="margin-top: 50px;" placeholder="詳細地址">
							<span id="address_error" class="help-block with-errors" style="display:none;color:#a94442"><ul class="list-unstyled"><li>請填寫這個欄位。</li></ul></span>
						</div>				
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control clean" name="company_web" placeholder="公司/單位網址或Facebook粉絲專業">
						</div>				
					</div>
				</div>		
					
				<div class="form-group" style="height:auto;">
					<div class="col-sm-12">
						<textarea type="text" class="form-control member_provision my_scrollbar" rows="9" readonly >
親愛的朋友你好：
歡迎你加入「NEEDER。你的」成為會員，當你選擇加入「NEEDER。你的」成為會員時，即視為你已閱讀、瞭解並同意接受「NEEDER。你的會員規約」之全部內容，若你不同意，請立即登出並停止使用本網站所提供之任何會員服務。NEEDER。你的有限公司有權於任何時間修改或變更本會員規約之內容，你若於修改或變更後繼續使用「NEEDER。你的」之服務，即視為你已閱讀、瞭解並同意接受該等修改或變更後之內容。
(以下你以「本人」簡稱；NEEDER。你的有限公司經營之「NEEDER。你的」以「本網站」簡稱)
第一條：本網站告知事項
本網站根據個人資料保護法第八條規定，公告以下告知事項：
(一)蒐集目的：會員服務與管理。
(二)個人資料類別：識別類、特徵類、家庭情形、社會情況、教育、技術或其他專業、受僱情形。
(三)個人資料利用期間：本網站存續期間。
(四)個人資料利用對象：本網站及利用本網站服務之會員。
(五)個人資料利用地區：利用對象所在地區。
(六)個人資料利用方法：網際網路、電子郵件、簡訊、書面及傳真。
(七)當事人依個人資料保護法第三條規定得行使之權利及方式：當事人得於本網站自行處理修改密碼、隱私及帳戶設定、修改基本資料，以及編輯或刪除分享之內容。當事人亦得透過線上NEEDER。你的網站意見反應留言。
(八)當事人得自由選擇提供個人資料時，不提供將對其權益之影響：本網站係一提供以舞台表演社群服務為主之網站，當事人若不加入成為本網站會員，本網站將無法適時或完整提供當事人相關之服務。
(九)本網站得以配合司法檢調單位提供使用者個人資料以協助司法調查。
第二條：本網站提供之會員服務  
(一)會員訊息服務：本條所稱「會員訊息服務」是指本網站得依本人之設定，以電子郵件方式，寄送相關之職涯資訊或會員互動通知訊息予本人。
(二)內容建立與分享：本條所稱「內容建立與分享」是指本人在本網站發布之所有資料內容，本人同意提供本網站非獨家、可轉讓、可再授權、全部免費之全球授權，惟當本人刪除本人提供之內容或帳號時，則該部分之授權視為終止，但本人提供之內容已經由他人分享至以他人帳號發表之內容，則不在此限。本人明瞭當本人使用公開設定發布內容或資料時，即視為本人已允許所有人皆得存取或使用該資料，並且將該則內容或資料與本人聯想在一起，故本人對於任何希望保密或不希望提供他人使用之內容或資料，均不應發布於本網站上。
(三)串連第三方網站或開發商：本條所稱「串連第三方網站或開發商」是指本網站提供之服務內容，可能包含連結到第三方網站或提供由第三方程式開發人員開發之平台應用程式，本人必須自行評估是否要使用此串連服務，對於本人使用此串連服務，本人須自行承擔風險。
(四)提醒你，本網站沒有義務介入會員與店家之間之爭執，亦不會因此而關閉本人或其他會員之帳戶，除非本人或會員違反本網站之使用規範且情節重大。
第三條：本人個資管理及維護
本人同意將本人於本網站填寫及留存之會員個人資料，無償且不附帶任何條件提供予本網站蒐集、處理、國際傳輸及利用：
(一)作為會員辨識及發送訊息之用。
(二)作為統計與分析之用，例如：針對會員男女分佈、學歷、職務、地區等為研究。分析結果之呈現方式僅有整體之數據及說明文字，除供內部研究外，本網站得視需要對外公布該等數據及說明文字，但不涉及特定個人資料之公開。
(三)作為接收NEEDER。你的各項服務與資訊之用。
(四)其他另經過本人以書面、電子郵件、傳真、網頁點選按鈕同意之利用。
(五)除個人資料的維護與記錄外，本網站沒有義務儲存、維護或備份本人所使用或發表的任何內容。
第四條：會員注意事項
為了使用本網站提供之會員服務，本人同意以下事項：
(一)一人限申請使用一帳號。
(二)依本服務註冊表之提示提供本人正確、最新及完整之資料。
(三)如果本人不同意本網站之會員規約，則不應登錄成本網站會員或使用本網站之任何服務。
(四)本人同意且了解，不得與他人共用同一帳戶，亦不得將登入本網站之帳號及密碼提供或透露予任何第三人，任何第三人因獲悉本人帳號密碼而登入本人帳戶所發生之任何問題，概由本人自行承擔所有後果。
第五條：本網站安全維護
(一)本人同意不得於本網站騷擾、恐嚇其他會員或從事任何非法、違反公序良俗之行為。
(二)本人同意不上傳病毒或其他攻擊程式碼。
(三)本人同意未經本網站事前書面同意，不得發布任何他人之個人聯絡資訊。
(四)本人同意不發布任何誹謗侵害他人名譽或為謠言、猜測、捏造不實言論等行為。
(五)本人同意不在本網站從事任何銷售或多層次傳銷或與發表與主題無關的商業廣告行為。
(六)本人同意不會進行任何會導致本網站關閉、超載、損害正常運作或外觀之行為。
(七)本人同意不得於於本網站公開蒐集其他使用者之聯絡資訊。
第六條：保護其他人權利
(一)本人同意不會在本網站張貼任何侵犯、揭露他人隱私或純屬他人私領域之言論或物品。
(二)對於本人張貼的任何內容或資料若有違反本網站政策或會員規約，本人同意本網站有權逕行移除。若經本網站通知仍未改善者，本網站有權停止本人繼續使用本網站之權利。
(三)本人同意且了解，若本人須蒐集部份會員資料，本人必須事先徵得該會員之同意，並明確表示是本人（而非本網站）蒐集其個人資料，並明確告知蒐集後之處理與利用方式。
(四)本人同意不會亦不得誘使他人於本網站，發布其個人身分證明文件或敏感性個人資料。
(五)本人同意且了解，若因本人未遵守本網站會員規約，致侵害他人權益或造成本網站之損害（包括且不限於商譽之損害），本人同意負擔所有相關之損害賠償或回復原狀之責任。
第七條：本網站之免責約定
(一)本人明瞭於本網站發布之內容或資料，若非本人所獨立創作或是任何人得公開使用之著作，而是重製或引用他人享有著作權之著作時，本人應注意著作權法之合理使用規範，若生爭議，本人除應立即刪除涉有侵害他人著作權之內容或資料外，並應主動與他人協商解決，本網站對此不負任何賠償責任。
(二)本人明瞭本人經由本網站取得之資訊或建議，本網站並不擔保其為完全正確無誤。本網站對於本服務所提供之資訊或建議有權隨時修改或刪除。
(三)本網站隨時會與廠商等第三人（「內容提供者」）合作，由其提供包括新聞、訊息、電子報等不同內容供本網站刊登，本網站對其所提供之內容並不做實質之審查或修改，對該內容之正確真偽亦不負任何責任。對該內容之正確真偽，須由本人自行判斷之。
(四)本人明瞭在本網站瀏覽到的所有廣告內容、文字與圖片之說明、展示樣品或其他銷售資訊，均由各該廣告商所設計與提出。本人需自行斟酌與判斷該廣告之正確性與可信度。本網站僅接受委託予以刊登，不對前述廣告內容負擔保責任。
(五)本人同意不對本網站或104企業集團就以上情形之發生為民事損害賠償之請求，亦不任意公開散佈有損本網站或104企業集團或其負責人、董監事、受僱人商譽或名譽之言論或文字。
第八條：違約處理
本人同意若發生違反以上各項約定，經查明屬實者，本網站得取消本人之會員資格，並拒絕本人邇後使用本網站各項服務之權利。另本人若違反第六條、第七條之約定者，本網站另可將本人之姓氏、會員代碼或暱稱及事實經過公告於本網站。
本人同意若本人違反本網站之使用條款或服務規範，本網站有權逕行移除您所發布的內容，並視情況停止您使用本網站服務的權利。
第九條：隱私權保護政策
本網站之隱私權保護政策準用NEEDER。你的隱私權保護政策辦理，本人明瞭該隱私權保護政策亦構成本會員規約之一部份。
第十條：準據法及管轄法院
本會員規約之解釋與適用，以中華民國法律為準據法。本人與NEEDER。你的有限公司共同約定，若因使用本網站發生爭議而有訴訟之必要時，雙方合意以台灣台北地方法院為第一審管轄法院。
</textarea>
					</div>				
				</div>
				<div class="form-group col-sm-12 smart-forms" >
					<label class="option noselect pointer">
						<input type="checkbox" class="provision" name="provision" value="1" required  data-error="您必須同意會員條款">
						<span class="checkbox"></span> 我已閱讀上述會員條款且願意遵守條款內容*
						<span class="help-block with-errors"></span>
					</label>					
				</div>
				<div class="form-group col-sm-12 smart-forms" >
					<label class="option noselect pointer">
						<input type="checkbox" class="provision" name="subscription" value="1">
						<span class="checkbox"></span> 訂閱會員電子報以掌握最新的舞台資訊
					</label>					
				</div>
				<div class="form-group">
					<div class="col-sm-2">							
						<input type="text" class="form-control captcha_code clean" name="captcha"  maxlength="6" placeholder="請填入驗證碼" required >						
					</div>
					<div class="col-sm-4">
						<img id="captcha" class="captcha_img pointer" src="<?=$img;?>" />
					</div>
				</div>				
				<input type="button" id="clear" value="清除欄位重填">
				<input type="submit" id="go_register" value="註冊會員">
			</form>
		</section>
	</div>	
</div>
<script>
$('#birthday').combodate();
$('#twzipcode').twzipcode();
$(document).on("click","#member_type",function() {
    if($('input[name=type]:checked').val()==2)
		$('#company').show();
	else
		$('#company').hide();
});
//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('member/securimage_jpg')?>'); 				
});	

$(document).on("click", '#clear', function() {
	$('.clean').each(function(){
		$(this).val('');
	});
	$('input[type=checkbox]:checked,input[type=radio]:checked').each(function(){
		$(this).attr('checked', false);
	});
	$('select').each(function(){
		$(this).val($(this).find("option:first").val());
	});
});	

$('form').submit(function() {
	$('#theme_error').hide();
	if($('#mytheme input:checked').length==0){
		$('#theme_error').show();
		return false;
	}
	if($('#member_type input:checked').val()==2){
		if($('[name=company_name]').val()=='')
			$('#company_error').show();
		if($('[name=company_address]').val()=='')
			$('#address_error').show();
	}
});

</script>