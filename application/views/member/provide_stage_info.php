<!-- Jquery.fileupload -->
<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload.css');?>" rel="stylesheet" >
<link href="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/css/jquery.fileupload-ui.css');?>" rel="stylesheet" >
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/vendor/jquery.ui.widget.js');?>"></script>
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.iframe-transport.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload.js');?>"></script>	
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-process.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-image.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-validate.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/jquery.fileupload-ui.js');?>"></script>
<script src="<?=base_url('public/plugin/jQuery-File-Upload-9.8.1/js/cors/jquery.xdr-transport.js');?>"></script>



<div id="main" style="background: #1c1d26;color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<section id="provide_stage" class="row black_form">
			<form class="form-horizontal stage_block" method="post" action="<?=base_url('member/provide_stage_info_check');?>" accept-charset="utf-8"  data-toggle="validator" role="form" >				
				<h2 class="text-center">基本資料核對</h2>
				<p class="text-center">請務必確認您的基本資料都確實填寫且正確無誤，再繼續前往提供舞台的申請，以加速審查、節省您的寶貴時間。</p>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>
				<div id="my_stages">
					<div class="form-group">
						<label class="form_title control-label la45">公司名稱</label>
						<div class="col-sm-5">
							<input type="text" class="form-control clean margin5" value="<?=$info['company_name']?>" name="company_name" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group">					
						<label class="form_title control-label la45">公司類型</label>
						<div class="col-sm-8 margin5" >
							<select name="company_type" class="selectpicker" data-width="250" data-size="8" required>	
								<?php foreach($company_type as $value){ ?>
								<option value="<?=$value['key']?>" <?=($info['company_type']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
								<?php } ?>								
							</select>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="form_title control-label la45">公司地址</label>
						<div class="col-sm-8 margin5" >
							<div id="twzipcode">
								<div data-role="county"
									 data-name="county"
									 data-style="county-district selectpicker"
									 style="float:left;"></div>							
								<div data-role="district"
									 data-name="district"
									 data-style="county-district selectpicker"
									 style="float:left;"></div>
								<div data-role="zipcode"
									 data-name="company_district"
									 data-style="zipcode-style"
									 style="display:none;"></div>
							</div>
						</div>
						
					</div>
					<div class="form-group">
						<label class="form_title control-label la45"></label>
						<div class="col-sm-5 margin5">
							<input type="text" name="company_address" value="<?=$info['company_address']?>" class="form-control"  placeholder="必填" required />
							<span class="help-block with-errors"></span>
						</div>							
					</div>
					<div class="form-group">
						<label class="form_title control-label la45">姓</label>
						<div class="col-sm-5 margin5" >
							<input type="text" name="last_name" class="form-control" value="<?=$info['last_name']?>" placeholder="" required />
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="form_title control-label la45">名</label>
						<div class="col-sm-5 margin5">
							<input type="text" name="name" class="form-control" value="<?=$info['name']?>" placeholder="" required />
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="form_title control-label la45">聯絡電話</label>
						<div class="col-sm-5 margin5">
							<input type="text" name="phone" class="form-control" value="<?=$info['phone']?>" placeholder="" required />
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="form_title control-label la45">電子信箱</label>
						<div class="col-sm-5 margin5">
							<input style="background-color:#1C1D26;" type="text" class="form-control" value="<?=$info['account']?>" placeholder=""  disabled  required />
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="form_title control-label la45">希望被聯繫方式</label>
					<div class="col-sm-8 margin5" >
						<select name="contact" class="selectpicker" data-width="250">	
							<?php foreach($contact as $value){ ?>
							<option value="<?=$value['key']?>" <?=($info['contact']==$value['key'])?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="form_title control-label la45">希望被聯繫時段</label>
					<div class="col-sm-8" >
						<div class="input-group" style="width: 250px;" >
							<input type="text" name="contact_start_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_start_time']));?>" required>
							<span class="input-group-addon">to</span>
							<input type="text" name="contact_end_time" class="form-control clockpicker"  value="<?=date("H:i",strtotime($info['contact_end_time']));?>" required>
						</div>		
						<span class="help-block with-errors"></span>
					</div>						
				</div>
				<div class="form-group">
					<label class="form_title control-label la45">可供應之項目</label>
					<div class="col-sm-8 margin5" >
						<select name="supply_item[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
							<?php foreach($supply_item as $key => $value){ ?>
							<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_items))?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>	
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="form_title control-label la45"></label>
					<div class="col-sm-8 margin5" >
						<input type="text" name="other_item" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_item']?>" />
					</div>						
				</div>
				<div class="form-group">
					<label class="form_title control-label la45">可提供之設備</label>
					<div class="col-sm-8 margin5" >
						<select name="supply_device[]" class="selectpicker detail_type" multiple data-width="250" data-size="8">									
							<?php foreach($supply_device as $key => $value){ ?>
							<option value="<?=$value['value']?>" <?=(in_array($value['value'],$supply_devices))?'selected':'';?>><?=$value['value']?></option>
							<?php } ?>	
						</select>
					</div>						
				</div>
				<div class="form-group">
					<label class="form_title control-label la45"></label>
					<div class="col-sm-8 margin5" >
						<input type="text" name="other_device" class="form-control myinput" placeholder="其他" value="<?=$info['other_supply_device']?>" />
					</div>						
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<p class="col-sm-offset-2 col-sm-8 text-center">請上傳3-5張您店內照片（至少三張），內容包含店家外觀、店內擺設、預計提供或挪用為展演的空間等，讓尋找舞台者能更容易找到您的位置，也更清楚空間的使用與限制。</p>
						<div id="photos_upload" class="col-sm-12">
							<span class="btn btn-success fileinput-button" style="margin-bottom:20px;">
								<i class="glyphicon glyphicon-plus"></i>
								<span>新增照片</span>
								<input id="fileupload" type="file" name="files[]" multiple>
							</span>						
							<div class="files" role="presentation"></div>					
						</div>
					</div>					
				</div>	
				
				
				
				<input type="button" id="clear" value="清除欄位重填">
				<input type="submit" id="go_register" value="儲存並前往申請舞台">
			</form>
		</section>
	</div>	
</div>

<!-- 上傳前的template -->
<script id="template-upload2" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<div class="col-lg-15 col-md-4 col-sm-4 template-upload fade" style="padding:0 5px 0 5px;margin-bottom:10px;">			
		<div class="preview"></div>
		<strong class="error text-danger"></strong>
		<div style="border-radius:0;height: 10px;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
	</div>
{% } %}
</script>

<!-- 上傳完後的template -->
<script id="template-download2" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}    
    <div class="col-lg-15 col-md-4 col-sm-4 template-download fade" data-key="" style="padding:0 5px 0 5px;margin-bottom:10px;">	
		<div class="photo-wrapper115" style="">
			<div class="photo" style="background-image: url({%=file.url%});"></div>			
		</div>
		<span class="photos_cover" style="color: black;border-radius: 4px;" data-name="{%=file.name%}">設為封面</span>	 
		<span class="delete photos_delete" style="top:1px;right: 6px;" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
			<i class="fa fa-times"></i>	
			<input type="hidden" name="filename[]" value="{%=file.name%}">	
		</span>	 
    </div>
{% } %}
</script>

<script>



$(document).ready(function() {
	$('.clockpicker').clockpicker({
		placement: 'bottom',
		align: 'left',
		autoclose: true
	});
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});
	create_multiple_upload('#photos_upload');
	$('#twzipcode').twzipcode({	
		<?php if($info['company_district']!=0){?>
		'zipcodeSel': '<?=$info['company_district'];?>'
		<?php }?>
	});
	$('.selectpicker').on('change', function() {
		$('.selectpicker').selectpicker('refresh');
	});
});

//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('member/securimage_jpg')?>'); 				
});	
//清除欄位
$(document).on("click", '#clear', function() {
	$('.clean').each(function(){
		$(this).val('');
	});
	$('input[type=checkbox]:checked,input[type=radio]:checked').each(function(){
		$(this).attr('checked', false);
	});
	$('select').each(function(){
		$(this).val($(this).find("option:first").val());
	});
});	

$(document).on('click','.photos_cover',function(){	
	var me=$(this);
	$.ajax({
		url: base_url+'upload/set_cover',
		type:"post",
		data: {
			'id':$('#id').val(),
			'filename':$(this).data('name')				
		},
		success:function(){
			$('.photos_cover').removeClass('is_cover');
			me.addClass('is_cover');
			alert('設置封面!');				
		}
	});
});
 $('form').submit(function() {
	if($('#photos_upload .photo').length<3){
		alert('至少上傳3張照片');
		return false;
	}
	if($('#photos_upload .is_cover').length==0){
		alert('請選擇一張當封面');
		return false;
	}
});

</script>