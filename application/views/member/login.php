<div class="container-fluid">
	<section id="login" class="row">
		<div class="login">
			<?=form_open('member/login_check', array('class' => 'form-horizontal'));?>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>
				<div class="form-group">				
					<div class="col-sm-12">
						<input type="text" class="form-control" name="username" id="username" placeholder="帳號">
					</div>
				</div>
				<div class="form-group">				
					<div class="col-sm-12">
						<input type="password" class="form-control" name="password" id="password" placeholder="密碼">
					</div>
				</div>				
				<div class="form-group">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-default" value="登入">
					</div>
				</div>
				<a id="forgetLink" href="#forgetModal" data-toggle="modal" class="forget" onclick="$('#loginModal').modal('hide');">忘記密碼？</a>				
				<div class="form-group">
					<div class="col-sm-12">
						<a href="<?=base_url('member/register')?>" class="btn btn-default regist" >註冊</a>
					</div>
				</div>
			</form>
		</div>	
	</section>
</div>	
<!-- forgetModal -->
<div class="modal fade" id="forgetModal" tabindex="-1" role="dialog" aria-labelledby="forgetModalLabel" data-keyboard="false" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body clearfix" >	
				<button type="button" class="close" data-dismiss="modal" style="position: absolute;right: 15px;z-index: 5;"><i class="fa fa-times"></i></button>
				<div class="form-group col-sm-12">
					<h3 id="forgetModalLabel">忘記密碼</h3>
				</div>
				<div id="checking_email">
					<?php echo form_open('member/forget',array('id'=>'forget_frm'));?>
					<div class="form-group col-sm-12" >
						<label>請輸入您的Email，我們將寄送更改密碼的連結至您的信箱。</label>										
					</div>
					<div class="form-group col-sm-12" >
						<label for="forget_usr">Email</label>
						<input type="input" name="forget_usr" class="form-control" id="forget_usr" placeholder="" />						
					</div>
					<div class="form-group col-sm-12">
						<img id="forget_captcha" class="captcha captcha_img pointer" src="<?=$img;?>" />
						<input type="text" class="form-control captcha_code" placeholder="" name="forget_captcha"  maxlength="6" >					
					</div>					
					<div class="form-group col-sm-12" style="margin: 5px 0 30px 0;">						
						<button type="button" id="forget" class="btn btn-primary ladda-button" data-style="zoom-in" data-size="l" style="float:left;width:140px;position:relative;height:34px;">
							<div id="loading" style="display:none;"></div><span class="ladda-label">發送連結到信箱</span>
						</button>
						<div id="forget_error" class="msg" ></div>
					</div>
					<?=form_close();?>							
				</div>
				<div id="emailed" style="display:none;">
					<div class="form-group col-sm-12" >
						<label>信件已發送至您的信箱，請點擊信中連結更改您的新密碼。</label>						
					</div>
					<div class="form-group col-sm-12" >
						<button type="button"  class="btn btn-primary" data-dismiss="modal"><span>關閉</span></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).on("click", '#forgetLink', function() {
	$('#forget_captcha').attr('src','<?=base_url('member/securimage_jpg');?>'); 	
	$("#checking_email").show();
	$("#emailed").hide();	
});	
$('#forgetModal').on('hidden.bs.modal', function () {
	$('#login_captcha').attr('src','<?=base_url('member/securimage_jpg');?>');
});
//點擊發送連結到信箱(忘記密碼)	
$(document).on("click", '#forget', function(e) {			
	if($('#forget_usr').val()==''){
		$("#forget_error").css('color','indianred').text("請輸入帳號").show();			
		return false;	
	}else{				
		$('#loading').show();
		$('#forget span').text('');
		$('#forget').prop('disabled',true);
		$.ajax({	
			url: $('#forget_frm').attr('action'),
			type:"post",
			data: $('#forget_frm').serialize(),
			dataType:"json",
			success:function(result){
				$('#forget span').text('發送連結到信箱');
				$('#forget').prop('disabled',false);
				$('#loading').hide();
				if(result==1){
					$("#checking_email").hide();
					$("#emailed").show();						
				}else{
					$("#forget_error").css('color','indianred').text(result).show();
					$('#forget_captcha').attr('src','<?=base_url('member/securimage_jpg');?>');		
				}
			}	
		});
	}
});
var opts = {
	lines: 9, // The number of lines to draw
	length: 4, // The length of each line
	width: 3, // The line thickness
	radius: 4, // The radius of the inner circle
	corners: 1, // Corner roundness (0..1)
	rotate: 0, // The rotation offset
	direction: 1, // 1: clockwise, -1: counterclockwise
	color: '#000', // #rgb or #rrggbb or array of colors
	speed: 1, // Rounds per second
	trail: 66, // Afterglow percentage
	shadow: false, // Whether to render a shadow
	hwaccel: false, // Whether to use hardware acceleration
	className: 'spinner', // The CSS class to assign to the spinner
	zIndex: 2e9, // The z-index (defaults to 2000000000)
	top: '50%', // Top position relative to parent
	left: '50%' // Left position relative to parent
};
var spinner = new Spinner(opts).spin();
$("#loading").append(spinner.el);
</script>