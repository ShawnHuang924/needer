
<div class="container-fluid">
	<section id="login" class="row">
		<div class="login forget">				
			<h3>輸入您的新密碼</h3>
			<hr>
			<?php if($status==0){ ?>
			<div class="alert alert-danger" role="alert">抱歉!! 驗證碼已過期</div>
			<?php }else{ ?>		
				<?php echo form_open('member/change_pwd',array('id'=>'reset_frm'));?>
				<input type="hidden" name="hash" id="hash" value="<?php echo $hash;?>" class="reset"/>
				<div class="form-group col-sm-12" >
					<label for="new_pwd">新密碼</label>
					<input type="password" name="new_pwd" class="form-control" id="new_pwd" />						
				</div>
				<div class="form-group col-sm-12">
					<label for="comfirm_pwd">確認密碼</label>					
					<input type="password" name="comfirm_pwd" class="form-control" id="comfirm_pwd"/>					
				</div>					
				<div class="form-group col-sm-12" style="margin: 5px 0 30px 0;">					
					<button type="submit" id="new_password" class="btn btn-primary" style="float:left;">確定更改密碼</button>
					<div id="new_error" class="msg" style="line-height: 34px;  margin-left: 125px"></div>
				</div>
				<?php echo form_close();?>
			<?php } ?>
		</div>	
	</section>
</div>	
<script>
$(document).on("click", '#new_password', function() {
	if($('#new_pwd').val()!=$('#comfirm_pwd').val()){
		$("#new_error").css('color','indianred').text("密碼不相符").show();
		return false;		
	}
});
</script>