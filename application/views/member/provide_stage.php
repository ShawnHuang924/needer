<div id="main" style="background: #1c1d26;color: rgba(255, 255, 255, 0.75);">
	<div class="container">
		<section id="provide_stage" class="row black_form">
			<form class="form-horizontal stage_block" method="post" action="<?=base_url('member/provide_check');?>" accept-charset="utf-8"  data-toggle="validator" role="form" >				
				<h2 class="text-center">提供舞台</h2>
				<?php if(isset($msg)){?>
				<div class="alert table_alert alert-<?=$msg['color']?>" role="alert">
					<a class="alert-link"><?=$msg['text']?></a>
				</div>	
				<?php } ?>	
				<div id="my_stages"></div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="button" id="new_stage" style="background: gold;color: black;" value="新增其他舞台">
					</div>
				</div>
				<input type="button" id="clear" value="清除欄位重填">
				<input type="submit" id="go_register" value="送出舞台申請">
			</form>
		</section>
	</div>	
</div>





<div id="one_stage" style="display:none">
	<div class="one_stage" data-stage="num">	
		<div class="form-group" >					
			<label class="form_title control-label la45">欲提供之舞台類型*：</label>
			<div class="form_options">
				<?php foreach($stage_type as $val){ ?>
				<div class="form_item1 control-label la45" style="text-align:center!important"><?=$val['type']?>
					<input class="hidding stage_type stage_type_check" name="p_num[stage_type]" id="<?=$val['key']?>_num" <?=($val['id']==1)?'checked':''?> type="radio" value="<?=$val['id']?>">	
					<label for="<?=$val['key']?>_num" class="circles2 pointer noselect" ></label>	
				</div>
				<?php } ?>		
			</div>
		</div>
		<div class="form-group">
			<label class="form_title control-label la45">偏好類別* (複選)：</label>	
			<div class="form_options">	
				<?php foreach($detail_type as $key => $val){ ?>
				<div class="form_item1 control-label la45 detail_type" style="text-align:center!important"><?=$val?>
					<input class="hidding detail_type_check" name="p_num[detail_type][]" id="<?=$val?>_num" type="checkbox" value="<?=$val?>" >
					<label for="<?=$val?>_num" class="circles2 pointer noselect"></label>
				</div>
				<?php } ?>
				<div class="col-xs-1 control-label la45 ggg" style="text-align:center!important">
					<input class="form-control clean" name="p_num[other_detail_type]" id="other_detail_type" type="text" placeholder="其他">					
				</div>
			</div>
			<span class="help-block with-errors"></span>
		</div>			
		<div class="date1">
			<div class="form-group">
				<label class="form_title control-label la45">可供活動日期* (複選)：</label>
				<div class="form_options">
					<?php foreach($week as $val){ ?>
					<div class="form_item1 control-label la45 weeks" style="text-align:center!important"><?=$val['value']?>
						<input class="hidding weeks_check" name="p_num[week][]" id="week<?=$val['key']?>_num" type="checkbox" value="<?=$val['value']?>">						
						<label for="week<?=$val['key']?>_num" class="circles2 pointer noselect"></label>
					</div>
					<?php } ?>
					<span class="help-block with-errors"></span>
				</div>						
				<div class="control-label la45" style="text-align:center!important;float:left;width:190px;padding-left:3px;">				
					<input type="text" class="form-control clean spec spec_date input-datepicker margin5" name="p_num[spec_date]" disabled style="display:none" placeholder="特定日期">
				</div>
				<button type="button" data-type="1" class="btn btn-info switch" style="margin-left:15px;margin-top:5px;">選特定日期</button>													
			</div>
			
			<div class="form-group">					
				<label class="form_title control-label la45">可供活動時段*：</label>
				<div class="col-sm-8">
					<div class="input-group" style="width: 250px;">
						<input type="text" class="form-control clockpicker clean start_time_check" placeholder="起"  name="p_num[start_time]" value="">
						<span class="input-group-addon">~</span>
						<input type="text" class="form-control clockpicker clean end_time_check" placeholder="迄"  name="p_num[end_time]" value="">
					</div>
				</div>
			</div>
			
			<div class="form-group">					
				<label class="form_title control-label la45">表演時間長度*：</label>
				<div class="col-sm-8 margin5" >
					<select name="p_num[show_length]">
						<?php foreach($length as $key => $val){?>
						<option value="<?=$val['value']?>"><?=$val['value']?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<div class="date2" style="display:none">
			<div class="form-group">
				<label class="form_title control-label la45">展覽日期*：</label>
				<div class="col-sm-8">
					<div class="input-daterange input-group" style="width: 250px;">
						<input type="text" class="form-control" name="p_num[date_start]" />
						<span class="input-group-addon">~</span>
						<input type="text" class="form-control" name="p_num[date_end]" />
					</div>
				</div>
			</div>
			<div class="form-group">					
				<label class="form_title control-label la45">展期長度*</label>
				<div class="col-sm-8">
					<select name="p_num[my_show_time]">
						<?php foreach($weekend as $key => $val){?>
						<option value="<?=$val['value']?>"><?=$val['value']?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="form_title control-label la45">備註事項 (複選)：</label>
			<div class="form_options">
				<div class="form_item1 control-label la45" style="text-align:center!important;float:left;width:156px;">提供影音連結或檔案
					<input class="hidding" name="p_num[remark][]" id="event1_num" type="checkbox" value="提供影音連結或檔案">
					<label for="event1_num" class="circles2 pointer noselect" style="width: 150px;left: calc(50% - 75px);"></label>
				</div>
				<div class="form_item1 control-label la45" style="text-align:center!important;float:left;">提供作品集
					<input class="hidding" name="p_num[remark][]" id="event2_num" type="checkbox" value="提供作品集">
					<label for="event2_num" class="circles2 pointer noselect"></label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="form_title control-label la45"></label>
			<div class="col-sm-5">
				<input type="text" class="form-control clean" name="p_num[other_remark]" placeholder="其他備註事項">
			</div>
		</div>
	</div>
</div>	











<script>

function set_detail_type(me) {
    $.ajax({
		url: base_url+'member/get_detail_type',
		type:"post",
		data: {'id':me.val()},
		dataType: 'json',
		success:function(result){
			//console.log(result);
			var my =  me.closest('.one_stage');			
			my.find('.detail_type').remove();	
			var num = my.data('stage');
			for(var key in result){
				my.find('.ggg').before(
					'<div class="form_item1 control-label la45 detail_type" style="text-align:center!important">'+result[key]+
						'<input class="hidding detail_type_check" name="p_'+num+'[detail_type][]" id="'+result[key]+'_'+num+'" type="checkbox"  value="'+result[key]+'" >'+
						'<label for="'+result[key]+'_'+num+'" class="circles2 pointer noselect"></label>'+
					'</div>'
				);				
			}
		}			
	});   
}

function clone_stage() {
	var num = $('#my_stages').find('.one_stage').length+1;
	if(num > 1)
		$('#my_stages').append('<hr>');
	var clone = $('#one_stage .one_stage').clone();
	clone.find('[name]').each(function(){
		$(this).attr("name",$(this).attr("name").replace('p_num', 'p_'+num ));
    });
	clone.find('[for]').each(function(){
		$(this).attr("for",$(this).attr("for").replace('\_num', '_'+num ));
    });
	clone.find('[id]').each(function(){
		$(this).attr("id",$(this).attr("id").replace('\_num', '_'+num ));
    });	
	clone.data('stage',num);
	clone.find('.input-daterange').datepicker({
		format: "yyyy-mm",
		startView: 2,
		minViewMode: 1,
		autoclose: true,
		language: "zh-TW",
		todayBtn: "linked"
	});
	clone.find('.input-datepicker').datepicker({
		format: "yyyy-mm-dd",
		startView: 2,
		autoclose: true,
		language: "zh-TW",
		todayBtn: "linked"
	});
	
	$('#my_stages').append(clone);
	$('.clockpicker').clockpicker({
		placement: 'bottom',
		align: 'left',
		autoclose: true
	});	
	
	
}

$(document).on('click', '#new_stage', function() {	
	clone_stage();
});

$(document).on('change', '.stage_type', function() {	
	if($(this).val()=='2'){
		$(this).closest('.one_stage').find('.date1').hide();
		$(this).closest('.one_stage').find('.date2').show();
	}			
	else{
		$(this).closest('.one_stage').find('.date1').show();
		$(this).closest('.one_stage').find('.date2').hide();
	}
	set_detail_type($(this));
	//console.log($(this).val());
});





$(document).ready(function() {
	clone_stage();
	$('.combodate').combodate();	
});

//重整驗證碼
$(document).on("click", '#captcha', function() {
	$('#captcha').attr('src','<?=base_url('member/securimage_jpg')?>'); 				
});	
//清除欄位
$(document).on("click", '#clear', function() {
	$('.clean').each(function(){
		$(this).val('');
	});
	$('input[type=checkbox]:checked,input[type=radio]:checked').each(function(){
		$(this).attr('checked', false);
	});
	$('select').each(function(){
		$(this).val($(this).find("option:first").val());
	});
});	
//切換每週
$(document).on('click', '.switch', function() {			
	var input = $(this).closest('.form-group');
	if($(this).data('type')==1){
		$(this).text('選每週固定');
		$(this).data('type',2);
		input.find('div.weeks').hide();			
		input.find('div.weeks input').attr('disabled',true);
		input.find('.spec').attr('disabled',false).show();
		input.find('.e-datewidget').show();
	}else{
		$(this).text('選特定日期');
		$(this).data('type',1);
		input.find('div.weeks').show();
		input.find('div.weeks input').attr('disabled',false);
		input.find('.spec').attr('disabled',true).hide();	
		input.find('.e-datewidget').hide();		
	}
});

</script>