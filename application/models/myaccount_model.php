<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount_model extends CI_Model{
	
	//取得會員資料
	public function get_member_info($id){
		$this->db->select('member.*')
				 ->from('member')
				 ->where('member.id',$id);
		$date['info']=$this->db->get()->row_array();	
		$this->db->select('theme_id')
				 ->from('member_theme')
				 ->where('member_id',$id);
		$date['themes']=$this->db->get()->result_array();		
		return $date; 
	}
	
	//儲存興趣
	public function save_theme($id,$theme){
		$this->db->where('member_id', $id);
		$this->db->delete('member_theme');
		if(!empty($theme)){
			$data = array();		
			foreach($theme as $val){
				$arr=array('member_id' => $id ,'theme_id' => $val);
				array_push($data, $arr);
			}		
			$this->db->insert_batch('member_theme', $data);
		}
	}
	
	//取得經歷
	public function get_history_info($id){
		$this->db->select('history.*')
				 ->from('history')
				 ->where('history.member_id',$id);
		$date=$this->db->get()->result_array();			
		return $date; 
	}
	
	//取得證照資格
	public function get_license_info($id){
		$this->db->select('license.*')
				 ->from('license')
				 ->where('license.member_id',$id);
		$date=$this->db->get()->result_array();			
		return $date; 
	}
	
	//取得媒合提報
	public function get_match_info($id){
		$this->db->select('show_info.*')
				 ->from('show_info')
				 ->where('show_info.member_id',$id);
		$date=$this->db->get()->row_array();			
		return $date; 
	}
	
	//取得媒合提報-注意事項
	public function get_notice_info($id){
		$this->db->select('notice.*')
				 ->from('notice')
				 ->where('notice.member_id',$id);
		$date=$this->db->get()->result_array();			
		return $date; 
	}
	
}
?>
