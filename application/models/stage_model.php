<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stage_model extends CI_Model{
	
	//取得舞台
    public function get_stage($data_post){    
		extract($data_post);
        $this->db->select('member.id,c.filename,member.company_name,county.county_name,GROUP_CONCAT(b.value) as stage_type',FALSE)
				 ->from('stage_info')				 
				 ->join('member','member.id = stage_info.member_id','left')
				 ->join('county','member.county = county.id','left')
				 ->join('(SELECT * FROM mapping WHERE type="stage_type") as b','b.key=stage_info.stage_type','left')
				 ->join('(SELECT * FROM member_photos WHERE cover=1) as c','c.user_id=member.id','left')
				 ->group_by("member.id")
				 /*->where('member.enable',1)*/;
        switch($action){
			case "filter":
				//篩選
				if(!empty($stage_type) && !in_array(0,$stage_type)){
					$this->db->where_in('stage_info.stage_type', $stage_type);					
				}					
				if(!empty($area) && !in_array(0,$area)){
					$this->db->where_in('county.area', $area);	
				}
				if(!empty($county) && !in_array(0,$county)){
					$this->db->where_in('county.id', $county);	
				}
				break;
				//排序
				/*switch($sort){								
					case 1:
						$this->db->order_by("baby_info.hit", "DESC");							
						break;
					case 2:
						$this->db->order_by("baby_info.create_time", "DESC");
						break;
				}
				break;	*/
			//搜尋	
			case "search":
				$this->db->like('member.company_name', $search);					
				break;
        }                       
        return $this->db->get()->result_array();        
    }
}
?>
